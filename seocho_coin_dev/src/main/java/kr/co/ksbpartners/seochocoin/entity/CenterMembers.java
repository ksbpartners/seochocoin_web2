package kr.co.ksbpartners.seochocoin.entity;

import lombok.Data;
import org.apache.ibatis.type.Alias;

import java.io.Serializable;
import java.util.Date;

@Data
@Alias("center_members")
public class CenterMembers extends EntityBase implements Serializable {
    private static final long serialVersionUID = 1L;

    private String centerId;
    private String memberId;


    public CenterMembers() {
    }

    public CenterMembers(String centerId, String memberId) {
        this.centerId = centerId;
        this.memberId = memberId;
    }

}
