package kr.co.ksbpartners.seochocoin.entity;

import kr.co.ksbpartners.seochocoin.util.constraint.ValidPassword;
import lombok.Data;
import org.apache.ibatis.type.Alias;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@Data
@Alias("member")
public class Member extends EntityBase implements Serializable {
    private static final long serialVersionUID = 1L;

    private String id;
    @ValidPassword
    private String pwd;
    private String old_pwd;
    @NotEmpty
    private String name;
    @NotEmpty
    private String mobile;
    private String token;

    private long coin;
    private String area;
    private int age;
    private String birth;
    private String sex;
    private String married;
    private String phone;
    private String email;

    private String postalCode;
    private String addrDoro;
    private String addrJibun;
    private String addrDetail;

    private String center;
    private String center1;
    private String center1Nm;
    private String center2;
    private String center2Nm;
    private String center3;
    private String center3Nm;

    private String keyword;
    private String keyword1;
    private String keyword2;
    private String keyword3;

    private int ranking;
    private int numOfActivity;

    public Member() { }

    public Member(String id, String pwd, String userId) {
        super(userId);
        this.id = id;
        this.pwd = pwd;
    }

    public Member(String id, String pwd, String name, String userId) {
        super(userId);
        this.id = id;
        this.pwd = pwd;
        this.name = name;
    }

}
