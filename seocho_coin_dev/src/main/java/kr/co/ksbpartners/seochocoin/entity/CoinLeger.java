package kr.co.ksbpartners.seochocoin.entity;

import lombok.Data;
import org.apache.ibatis.type.Alias;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

@Data
@Alias("coin_leger")
public class CoinLeger extends EntityBase implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String centerId;
    private String activityId;
    private String activityName;
    private String memberId;
    private String memberName;
    private String memberMobile;
    private String type;

    private long paid;
    private long coin;
    private transient LocalDate activityDate;

    private long coinSum;

    public CoinLeger() {
    }

    public CoinLeger(String centerId, String activityId, String memberId, String type, String userId) {
        super(userId);
        this.centerId = centerId;
        this.activityId = activityId;
        this.memberId = memberId;
        this.type = type;
    }
    
}
