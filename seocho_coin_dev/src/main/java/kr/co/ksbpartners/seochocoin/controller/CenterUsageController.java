package kr.co.ksbpartners.seochocoin.controller;

import kr.co.ksbpartners.seochocoin.common.userdetails.CentUser;
import kr.co.ksbpartners.seochocoin.entity.*;
import kr.co.ksbpartners.seochocoin.service.ActivityService;
import kr.co.ksbpartners.seochocoin.service.ApiActivityService;
import kr.co.ksbpartners.seochocoin.service.CoinLegerService;
import kr.co.ksbpartners.seochocoin.service.UsageService;
import kr.co.ksbpartners.seochocoin.vo.UsageVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/cent")
public class CenterUsageController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    ActivityService activityService;

    @Autowired
    CoinLegerService coinLegerService;

    @Autowired
    UsageService usageService;

    @Autowired
    ApiActivityService apiActivityService;


    @GetMapping("/usage-add")
    public String usageAdd(@RequestParam(required = false) String id, Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Center cent = ((CentUser)auth.getPrincipal()).getCenter();

        List<Activity> lst = null;
        Usage ret = null;
        if (null != cent) {
            // 활동처명 리스트 취득
            lst = activityService.getActivityListByCenterId(cent.getId());
        }
        if (StringUtils.isEmpty(id)) {
            ret = new Usage();
        } else {
            ret = usageService.getById(id);
        }

        model.addAttribute("activities", lst);
        model.addAttribute("usage", ret);
        return "cent/usage-add";
    }

    @PostMapping("/usage-add")
    public String usageAdd(@ModelAttribute("usage") @Validated Usage usage,
                              BindingResult bindingResult, Model model) throws Exception {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (bindingResult.hasErrors()) {
            return "cent/usage-add";
        }

        usage.setUpdatedBy(auth.getName());

        if (StringUtils.isEmpty(usage.getId())) {
            Center cent = ((CentUser)auth.getPrincipal()).getCenter();
            usage.setCenterId(cent.getId());
            usage.setCreatedBy(auth.getName());

            usage.setId(usageService.add(usage));
        } else {
            usageService.modify(usage);
        }

        return usageAdd(usage.getId(), model);
    }

    @GetMapping("/usage-list")
    public String usageList(@RequestParam(required = false) String keyword,
                            @RequestParam(required = false) String fromDate,
                            @RequestParam(required = false) String toDate,
                            @RequestParam(required = false) String activityId,
                            Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        List<Activity> lst = null;
        List<UsageVO> usages = null;
        Center cent = ((CentUser)auth.getPrincipal()).getCenter();
        if (null != cent) {
            // 활동처명 리스트 취득
            lst = activityService.getActivityListByCenterId(cent.getId());
            usages = usageService.getAll(cent.getId(), fromDate, toDate, keyword, activityId);
        }

        model.addAttribute("activities", lst);
        model.addAttribute("usages", usages);
        model.addAttribute("fromDate", fromDate);
        model.addAttribute("toDate", toDate);
        model.addAttribute("keyword", keyword);
        model.addAttribute("activityId", activityId);
        return "cent/usage-list";
    }


    @GetMapping("/usage-user-list")
    public String usageUserList(@RequestParam(required = false) String usageId,
                                @RequestParam(required = false) String keyword,
                                @RequestParam(required = false) String fromDate,
                                @RequestParam(required = false) String toDate,
                                Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Center cent = ((CentUser)auth.getPrincipal()).getCenter();

        List<Usage> lst = null;
        Usage usage = null;
        List<CoinLeger> memberList = null;
        if (null != cent) {
            // 사용처명 리스트 취득
            lst = usageService.getAllByCenterId(cent.getId());
            // 대상 사용처정보
            if (null != usageId) {
                usage = lst.stream().filter(v -> usageId.equals(v.getId())).findFirst().orElse(null);
            }
            memberList = usageService.getMemberList(usageId, fromDate, toDate, keyword);
        }

        model.addAttribute("usageId", usageId);
        model.addAttribute("fromDate", fromDate);
        model.addAttribute("toDate", toDate);
        model.addAttribute("keyword", keyword);
        model.addAttribute("usages", lst);
        model.addAttribute("usage", usage);
        model.addAttribute("memberList", memberList);
        return "cent/usage-user-list";
    }

    @GetMapping("/usage-download")
    public ResponseEntity<InputStreamResource> usageDownload(@RequestParam(required = false) String keyword,
                                                                @RequestParam(required = false) String fromDate,
                                                                @RequestParam(required = false) String toDate,
                                                                @RequestParam(required = false) String activityId,
                                                                Model model) throws IOException {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        List<UsageVO> lst = new ArrayList<>();
        Center cent = ((CentUser)auth.getPrincipal()).getCenter();
        if (null != cent) {
            lst = usageService.getAll(cent.getId(), fromDate, toDate, keyword, activityId);
        }
        List<Usage> usageList = new ArrayList<>();
        if (null != lst && !lst.isEmpty()) {
            for (UsageVO data : lst) {
                usageList.add(data.getUsage());
            }
        }

        ByteArrayInputStream in = usageService.generateUsageListFile(usageList);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "attachment; filename=usageList.xlsx");

        return ResponseEntity
                .ok()
                .headers(headers)
                .body(new InputStreamResource(in));
    }

    @PostMapping("/usage-upload")
    public String usageUpload(@RequestParam("elmFile") MultipartFile multipartFile,
                                 RedirectAttributes redirectAttributes) {
        // ファイルが空の場合は異常終了
        if (multipartFile.isEmpty()) {
            // TODO error
        }
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Center cent = ((CentUser)auth.getPrincipal()).getCenter();

        logger.info("upload file {}", multipartFile.getOriginalFilename());
        int uploadCnt = 0;
        String msg = null;
        try {
            uploadCnt = usageService.uploadUsageListFile(cent, multipartFile);
            msg = "업로드 성공: " + multipartFile.getOriginalFilename() + " 등록 수: " + uploadCnt + "건";
        } catch (Exception e) {
            msg = e.getMessage();
        }
        logger.info(msg);

        redirectAttributes.addFlashAttribute("message", msg);
        return "redirect:/cent/usage-list";
    }

    /**
     * 활동별 회원 내역 다운로드
     */
    @GetMapping("/usageMember-download")
    public ResponseEntity<InputStreamResource> usageMemberDownload(@RequestParam(required = false) String usageId,
                                                                      @RequestParam(required = false) String keyword,
                                                                      @RequestParam(required = false) String fromDate,
                                                                      @RequestParam(required = false) String toDate,
                                                                      Model model) throws IOException {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        Center cent = ((CentUser)auth.getPrincipal()).getCenter();
        List<CoinLeger> memberList = null;
        if (null != cent) {
            // 대상 활동내 사용자 리스트 취득
            memberList = usageService.getMemberList(usageId, fromDate, toDate, keyword);
        }

        ByteArrayInputStream in = usageService.genMemberListFile(memberList);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "attachment; filename=usageMemberList.xlsx");

        return ResponseEntity
                .ok()
                .headers(headers)
                .body(new InputStreamResource(in));
    }

    @PostMapping("/usage-delete")
    public String deleteUsage(@RequestParam List<String> delIds,
                                 @RequestParam(required = false) String keyword,
                                 @RequestParam(required = false) String fromDate,
                                 @RequestParam(required = false) String toDate,
                                 @RequestParam(required = false) String activityId,
                                 Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        // ファイルが空の場合は異常終了
        if (delIds.isEmpty()) {
            // TODO error
        }

        usageService.remove(delIds, auth.getName());

        return usageList(keyword, fromDate, toDate, activityId, model);
    }

    @PostMapping("/usage-member-delete")
    public String deleteUsageMember(@RequestParam List<String> delIds,
                                       @RequestParam("usageId") String usageId,
                                       @RequestParam(required = false) String keyword,
                                       @RequestParam(required = false) String fromDate,
                                       @RequestParam(required = false) String toDate, Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        // ファイルが空の場合は異常終了
        if (delIds.isEmpty()) {
            // TODO error
        }

        coinLegerService.removeMembers(usageId, delIds, auth.getName());

        return usageUserList(usageId, keyword, fromDate, toDate, model);
    }

    @GetMapping("/usage-coin-cancel")
    @Transactional
    public String cancelUsageCoin(@RequestParam String memberId,
                                  @RequestParam String activityId,
                                  @RequestParam long coin,
                                  @RequestParam(required = false) String usageId,
                                  @RequestParam(required = false) String keyword,
                                  @RequestParam(required = false) String fromDate,
                                  @RequestParam(required = false) String toDate, Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        coinLegerService.removeMembers(activityId, Arrays.asList(memberId), auth.getName());
        Member member = new Member();
        member.setCoin(coin);
        member.setId(memberId);
        member.setUpdatedBy(auth.getName());
        apiActivityService.updateCoinOfMember(member);
        return usageUserList(usageId, keyword, fromDate, toDate, model);
    }
}
