package kr.co.ksbpartners.seochocoin.controller;

import kr.co.ksbpartners.seochocoin.entity.*;
import kr.co.ksbpartners.seochocoin.service.*;
import kr.co.ksbpartners.seochocoin.constants.Constants;
import kr.co.ksbpartners.seochocoin.util.Utils;
import kr.co.ksbpartners.seochocoin.vo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

@RestController
@RequestMapping("/api")
public class ApiActivityController {
    DecimalFormat df = new DecimalFormat("#,###");

    @Autowired
    ApiMemberService apiMemberService;

    @Autowired
    ActivityService activityService;

    @Autowired
    ApiActivityService apiActivityService;

    @Autowired
    MemberService memberService;

    @Autowired
    ActivityCategoryService activityCategoryService;

    @PostMapping("/activities")
    public Map<String, Object> getAllActivities(String uid, String token) {
        Map<String, Object> result = checkToken(uid, token);

        if (!result.containsKey(Constants.RESULT)) {
            List<String> centerIds = new ArrayList<>();
            List<String> keywords = new ArrayList<>();

            Member member = apiMemberService.getById(uid);
            centerIds.add(member.getCenter1());
            if (member.getCenter2() != null && !member.getCenter2().isEmpty())
                centerIds.add(member.getCenter2());
            if (member.getCenter3() != null && !member.getCenter3().isEmpty())
                centerIds.add(member.getCenter3());

            if (member.getKeyword1() != null && !member.getKeyword1().isEmpty())
                keywords.add(member.getKeyword1());
            if (member.getKeyword2() != null && !member.getKeyword2().isEmpty())
                keywords.add(member.getKeyword2());
            if (member.getKeyword3() != null && !member.getKeyword3().isEmpty())
                keywords.add(member.getKeyword3());

            List<ActivityInfo> activityList = apiActivityService.getAllActivities(new ActivityInfo(centerIds, keywords));

            result.put(Constants.RESULT, Constants.RESULT_OK);
            result.put(Constants.LIST, activityList);
            result.put(Constants.COUNT, activityList != null && !activityList.isEmpty() ? activityList.size() : 0);
        }

        return result;
    }

    @PostMapping("/activities/{a_id}")
    public Map<String, Object> getAllActivities(String uid, String token,
                                                @PathVariable String a_id) {
        Map<String, Object> result = checkToken(uid, token);

        if (!result.containsKey(Constants.RESULT)) {
            MyActivityInfo myActivityInfo = apiMemberService.getActivityInfoById(uid, a_id);

            ActivityInfo activityInfo = apiActivityService.getAllActivitiesById(a_id);
            activityInfo.setCoinPrice(df.format(activityInfo.getAddCoinVal() * Constants.BASE_COIN_PRICE));
            activityInfo.setTimeTables(Utils.SET_TIMETABLES(activityInfo));

            result.put(Constants.RESULT, Constants.RESULT_OK);
            result.put(Constants.ACTIVITY, activityInfo);
            if (myActivityInfo != null && myActivityInfo.getActivityId().equals(activityInfo.getId())) {
                if (!Constants.ACTIVITY_STATUS_CANCEL.equals(myActivityInfo.getActivityStatus())) {
                    result.put(Constants.STATUS, Constants.STATUS_ALREADY_REGIST);
                }
            }
        }

        return result;
    }

    @PostMapping("/activities/attend")
    public Map<String, Object> attendActivity(String uid, String token,
                                                 String aid) {
        Map<String, Object> result = checkToken(uid, token);

        if (!result.containsKey(Constants.RESULT)) {
            ActivitySchedule paramActivity = new ActivitySchedule();
            paramActivity.setMemberId(uid);
            paramActivity.setActivityId(aid);

            ActivitySchedule activitySchedule = apiActivityService.getAttendActivity(paramActivity);

            if (activitySchedule == null) {
                // new registration
                result.put(Constants.RESULT, Constants.NOT_REGIST);
            } else {
                if (activitySchedule.getAttendedAt() != null) {
                    result.put(Constants.RESULT, Constants.ALREADY_ATTENDED);
                } else {
                    paramActivity.setUpdatedBy(uid);
                    apiActivityService.setAttendActivity(paramActivity);

                    Activity activity = apiActivityService.getAddCoinInfo(aid);
                    if ("N".equals(activity.getAttendCheck())) {
                        CoinLeger coinLeger = new CoinLeger();
                        coinLeger.setCenterId(activity.getCenterId());
                        coinLeger.setActivityId(aid);
                        coinLeger.setMemberId(uid);
                        coinLeger.setType("적립");
                        coinLeger.setCoin(activity.getAddCoinVal());
                        coinLeger.setPaid(activity.getAddCoinVal() * Constants.BASE_COIN_PRICE);
                        coinLeger.setActivityDate(LocalDate.now());
                        coinLeger.setCreatedBy("sys");
                        coinLeger.setUpdatedBy("sys");

                        apiActivityService.addCoinLedger(coinLeger);

                        Member member = new Member();
                        member.setCoin(activity.getAddCoinVal());
                        member.setId(uid);
                        member.setUpdatedBy(uid);
                        apiActivityService.updateCoinOfMember(member);
                    } else {
                        // 관리자 확인 적립
                    }

                    result.put(Constants.RESULT, Constants.RESULT_OK);
                }
            }
        }

        return result;
    }

    @PostMapping("/activities/attend/v2")
    public Map<String, Object> registActivity(@RequestBody Map<String, String> params) {
        String uid_token = params.get("uid_token");
        String aid = params.get("aid");

        if (uid_token == null || uid_token.isEmpty() || aid == null || aid.isEmpty()) {
            Map<String, Object> result = new HashMap<>();
            result.put("result", Constants.INSUFFICIENT_PARAM);
            return result;
        }

        StringTokenizer st = new StringTokenizer(uid_token, "&");
        String _uid = st.nextToken();
        String _token = st.nextToken();

        return attendActivity(_uid.substring(4), _token.substring(6), aid);
    }

    @PostMapping("/activities/regist")
    public Map<String, Object> registActivity(String uid, String token,
                                              String aid) {
        Map<String, Object> result = checkToken(uid, token);

        if (!result.containsKey(Constants.RESULT)) {
            Activity activity = activityService.getActivityById(aid);

            MemberActivityVO maVo = new MemberActivityVO();
            maVo.setMemberId(uid);
            maVo.setCenterId(activity.getCenterId());
            maVo.setActivityId(activity.getId());
            maVo.setActivityStatus("신청중");
            maVo.setAccumStatus("Y".equals(activity.getAddCoin()) ? "적립" : "적립안됨");
            maVo.setAccumCoin(activity.getAddCoinVal());
            maVo.setCreatedBy(uid);
            maVo.setUpdatedBy(uid);

            try {
                ActivityScheduleVO schedule = new ActivityScheduleVO();
                if (Constants.YES.equals(activity.getMon()))
                    schedule.setMonMemIds(Arrays.asList(uid));
                if (Constants.YES.equals(activity.getTue()))
                    schedule.setTueMemIds(Arrays.asList(uid));
                if (Constants.YES.equals(activity.getWed()))
                    schedule.setWedMemIds(Arrays.asList(uid));
                if (Constants.YES.equals(activity.getThu()))
                    schedule.setThuMemIds(Arrays.asList(uid));
                if (Constants.YES.equals(activity.getFri()))
                    schedule.setFriMemIds(Arrays.asList(uid));

                List<ActivitySchedule> newScheduleList = Utils.SET_ACTIVITY_SCHEDULE(activity, schedule);
                apiActivityService.registActivitySchedule(newScheduleList);

                int isAdded = apiActivityService.registMember(maVo);

                if (isAdded < 1) {
                    result.put(Constants.RESULT, Constants.RESULT_NG);
                } else {
                    result.put(Constants.RESULT, Constants.RESULT_OK);
                    result.put(Constants.AC_ID, activity.getId());
                }
            } catch (Exception ex) {
                result.put(Constants.RESULT, Constants.DUPLICATE_KEY);
                result.put(Constants.ERROR_MSG, ex.getMessage());
            }
        }

        return result;
    }

    @PostMapping("/activities/cancel")
    public Map<String, Object> cancelActivity(String uid, String token,
                                              String aid) {
        Map<String, Object> result = checkToken(uid, token);

        if (!result.containsKey(Constants.RESULT)) {
            MemberActivityVO maVo = new MemberActivityVO();
            maVo.setMemberId(uid);
            maVo.setActivityId(aid);
            maVo.setActivityStatus("취소");
            maVo.setUpdatedBy(uid);

            int isAdded = apiActivityService.cancelMember(maVo);

            if (isAdded < 1) {
                result.put(Constants.RESULT, Constants.RESULT_NG);
            } else {
                result.put(Constants.RESULT, Constants.RESULT_OK);
                result.put(Constants.AC_ID, aid);
            }
        }

        return result;
    }

    @PostMapping("/activities/categories")
    public Map<String, Object> getActivityCategory(String uid, String token,
                                              String type) {
        Map<String, Object> result = checkToken(uid, token);

        if (!result.containsKey(Constants.RESULT)) {
            List<ActivityCategoryVO> categoryList = activityCategoryService.getAllByType(type);

            result.put(Constants.AC_CATE_LIST, categoryList);
        } else {
            result.put(Constants.AC_CATE_LIST, new ArrayList<>());
        }

        result.put(Constants.RESULT, Constants.RESULT_OK);

        return result;
    }

    private Map<String, Object> checkToken(String uid, String token) {
        Map<String, Object> result = new HashMap<>();

        Member _member = apiMemberService.getTokenById(uid);
        if (_member == null) {
            result.put(Constants.RESULT, Constants.NOT_EXISTS);
        } else {
            if (token == null || !token.equals(_member.getToken())) {
                result.put(Constants.RESULT, Constants.EXPIRED_TOKEN);
            } else {
                result.clear();
            }
        }

        return result;
    }
}
