package kr.co.ksbpartners.seochocoin.service;

import kr.co.ksbpartners.seochocoin.constants.Constants;
import kr.co.ksbpartners.seochocoin.entity.Activity;
import kr.co.ksbpartners.seochocoin.entity.ActivitySchedule;
import kr.co.ksbpartners.seochocoin.entity.CoinLeger;
import kr.co.ksbpartners.seochocoin.entity.Member;
import kr.co.ksbpartners.seochocoin.repository.*;
import kr.co.ksbpartners.seochocoin.util.constraint.PasswordConstraintValidator;
import kr.co.ksbpartners.seochocoin.util.crypto.CryptoUtil;
import kr.co.ksbpartners.seochocoin.vo.CellVO;
import kr.co.ksbpartners.seochocoin.util.excel.ExcelPOIUtil;
import kr.co.ksbpartners.seochocoin.vo.MemberVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

@Service
public class MemberService extends AbstractService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private MemberMapper memberMapper;

    @Autowired
    private ActivityMapper activityMapper;

    @Autowired
    private ActivityScheduleMapper activityScheduleMapper;

    @Autowired
    private CoinLegerMapper coinLegerMapper;

    @Autowired
    private ApiActivityMapper apiActivityMapper;

    @Autowired
    private ApiMemberService apiMemberService;

    public Member getById(String id) {
        return memberMapper.getById(id);
    }

    public List<MemberVO> getAll(String centerId, String keyword) {

        List<Member> members = memberMapper.selectAll(centerId, keyword);

        List<MemberVO> lst = new ArrayList<>();
        if (null != members && members.size() > 0) {
            for (Member mem : members) {
                MemberVO vo = new MemberVO();
                vo.setMember(mem);
                List<Activity> actList = activityMapper.selectAllByMemberId(mem.getId(), centerId);
                vo.setActivityList(actList);
                vo.setNumOfActivity(null != actList ? actList.size() : 0);
                lst.add(vo);
            }
        }

        return lst;
    }

    public List<MemberVO> getAll4Stat(String centerId, String fromDate, String toDate, String keyword) {
        List<Member> members = memberMapper.selectAll4Stat(centerId, fromDate, toDate, keyword);

        List<MemberVO> lst = new ArrayList<>();
        long count = countAll4Stat(centerId, fromDate, toDate, keyword);
        if (null != members && members.size() > 0) {
            Map<String, String> map = new HashMap<>();
            for (Member mem : members) {
                map.clear();
                map.put("memberId", mem.getId());
                MemberVO vo = new MemberVO();
                vo.setMember(mem);
                List<CoinLeger> coinLegersList = coinLegerMapper.selectCoinSumByType(map);
                if (coinLegersList != null && !coinLegersList.isEmpty()) {
                    coinLegersList.forEach(coin -> {
                        if (Constants.COIN_PLUS.equals(coin.getType())) {
                            vo.setCoinPlusSum(vo.getCoinPlusSum() + coin.getCoinSum());
                        } else if (Constants.COIN_MINUS.equals(coin.getType())) {
                            vo.setCoinPlusSum(vo.getCoinPlusSum() + coin.getCoinSum());
                        } else if (Constants.COIN_USED.equals(coin.getType())) {
                            vo.setCoinMinusSum(coin.getCoinSum());
                        }
                        vo.setUpdatedAt(coin.getUpdatedAt());
                    });
                }

                if (vo.getCoinPlusSum() == 0 && vo.getCoinMinusSum() == 0) {
                    count--;
                    continue;
                }

                vo.setNo(count--);
                lst.add(vo);
            }
        }

        return lst;
    }

    public long countAll4Stat(String centerId, String fromDate, String toDate, String keyword) {
        return memberMapper.countAll4Stat(centerId, fromDate, toDate, keyword);
    }

    public MemberVO getDetail(String memberId) {

        Member member = memberMapper.getById(memberId);

        MemberVO vo = new MemberVO();
        if (null != member) {
            vo.setMember(member);
            List<Activity> actList = activityMapper.selectAllByMemberId(member.getId(), null);
            vo.setActivityList(actList);
            vo.setNumOfActivity(null != actList ? actList.size() : 0);

            List<ActivitySchedule> scheduleList = activityScheduleMapper.selectAllByMemberId(memberId, null, null, null);
            vo.setScheduleList(scheduleList);

            List<CoinLeger> coinLegerList = coinLegerMapper.selectAll(memberId, null);
            vo.setCoinLegerList(coinLegerList);
        }

        return vo;
    }

    public List<ActivitySchedule> getMemberScheduleList(String memberId, String activityId, String fromDate, String toDate) {
        return activityScheduleMapper.selectAllByMemberId(memberId, activityId, fromDate, toDate);
    }

    public String add(Member member) throws Exception{
        // timestamp
        LocalDateTime timeStamp = LocalDateTime.now();
        // get id
        Member entity = memberMapper.getMaxId();
        String newId = getNewId(Constants.PREFIX_MEMBER_ID, null == entity ? "" : entity.getId());

        Member newMember = new Member();
        BeanUtils.copyProperties(member, newMember);
        newMember.setId(newId);
        newMember.setCreatedAt(timeStamp);
        newMember.setUpdatedAt(timeStamp);

        int ret = memberMapper.register(newMember);
        if (ret != 1) {
            throw new Exception("Failed to generate new data.");
        }
        // add base coin
        CoinLeger coinLeger = new CoinLeger();
        coinLeger.setActivityId("act000000");
        coinLeger.setCenterId(newMember.getCenter1());
        coinLeger.setMemberId(newMember.getId());
        coinLeger.setType("적립");
        coinLeger.setCoin(newMember.getCoin());
        coinLeger.setPaid(newMember.getCoin() * Constants.BASE_COIN_PRICE);
        coinLeger.setCreatedBy(newMember.getCreatedBy());
        coinLeger.setUpdatedBy(newMember.getUpdatedBy());

        apiActivityMapper.addCoinLedger(coinLeger);

        return newMember.getId();
    }

    public int modify(Member member) {
        // timestamp
        LocalDateTime timeStamp = LocalDateTime.now();
        member.setUpdatedAt(timeStamp);

        // update base coin
        CoinLeger coinLeger = new CoinLeger();
        coinLeger.setActivityId("act000000");
        coinLeger.setCenterId(member.getCenter1());
        coinLeger.setMemberId(member.getId());
        coinLeger.setCoin(member.getCoin());
        coinLeger.setPaid(member.getCoin() * Constants.BASE_COIN_PRICE);
        coinLeger.setUpdatedBy(member.getUpdatedBy());

        apiActivityMapper.updateBaseCoinLedger(coinLeger);

        return memberMapper.modify(member);
    }

    public int remove(List<String> ids, String updatedBy) {
        // timestamp
//        LocalDateTime timeStamp = LocalDateTime.now();
//        member.setUpdatedAt(timeStamp);
        // delete member
        int res = memberMapper.delete(ids, updatedBy);
        // delete member_activity
        res += activityMapper.deleteMembersActivity(ids, updatedBy);
        // delete activity_schedule
        res += activityScheduleMapper.deleteMembersSchedules(ids, updatedBy);
        // delete base coin
        res += coinLegerMapper.deleteUsageMembers("act000000", ids, updatedBy);
        return res;
    }

    @Transactional
    public int uploadMemberListFile(MultipartFile multipartFile, String centerId) throws Exception {
        logger.info("load file data to db.");

        final String fileType = "centMember";

        StringBuffer filePath = new StringBuffer("./uploadfiles").append(File.separator).append(fileType);

        // file upload to server
        String fileName = getFileName(multipartFile.getOriginalFilename());
        File uploadDir = mkdirs(filePath);
        File uploadFile = new File(uploadDir.getPath() + "/" +  fileName);

        try (BufferedOutputStream uploadFileStream
                     = new BufferedOutputStream(new FileOutputStream(uploadFile))) {

            byte[] bytes = multipartFile.getBytes();
            uploadFileStream.write(bytes);

        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }


        // upload to data base
        if (!uploadFile.exists()) {
            throw new Exception("일괄등록 대상 파일을 읽을 수 없습니다.");
        }

        int uploadCnt;
        Map<Integer, List<CellVO>> map = ExcelPOIUtil.readExcel(uploadFile.getAbsolutePath());
        AtomicInteger cnt = new AtomicInteger(2);
        int sum = 0;
        for (Map.Entry<Integer, List<CellVO>> e : map.entrySet()) {
            if (e.getKey() != 0) {
                int i = insertIfNotExist(cnt.getAndIncrement(), e.getValue(), centerId);
                sum += i;
            }
        }
        uploadCnt = sum;

        return uploadCnt;
    }

    public ByteArrayInputStream generateMemberListFile(List<MemberVO> memberList) throws IOException {
        logger.info("generate Excel file.");

        List<Member> members = new ArrayList<>();
        if (null != memberList) {
            for (MemberVO vo : memberList) {
                vo.getMember().setNumOfActivity(vo.getNumOfActivity());
                members.add(vo.getMember());
            }
        }

        return ExcelPOIUtil.membersToExcel(members);
    }

    private int insertIfNotExist(int rowNum, List<CellVO> cellVal, String centerId) throws Exception {
        // TODO more handling
        if (StringUtils.isEmpty(cellVal.get(0).getContent())
            || StringUtils.isEmpty(cellVal.get(1).getContent())
            || StringUtils.isEmpty(cellVal.get(3).getContent())) {
            // 회원명이 미설정일 경우 스킵.
            throw new Exception(String.format("%s행[%s 님]의 1, 2, 4열은 필수 항목입니다.",
                    rowNum, cellVal.get(1).getContent()));
        }

        int pos = 0;
        Member mem = new Member();
        // 패스워드
        PasswordConstraintValidator validator = new PasswordConstraintValidator();
        String pwd = cellVal.get(pos++).getContent();
        if (!validator.isValid(pwd, null)) {
            throw new Exception(String.format("%s행[%s 님], %s열의 패스워드 형식이 잘못 입력되어 업로드가 실패했습니다. \n(영문,숫자,특수문자 조합 8자리 이상)",
                    rowNum, cellVal.get(1).getContent(), pos));
        }
        mem.setPwd(CryptoUtil.encryptAES256(pwd, Constants.SALT_KEY));
        try {
            // 회원명
            mem.setName(cellVal.get(pos++).getContent());
            // 생년월일
            mem.setBirth(cellVal.get(pos++).getContent());
            // 핸드폰(연락처)
            mem.setMobile(cellVal.get(pos++).getContent());
            // 기본코인
            mem.setCoin(cellVal.get(pos).getContent() != null ?
                    new BigDecimal(cellVal.get(pos++).getContent()).intValue() : 0);
            // 우편번호
            mem.setPostalCode(cellVal.get(pos++).getContent());
            // 도로명주소
            mem.setAddrDoro(cellVal.get(pos++).getContent());
            // 지번주소
            mem.setAddrJibun(cellVal.get(pos++).getContent());
            // 상세주소
            mem.setAddrDetail(cellVal.get(pos++).getContent());
            // 전화번호
            mem.setPhone(cellVal.get(pos++).getContent());
            // 성별(M/F)
            mem.setSex(cellVal.get(pos++).getContent());
            // 결혼여부(M/S)
            mem.setMarried(cellVal.get(pos++).getContent());
            // 이메일
            mem.setEmail(cellVal.get(pos++).getContent());
        } catch (Exception e) {
            throw new Exception(String.format("%s행[%s 님], %s열의 형식이 잘못 입력되어 업로드가 실패했습니다.",
                    rowNum, cellVal.get(1).getContent(), pos));
        }
        // check mobile
        List<Member> _member = apiMemberService.getTokenByMobile(mem.getMobile());
        if (_member != null && !_member.isEmpty() && !_member.get(0).getId().isEmpty()) {
            throw new Exception(String.format("%s행[%s 님], 핸드폰(연락처, %s) 값이 중복되어 업로드가 실패했습니다.",
                    rowNum, cellVal.get(1).getContent(), cellVal.get(3).getContent()));
        }

        // get id
        Member entity = memberMapper.getMaxId();
        String newId = getNewId(Constants.PREFIX_MEMBER_ID, null == entity ? "" : entity.getId());
        mem.setId(newId);
        // 활동 센터 1
        mem.setCenter1(centerId);
        mem.setCreatedBy("uploader");
        mem.setUpdatedBy("uploader");

        try {
            memberMapper.register(mem);
        } catch (Exception e) {
            throw new Exception(String.format("%s행[%s 님]의 형식이 잘못 입력되어 업로드가 실패했습니다.\n 상세내용: %s",
                    rowNum, cellVal.get(1).getContent(), e.getMessage()));
        }
//        Member old = memberMapper.getById(mem.getId());
//        if (null == old) {
//            mapper.register(mem);
//        }
        // 기본코인 반영
//        if (mem.getCoin() > 0) {
//            CoinLeger coinLeger = new CoinLeger();
//            coinLeger.setActivityId("act000000");
//
//
//        }
        return 1;
    }

}