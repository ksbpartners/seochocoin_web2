package kr.co.ksbpartners.seochocoin.service;

import kr.co.ksbpartners.seochocoin.entity.CoinLeger;
import kr.co.ksbpartners.seochocoin.repository.CoinLegerMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service
public class CoinLegerService extends AbstractService {

    @Autowired
    private CoinLegerMapper coinLegerMapper;

    public List<CoinLeger> getAll(String memberId, String centerId) {
        return coinLegerMapper.selectAll(memberId, centerId);
    }

    @Transactional
    public int removeMembers(String usageId, List<String> ids, String updatedBy) {
        return coinLegerMapper.deleteUsageMembers(usageId, ids, updatedBy);
    }

    public List<CoinLeger> selectCoinSumByType(Map<String, String> map) {
        return coinLegerMapper.selectCoinSumByType(map);
    }

    public List<CoinLeger> getTotCoin(String activityId, String fromDate, String toDate) {
        return coinLegerMapper.getTotCoin(activityId, fromDate, toDate);
    }
}