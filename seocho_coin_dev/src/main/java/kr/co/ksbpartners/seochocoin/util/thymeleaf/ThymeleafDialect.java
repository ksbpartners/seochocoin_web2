package kr.co.ksbpartners.seochocoin.util.thymeleaf;

import org.thymeleaf.dialect.AbstractDialect;
import org.thymeleaf.dialect.IExpressionObjectDialect;
import org.thymeleaf.expression.IExpressionObjectFactory;

public class ThymeleafDialect extends AbstractDialect implements IExpressionObjectDialect {

    protected ThymeleafDialect() {
        super("helper");
    }

    @Override
    public IExpressionObjectFactory getExpressionObjectFactory() {
        return new ThymeleafExpressionObjectFactory();
    }
}
