package kr.co.ksbpartners.seochocoin.repository;

import kr.co.ksbpartners.seochocoin.entity.Activity;
import kr.co.ksbpartners.seochocoin.vo.MemberActivityVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface ActivityMapper {

    Activity getById(String activityId);

    List<Activity> selectAllByMemberId(String memberId, String centerId);

    List<Activity> selectAllByCenterId(String centerId, String fromDate, String toDate, String keyword);

    List<Activity> selectAll4Stat(String centerId, String fromDate, String toDate, String keyword);
    long countAll4Stat(String centerId, String fromDate, String toDate, String keyword);

    Activity getMaxId();

    int register(Activity activity);

    int modify(Activity activity);

    int updateSchedule(Activity activity);

    int updateOldMemStatus(String activityId);

    int upsertNewMember(MemberActivityVO memberActivity);

    int delete(List<String> ids, String updatedBy);

    int deleteByCenterId(List<String> ids, String updatedBy);

    int deleteMemberActivities(List<String> ids, String updatedBy);

    int deleteMemberActivitiesByCenterId(List<String> ids, String updatedBy);

    int deleteActivityMembers(String activityId, List<String> ids, String updatedBy);

    int deleteActivityMember(String activityId, String memberId);

    int deleteMembersActivity(List<String>ids, String updatedBy);

    int changeMemberActivityStatus(String memberId, String activityId, String activityStatus, String updatedBy);

    int changeMemberAccumStatus(String memberId, String activityId, String accumStatus, String updatedBy);

    int updateAccumCoin(String activityId, long accumCoin, String updatedBy);
}