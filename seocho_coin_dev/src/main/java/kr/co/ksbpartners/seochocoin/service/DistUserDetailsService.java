package kr.co.ksbpartners.seochocoin.service;

import kr.co.ksbpartners.seochocoin.common.userdetails.CentUser;
import kr.co.ksbpartners.seochocoin.common.userdetails.DistUser;
import kr.co.ksbpartners.seochocoin.entity.Admin;
import kr.co.ksbpartners.seochocoin.entity.Center;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service("distUserDetailsService")
public class DistUserDetailsService implements UserDetailsService {

    @Autowired
    private AdminService adminService;

//    @Autowired
//    private PasswordEncoder passwordEncoder;
//
//    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
//        this.passwordEncoder = passwordEncoder;
//    }

    public void setAdminService(AdminService adminService) {
        this.adminService = adminService;
    }

    @Transactional
    @Override
    public UserDetails loadUserByUsername(String username)
            throws UsernameNotFoundException {

        Admin admin = adminService.getByDistId(username);
        List<GrantedAuthority> authorities = new ArrayList<>();
        if (admin == null) {
            throw new UsernameNotFoundException("User " + username + " was not found in the database");
        }
        authorities.add(new SimpleGrantedAuthority("ROLE_DIST"));

        return buildUserForAuthentication(admin, authorities);
    }

    private User buildUserForAuthentication(Admin admin, List<GrantedAuthority> authorities) {
        if (admin != null) {
            return new DistUser(admin.getName(), admin.getId(), admin.getPwd(), true, true, true, true, authorities);
        } else {
            return null;
        }
    }

}
