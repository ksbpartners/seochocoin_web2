package kr.co.ksbpartners.seochocoin.repository;

import kr.co.ksbpartners.seochocoin.entity.CoinLeger;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface CoinLegerMapper {

    List<CoinLeger> selectAll(@Param("memberId") String memberId, @Param("centerId") String centerId);

    List<CoinLeger> selectByUsageId(String usageId, String fromDate, String toDate, String keyword);

    int deleteUsageMembers(String usageId, List<String> ids, String updatedBy);

    List<CoinLeger> getTotCoin(String activityId, String fromDate, String toDate);

    List<CoinLeger> selectCoinSumByType(Map<String, String> map);
}