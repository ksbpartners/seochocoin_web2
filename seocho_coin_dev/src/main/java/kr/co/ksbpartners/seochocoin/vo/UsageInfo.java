package kr.co.ksbpartners.seochocoin.vo;

import lombok.Data;

@Data
public class UsageInfo {
    private String id;
    private String name;
    private String type;
    private String area;
    private String useCoin;
    private String price;
    private String addr;
    private String createdAt;
    private String createdAtStr;

    public UsageInfo() {

    }

    public UsageInfo(String id, String name, String type, String area, String useCoin, String price, String addr, String createdAt, String createdAtStr) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.area = area;
        this.useCoin = useCoin;
        this.price = price;
        this.addr = addr;
        this.createdAt = createdAt;
        this.createdAtStr = createdAtStr;
    }
}
