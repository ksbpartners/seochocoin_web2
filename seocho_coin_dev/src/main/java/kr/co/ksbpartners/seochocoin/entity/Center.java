package kr.co.ksbpartners.seochocoin.entity;

import kr.co.ksbpartners.seochocoin.util.constraint.ValidPassword;
import lombok.Data;
import org.apache.ibatis.type.Alias;

import java.io.Serializable;

@Data
@Alias("center")
public class Center extends EntityBase implements Serializable {
    private static final long serialVersionUID = 1L;

    private String id;
    private String adminId;
    private String adminNm;
    @ValidPassword
    private String pwd; // for login

    private String name;
    private String area;
    private String type;
    private String postalCode;
    private String addr;
    private String description;

    private Integer activityCnt;
    private Integer memberCnt;

    public Center() {
    }

    public Center(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public Center(String id, String adminId, String name) {
        this.id = id;
        this.adminId = adminId;
        this.name = name;
    }

}
