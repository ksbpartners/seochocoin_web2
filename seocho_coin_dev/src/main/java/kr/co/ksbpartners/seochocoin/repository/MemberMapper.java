package kr.co.ksbpartners.seochocoin.repository;

import kr.co.ksbpartners.seochocoin.entity.Member;
import kr.co.ksbpartners.seochocoin.vo.MemberActivityVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface MemberMapper {

    Member getById(String id);

    Member getMaxId();

    List<Member> selectAll(String centerId, String keyword);

    List<Member> selectAll4Stat(String centerId, String fromDate, String toDate, String keyword);
    long countAll4Stat(String centerId, String fromDate, String toDate, String keyword);

    List<MemberActivityVO> getMemberActivity(String centerId, String activityId, String fromDate, String toDate, String keyword);

    int register(Member member);

    int modify(Member member);

    int delete(List<String> ids, String updatedBy);


}