package kr.co.ksbpartners.seochocoin.config;

import kr.co.ksbpartners.seochocoin.service.CenterUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebSecurity
@Order(10)
public class CentSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private CenterUserDetailsService centerUserDetailsService;

    @Autowired
    private AccessDeniedHandler centAccessDeniedHandler;

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        return bCryptPasswordEncoder;
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers(
                "/api/**",
                "/ncom/**",
                "/webjars/**",
                "/images/**",
                "/css/**",
                "/js/**",
                "/format/**"
        );
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http    .antMatcher("/**")
                .authorizeRequests()
                .antMatchers("/cent/login").permitAll()
                .antMatchers("/cent/**").hasRole("CENT")
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/cent/login") //ログインページはコントローラを経由しないのでViewNameとの紐付けが必要
                .loginProcessingUrl("/cent/sign_in") //フォームのSubmitURL、このURLへリクエストが送られると認証処理が実行される
                .usernameParameter("username") //リクエストパラメータのname属性を明示
                .passwordParameter("password")
                .successForwardUrl("/cent/index")
                .failureUrl("/cent/login?error")
                .permitAll()
                .and()
                .logout()
                .logoutUrl("/cent/logout")
                .logoutSuccessUrl("/cent/login?logout")
                .permitAll();

        http.exceptionHandling().accessDeniedHandler(centAccessDeniedHandler);
    }

    @Autowired
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
//        String password = passwordEncoder().encode("pass");
        auth.userDetailsService(centerUserDetailsService).passwordEncoder(passwordEncoder());
//        auth
//                .inMemoryAuthentication()
//                .withUser("cent1").password("{noop}pass").roles("CENT");
    }

}