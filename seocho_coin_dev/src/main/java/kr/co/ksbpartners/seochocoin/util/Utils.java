package kr.co.ksbpartners.seochocoin.util;

import ch.qos.logback.core.joran.util.StringToObjectConverter;
import kr.co.ksbpartners.seochocoin.constants.Constants;
import kr.co.ksbpartners.seochocoin.entity.Activity;
import kr.co.ksbpartners.seochocoin.entity.ActivitySchedule;
import kr.co.ksbpartners.seochocoin.entity.Member;
import kr.co.ksbpartners.seochocoin.vo.ActivityInfo;
import kr.co.ksbpartners.seochocoin.vo.ActivityScheduleVO;
import kr.co.ksbpartners.seochocoin.vo.BaseActivityInfo;

import javax.servlet.http.HttpServletRequest;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.StringTokenizer;

public class Utils {
    public static void REQUEST_PARAM_INFO(HttpServletRequest request) {
        System.out.println("<<<<<<<<");
        Enumeration<String> em = request.getParameterNames();
        while (em.hasMoreElements()) {
            String name = em.nextElement();
            String[] values = request.getParameterValues(name);
            System.out.println(name);
            for (String value : values) {
                System.out.println("\t" + value);
            }
        }
        System.out.println(">>>>>>>>");
    }

    public static String CONVERT_MOBILE1(String mobile) {
        if (mobile != null && !mobile.isEmpty() && mobile.indexOf("-") != -1) {
            return mobile.replaceAll("-", "");
        } else {
            return mobile;
        }
    }

    public static String CONVERT_MOBILE2(String mobile) {
        if (mobile != null && !mobile.isEmpty() && mobile.indexOf("-") == -1) {
            if (mobile.length() == 10) {
                return mobile.substring(0, 3) + "-" + mobile.substring(3, 6) + "-" + mobile.substring(6);
            } else if (mobile.length() == 11) {
                return mobile.substring(0, 3) + "-" + mobile.substring(3, 7) + "-" + mobile.substring(7);
            } else {
                return mobile;
            }
        } else {
            return mobile;
        }
    }

    public static void SET_KEYWORD(Member member) {
        StringTokenizer st = new StringTokenizer(member.getKeyword(), "|");
        int count = st.countTokens();
        if (count >= 1) {
            member.setKeyword1(st.nextToken());
        }
        if (count >= 2) {
            member.setKeyword2(st.nextToken());
        }
        if (count == 3) {
            member.setKeyword3(st.nextToken());
        }
    }

    public static void SET_CENTER(Member member) {
        StringTokenizer st = new StringTokenizer(member.getCenter(), "|");
        int count = st.countTokens();
        if (count >= 1) {
            member.setCenter1(st.nextToken());
        }
        if (count >= 2) {
            member.setCenter2(st.nextToken());
        }
        if (count == 3) {
            member.setCenter3(st.nextToken());
        }
    }

    public static List<String> SET_TIMETABLES(BaseActivityInfo activityInfo) {
        List<String> timeTables = new ArrayList<>();
        if (activityInfo.getMon() != null && Constants.YES.equals(activityInfo.getMon()))
            timeTables.add("월, " + activityInfo.getMonFrom() + " ~ " + activityInfo.getMonTo());
        if (activityInfo.getTue() != null && Constants.YES.equals(activityInfo.getTue()))
            timeTables.add("화, " + activityInfo.getTueFrom() + " ~ " + activityInfo.getTueTo());
        if (activityInfo.getWed() != null && Constants.YES.equals(activityInfo.getWed()))
            timeTables.add("수, " + activityInfo.getWedFrom() + " ~ " + activityInfo.getWedTo());
        if (activityInfo.getThu() != null && Constants.YES.equals(activityInfo.getThu()))
            timeTables.add("목, " + activityInfo.getThuFrom() + " ~ " + activityInfo.getThuTo());
        if (activityInfo.getFri() != null && Constants.YES.equals(activityInfo.getFri()))
            timeTables.add("금, " + activityInfo.getFriFrom() + " ~ " + activityInfo.getFriTo());

        return timeTables.isEmpty() ? null : timeTables;
    }

    public static LocalDate CHECK_DATEFORMAT(String date) {
        try {
            return LocalDate.parse(date, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        } catch (Exception ex) {
            if (date != null && date.length() == 8) {
                date = date.substring(0, 4) + "-" + date.substring(4, 6) + "-" + date.substring(6);
                return LocalDate.parse(date, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
            } else {
                return null;
            }
        }
    }

    public static String CHANGE_DATEFORMAT_HYPEN(String str) {
        if (str.length() == 8 && str.indexOf("-") == -1) {
            String _birth = str.substring(0, 4) + "-"
                    + str.substring(4, 6) + "-"
                    + str.substring(6);
            return _birth;
        } else {
            return str;
        }
    }

    public static String CHANGE_DATEFORMAT_NOHYPEN(String str) {
        if (str.length() == 10 && str.indexOf("-") != -1) {
            StringTokenizer st = new StringTokenizer(str, "-");

            return st.nextToken() + st.nextToken() + st.nextToken();
        } else {
            return str;
        }
    }

    public static List<ActivitySchedule> SET_ACTIVITY_SCHEDULE(Activity activity, ActivityScheduleVO schedule) {
        LocalDate now = LocalDate.now();
        LocalDate startDate = CHECK_DATEFORMAT(activity.getStartDate());
        // 오늘 보다 과거시각분은 무시
        if (startDate.isBefore(now)) {
            startDate = now;
        }
        LocalDate endDate = CHECK_DATEFORMAT(activity.getEndDate());

        List<ActivitySchedule> newScheduleList = new ArrayList<>();
        for (LocalDate date = startDate; date.isEqual(endDate) || date.isBefore(endDate); date = date.plusDays(1)) {
            if ("Y".equals(activity.getMon()) && DayOfWeek.MONDAY.equals(date.getDayOfWeek())) {
                for (String memId : schedule.getMonMemIds()) {
                    ActivitySchedule newSchedule = new ActivitySchedule();
                    newSchedule.setActivityId(activity.getId());
                    newSchedule.setMemberId(memId);
                    newSchedule.setActivityDate(date);
                    newSchedule.setDay("mon");
                    newSchedule.setStart(activity.getMonFrom());
                    newSchedule.setEnd(activity.getMonTo());
                    newSchedule.setCreatedBy(activity.getUpdatedBy());
                    newSchedule.setUpdatedBy(activity.getUpdatedBy());

                    newScheduleList.add(newSchedule);
                }
            } else if ("Y".equals(activity.getTue()) && DayOfWeek.TUESDAY.equals(date.getDayOfWeek())) {
                for (String memId : schedule.getTueMemIds()) {
                    ActivitySchedule newSchedule = new ActivitySchedule();
                    newSchedule.setActivityId(activity.getId());
                    newSchedule.setMemberId(memId);
                    newSchedule.setActivityDate(date);
                    newSchedule.setDay("tue");
                    newSchedule.setStart(activity.getTueFrom());
                    newSchedule.setEnd(activity.getTueTo());
                    newSchedule.setCreatedBy(activity.getUpdatedBy());
                    newSchedule.setUpdatedBy(activity.getUpdatedBy());

                    newScheduleList.add(newSchedule);
                }
            } else if ("Y".equals(activity.getWed()) && DayOfWeek.WEDNESDAY.equals(date.getDayOfWeek())) {
                for (String memId : schedule.getWedMemIds()) {
                    ActivitySchedule newSchedule = new ActivitySchedule();
                    newSchedule.setActivityId(activity.getId());
                    newSchedule.setMemberId(memId);
                    newSchedule.setActivityDate(date);
                    newSchedule.setDay("wed");
                    newSchedule.setStart(activity.getWedFrom());
                    newSchedule.setEnd(activity.getWedTo());
                    newSchedule.setCreatedBy(activity.getUpdatedBy());
                    newSchedule.setUpdatedBy(activity.getUpdatedBy());

                    newScheduleList.add(newSchedule);
                }
            } else if ("Y".equals(activity.getThu()) && DayOfWeek.THURSDAY.equals(date.getDayOfWeek())) {
                for (String memId : schedule.getThuMemIds()) {
                    ActivitySchedule newSchedule = new ActivitySchedule();
                    newSchedule.setActivityId(activity.getId());
                    newSchedule.setMemberId(memId);
                    newSchedule.setActivityDate(date);
                    newSchedule.setDay("thu");
                    newSchedule.setStart(activity.getThuFrom());
                    newSchedule.setEnd(activity.getThuTo());
                    newSchedule.setCreatedBy(activity.getUpdatedBy());
                    newSchedule.setUpdatedBy(activity.getUpdatedBy());

                    newScheduleList.add(newSchedule);
                }
            } else if ("Y".equals(activity.getFri()) && DayOfWeek.FRIDAY.equals(date.getDayOfWeek())) {
                for (String memId : schedule.getFriMemIds()) {
                    ActivitySchedule newSchedule = new ActivitySchedule();
                    newSchedule.setActivityId(activity.getId());
                    newSchedule.setMemberId(memId);
                    newSchedule.setActivityDate(date);
                    newSchedule.setDay("fri");
                    newSchedule.setStart(activity.getFriFrom());
                    newSchedule.setEnd(activity.getFriTo());
                    newSchedule.setCreatedBy(activity.getUpdatedBy());
                    newSchedule.setUpdatedBy(activity.getUpdatedBy());

                    newScheduleList.add(newSchedule);
                }
            }
        }

        return newScheduleList;
    }
}
