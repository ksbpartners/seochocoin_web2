package kr.co.ksbpartners.seochocoin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SeochocoinApplication {

    public static void main(String[] args) {
        SpringApplication.run(SeochocoinApplication.class, args);
    }

}
