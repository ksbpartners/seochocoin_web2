package kr.co.ksbpartners.seochocoin.service;

import kr.co.ksbpartners.seochocoin.constants.Constants;
import kr.co.ksbpartners.seochocoin.entity.*;
import kr.co.ksbpartners.seochocoin.repository.*;
import kr.co.ksbpartners.seochocoin.util.Utils;
import kr.co.ksbpartners.seochocoin.vo.*;
import kr.co.ksbpartners.seochocoin.util.excel.ExcelPOIUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
public class ActivityService extends AbstractService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private MemberMapper memberMapper;

    @Autowired
    private ActivityMapper activityMapper;

    @Autowired
    private ActivityScheduleMapper activityScheduleMapper;

    @Autowired
    private ApiActivityMapper apiActivityMapper;

    @Autowired
    private CoinLegerMapper coinLegerMapper;

    @Autowired
    private UsageService usageService;

    public Activity getActivityById(String activityId) {
        return activityMapper.getById(activityId);
    }

    public List<Activity> getActivityListByCenterId(String centerId) {
        return activityMapper.selectAllByCenterId(centerId, null, null, null);
    }

    public List<ActivityVO> getAll(String centerId, String fromDate, String toDate, String keyword) {
        List<ActivityVO> ret = new ArrayList<>();

        List<Activity>  actList = activityMapper.selectAllByCenterId(centerId, fromDate, toDate, keyword);
        for (Activity act : actList) {
            ActivityVO vo = new ActivityVO();
            vo.setActivity(act);
            vo.setMemberActivities(memberMapper.getMemberActivity(centerId, act.getId(), null, null, null));
            vo.setNumOfMembers(null != vo.getMemberActivities() ? vo.getMemberActivities().size() : 0);
            // calc coin used/added
            List<CoinLeger> tot = coinLegerMapper.getTotCoin(act.getId(), null, null);
            if (tot != null) {
                for (CoinLeger coin : tot) {
                    if (Constants.COIN_PLUS.equals(coin.getType())) {
                        vo.setCoinAdded(vo.getCoinAdded() + coin.getCoin());
                    } else if (Constants.COIN_MINUS.equals(coin.getType())) {
                        vo.setCoinAdded(vo.getCoinAdded() + coin.getCoin());
                    } else if (Constants.COIN_USED.equals(coin.getType())) {
                        vo.setCoinUsed(coin.getCoin());
                    }
                }
            }

            ret.add(vo);
        }

        return ret;
    }

    public List<MemberActivityVO> getMemberActivityList(String centerId, String activityId, String fromDate, String toDate, String keyword) {
        return memberMapper.getMemberActivity(centerId, activityId, fromDate, toDate, keyword);
    }


    public ByteArrayInputStream generateActivityListFile(List<Activity> list) throws IOException {
        logger.info("generate Excel file.");

        return ExcelPOIUtil.activityToExcel(list);
    }

    public ByteArrayInputStream genMemberActivityListFile(List<MemberActivityVO> list) throws IOException {
        logger.info("generate Excel file.");

        return ExcelPOIUtil.memberActivityToExcel(list);
    }

    public ByteArrayInputStream genMemberActivityScheduleListFile(List<ActivitySchedule> list) throws IOException {
        logger.info("generate Excel file.");

        return ExcelPOIUtil.memberActivityScheduleToExcel(list);
    }

    @Transactional
    public int uploadActivityListFile(Center cent, MultipartFile multipartFile) throws Exception {
        logger.info("load file data to db.");

        final String fileType = "activityList";

        StringBuffer filePath = new StringBuffer("/uploadfiles").append(File.separator).append(fileType);

        // file upload to server
        String fileName = getFileName(multipartFile.getOriginalFilename());
        File uploadDir = mkdirs(filePath);
        File uploadFile = new File(uploadDir.getPath() + "/" +  fileName);

        try (BufferedOutputStream uploadFileStream
                     = new BufferedOutputStream(new FileOutputStream(uploadFile))) {

            byte[] bytes = multipartFile.getBytes();
            uploadFileStream.write(bytes);

        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }

        // upload to data base
        if (!uploadFile.exists()) {
            throw new Exception("일괄등록 대상 파일을 읽을 수 없습니다.");
        }

        int uploadCnt;
        Map<Integer, List<CellVO>> map = ExcelPOIUtil.readExcel(uploadFile.getAbsolutePath());
        AtomicInteger cnt = new AtomicInteger(2);
        int sum = 0;
        for (Map.Entry<Integer, List<CellVO>> e : map.entrySet()) {
            if (e.getKey() != 0) {
                int i = insertIfNotExist(cnt.getAndIncrement(), cent, e.getValue());
                sum += i;
            }
        }
        uploadCnt = sum;

        return uploadCnt;
    }


    private int insertIfNotExist(int rowNum, Center cent, List<CellVO> cellVal) throws Exception {
        // TODO more handling
        if (StringUtils.isEmpty(cellVal.get(0).getContent())
            || StringUtils.isEmpty(cellVal.get(1).getContent())
            || StringUtils.isEmpty(cellVal.get(2).getContent())) {
                // 활동명
                // 활동유형
                // 카테고리
                throw new Exception(rowNum + "행의 1, 2, 3 열은 필수 항목입니다.");
        }

        // 컬럼갯수 포지션은 다운로드와 동일
        int pos = 0;
        Activity data = new Activity();
        try {
            // activityId
            // get id
            Activity entity = activityMapper.getMaxId();
            String newId = getNewId(Constants.PREFIX_ACTIVITY_ID, null == entity ? "" : entity.getId());
            data.setId(newId);
            // centerId
            data.setCenterId(cent.getId());

            // 활동명 2
            data.setName(cellVal.get(pos++).getContent());
            // 활동유형 3
            data.setType(cellVal.get(pos++).getContent());
            // 카테고리 4
            String category = cellVal.get(pos++).getContent();
            if (!StringUtils.isEmpty(category)) {
                category = category.split(":")[0];
            }
            data.setCategory(category);
            // 담당자
            data.setInCharge(cellVal.get(pos++).getContent());
            // 문의
            data.setContact(cellVal.get(pos++).getContent());
            // 상태
            data.setStatus(cellVal.get(pos++).getContent());
            // 지역
            data.setArea(cellVal.get(pos++).getContent());
            // 활동장소
            data.setMap(cellVal.get(pos++).getContent());
            // 활동내용
            data.setDetail(cellVal.get(pos++).getContent());
            // 시작일
            data.setStartDate(cellVal.get(pos++).getContent());
            // 종료일
            data.setEndDate(cellVal.get(pos++).getContent());

            // 활동횟수(일회성)(Y/N)
            data.setHasPeriod(Constants.YES);
            // Mon-Fri
            if (Constants.YES.equalsIgnoreCase(data.getHasPeriod())) {
                data.setMon(cvtFlag(cellVal.get(pos++).getContent()));
                data.setMonFrom(cvtTime(cellVal.get(pos++).getContent()));
                data.setMonTo(cvtTime(cellVal.get(pos++).getContent()));
                data.setTue(cvtFlag(cellVal.get(pos++).getContent()));
                data.setTueFrom(cvtTime(cellVal.get(pos++).getContent()));
                data.setTueTo(cvtTime(cellVal.get(pos++).getContent()));
                data.setWed(cvtFlag(cellVal.get(pos++).getContent()));
                data.setWedFrom(cvtTime(cellVal.get(pos++).getContent()));
                data.setWedTo(cvtTime(cellVal.get(pos++).getContent()));
                data.setThu(cvtFlag(cellVal.get(pos++).getContent()));
                data.setThuFrom(cvtTime(cellVal.get(pos++).getContent()));
                data.setThuTo(cvtTime(cellVal.get(pos++).getContent()));
                data.setFri(cvtFlag(cellVal.get(pos++).getContent()));
                data.setFriFrom(cvtTime(cellVal.get(pos++).getContent()));
                data.setFriTo(cvtTime(cellVal.get(pos++).getContent()));
            } else {
                data.setMon(Constants.NO);
                data.setTue(Constants.NO);
                data.setWed(Constants.NO);
                data.setThu(Constants.NO);
                data.setFri(Constants.NO);
                pos += 15; // move index
            }

            // 정원(명)
            data.setLimt(cellVal.get(pos++).getintVal());
            // 코인 사용(Y/N)
            data.setUseCoin(cvtFlag(cellVal.get(pos++).getContent()));
            // 2000
            data.setUseCoinVal(cellVal.get(pos++).getLongVal());
            // 코인 적립(Y/N)
            data.setAddCoin(cvtFlag(cellVal.get(pos++).getContent()));
            // 2000
            data.setAddCoinVal(cellVal.get(pos++).getLongVal());
            // 활동환료후 승인(Y/N)
            data.setAddCoinAfter(Constants.YES);
            // 출결확인 필요(Y/N)
            data.setAttendCheck(cvtFlag(cellVal.get(pos++).getContent()));
            // 80
            data.setAttendRate(cellVal.get(pos++).getLongVal());
            // 가격정보 사용(Y/N)
            data.setUsePriceInfo(cvtFlag(cellVal.get(pos++).getContent()));
            // 2500
            data.setPrice(cellVal.get(pos++).getLongVal());
            // 사용처 자동등록(Y/N)
            data.setAutoAddMarket(cvtFlag(cellVal.get(pos++).getContent()));
            // 게시(Y/N)
            data.setOpenActivity(cvtFlag(cellVal.get(pos++).getContent()));
        } catch (Exception e) {
            throw new Exception(rowNum + "행, " + pos + "열의 형식이 잘못 입력되어 업로드가 실패했습니다.");
        }

        //
        data.setCreatedBy("uploader");
        data.setUpdatedBy("uploader");
        data.setDelFlg(Constants.NO);

        int ret = 0;
        try {
            ret = activityMapper.register(data);
        } catch (Exception e) {
            throw new Exception(rowNum + "행의 형식이 잘못 입력되어 업로드가 실패했습니다.\n 상세내용: " + e.getMessage());
        }
        if ("Y".equals(data.getAutoAddMarket())) {
            // TODO 사용처 등록
            Usage usage = new Usage();
            usage.setCenterId(data.getCenterId());
            usage.setActivityId(data.getId());
            usage.setName(data.getName());
            usage.setInCharge(data.getInCharge());
            usage.setContact(data.getContact());
            usage.setType("교육");
            usage.setDetail(data.getDetail());
            usage.setArea(data.getArea());
            usage.setPrice(data.getPrice());
            usage.setUseCoin(data.getUseCoinVal());
            usage.setOpenActivity(data.getOpenActivity());
            usage.setCreatedBy(data.getCreatedBy());
            usage.setUpdatedBy(data.getUpdatedBy());
            try {
                usageService.add(usage);
            } catch (Exception e) {
                throw new Exception(rowNum + "행의 형식이 잘못 입력되어 업로드가 실패했습니다.\n 상세내용: " + e.getMessage());
            }
        }
        return ret;
    }

    /**
     * Y/N 아닌경우는 무시
     * @param contents
     * @return
     */
    private String cvtFlag(String contents) {
        String ret = "";
        if (!StringUtils.isEmpty(contents)) {
            ret = contents.trim();
            if (Constants.YES.equalsIgnoreCase(ret) || Constants.NO.equalsIgnoreCase(ret)) {
                ret = ret.toUpperCase();
            }

        }
        return ret;

    }

    /**
     * Time format
     * @param contents
     * @return
     */
    private String cvtTime(String contents) throws Exception {
        String ret = "";
        if (!StringUtils.isEmpty(contents)) {
            ret = contents.trim();
            ret = ret.substring(0, 5);
            Pattern p = Pattern.compile("^([0-1][0-9]|[2][0-3]):[0-5][0-9]$");
            Matcher m = p.matcher(ret);
            if ( !m.find() ) {
                throw new Exception();
            }
        }
        return ret;

    }

    @Transactional
    public String add(Activity activity) throws Exception {
        // timestamp
        LocalDateTime timeStamp = LocalDateTime.now();
        // get id
        Activity entity = activityMapper.getMaxId();
        String newId = getNewId(Constants.PREFIX_ACTIVITY_ID, null == entity ? "" : entity.getId());

        Activity newActivity = new Activity();
        BeanUtils.copyProperties(activity, newActivity);
        newActivity.setId(newId);
        newActivity.setCreatedAt(timeStamp);
        newActivity.setUpdatedAt(timeStamp);

        int ret = activityMapper.register(newActivity);
        if (ret != 1) {
            throw new Exception("Failed to generate new data.");
        }

        if ("Y".equals(newActivity.getAutoAddMarket())) {
            // TODO 사용처 등록
            Usage usage = new Usage();
            usage.setCenterId(newActivity.getCenterId());
            usage.setActivityId(newActivity.getId());
            usage.setName(newActivity.getName());
            usage.setInCharge(newActivity.getInCharge());
            usage.setContact(newActivity.getContact());
            usage.setType("교육");
            usage.setDetail(newActivity.getDetail());
            usage.setArea(newActivity.getArea());
            usage.setPrice(newActivity.getPrice());
            usage.setUseCoin(newActivity.getUseCoinVal());
            usage.setOpenActivity(newActivity.getOpenActivity());
            usage.setCreatedBy(newActivity.getCreatedBy());
            usage.setUpdatedBy(newActivity.getUpdatedBy());

            usageService.add(usage);
        }
        return newActivity.getId();
    }

    @Transactional
    public int modify(Activity activity) {
        int res = activityMapper.modify(activity);
        res += activityMapper.updateAccumCoin(activity.getId(), activity.getAddCoinVal(), activity.getUpdatedBy());

        return res;
    }

    @Transactional
    public int remove(List<String> ids, String updatedBy) {
        // timestamp
//        LocalDateTime timeStamp = LocalDateTime.now();
//        activity.setUpdatedAt(timeStamp);

        // delete activity
        int res = activityMapper.delete(ids, updatedBy);

        // delete member activity
        res += activityMapper.deleteMemberActivities(ids, updatedBy);

        // delete activity schedule
        res += activityScheduleMapper.deleteActivitiesSchedules(ids, updatedBy);

        return res;
    }

    @Transactional
    public int removeMembers(String activityId, List<String> ids, String updatedBy) {

        // delete member activity
        int res = activityMapper.deleteActivityMembers(activityId, ids, updatedBy);

        // delete activity schedule
        res += activityScheduleMapper.deleteActivityMembersSchedules(activityId, ids, updatedBy);

        return res;
    }

    @Transactional
    public int removeActivityMemberAndSchedule(String activityId, String memberId, String updatedBy) {

        // delete member activity
        int res = activityMapper.deleteActivityMember(activityId, memberId);

        // delete activity schedule
        res += activityScheduleMapper.deleteActivityMembersSchedule(activityId, memberId);

        return res;
    }

    public ActivityScheduleVO getDaySchedule(String activityId) {
        ActivityScheduleVO vo = new ActivityScheduleVO();
        vo.setActivity(activityMapper.getById(activityId));

        List<ActivitySchedule> schedules =  activityScheduleMapper.selectMembersByActivityId(activityId);
        vo.setActivitySchedules(schedules);

        List<String> monMemIds = new ArrayList<>();
        List<String> tueMemIds = new ArrayList<>();
        List<String> wedMemIds = new ArrayList<>();
        List<String> thuMemIds = new ArrayList<>();
        List<String> friMemIds = new ArrayList<>();

        List<String> monMemNms = new ArrayList<>();
        List<String> tueMemNms = new ArrayList<>();
        List<String> wedMemNms = new ArrayList<>();
        List<String> thuMemNms = new ArrayList<>();
        List<String> friMemNms = new ArrayList<>();
        for (ActivitySchedule as : schedules) {
            if ("mon".equals(as.getDay())) {
                monMemIds.add(as.getMemberId());
                monMemNms.add(as.getMemberName());
            } else if ("tue".equals(as.getDay())) {
                tueMemIds.add(as.getMemberId());
                tueMemNms.add(as.getMemberName());
            } else if ("wed".equals(as.getDay())) {
                wedMemIds.add(as.getMemberId());
                wedMemNms.add(as.getMemberName());
            } else if ("thu".equals(as.getDay())) {
                thuMemIds.add(as.getMemberId());
                thuMemNms.add(as.getMemberName());
            } else if ("fri".equals(as.getDay())) {
                friMemIds.add(as.getMemberId());
                friMemNms.add(as.getMemberName());
            }
        }
        vo.setMonMemIds(monMemIds);
        vo.setMonMemNms(monMemNms);
        vo.setTueMemIds(tueMemIds);
        vo.setTueMemNms(tueMemNms);
        vo.setWedMemIds(wedMemIds);
        vo.setWedMemNms(wedMemNms);
        vo.setThuMemIds(thuMemIds);
        vo.setThuMemNms(thuMemNms);
        vo.setFriMemIds(friMemIds);
        vo.setFriMemNms(friMemNms);
        return vo;
    }

    @Transactional
    public int modifySchedule(ActivityScheduleVO schedule) {
        Activity activity = schedule.getActivity();
        // timestamp
        LocalDateTime timeStamp = LocalDateTime.now();
        activity.setUpdatedAt(timeStamp);

        // 활동 갱신
        int res = activityMapper.updateSchedule(activity);

        // 활동 사용자 갱신
        // 기존 등록 활동 있고, 스케줄 없으면 활동중지.
        activityMapper.updateOldMemStatus(activity.getId());

        // 활동 등록 안되어 있고, 스케줄 있으면 신규등록
        List<String> memIdList = new ArrayList<>();
        memIdList.addAll(schedule.getMonMemIds());
        memIdList.addAll(schedule.getTueMemIds());
        memIdList.addAll(schedule.getWedMemIds());
        memIdList.addAll(schedule.getThuMemIds());
        memIdList.addAll(schedule.getFriMemIds());
        memIdList = memIdList.stream().distinct().collect(Collectors.toList());
        for (String memId : memIdList) {
            MemberActivityVO maVo = new MemberActivityVO();
            maVo.setMemberId(memId);
            maVo.setCenterId(activity.getCenterId());
            maVo.setActivityId(activity.getId());
            maVo.setActivityStatus("활동중");
//            maVo.setAccumStatus("Y".equals(activity.getAddCoin()) ? "적립" : "적립취소");
//            maVo.setAccumCoin("Y".equals(activity.getAddCoin()) ? activity.getAddCoinVal() : 0);
            maVo.setAccumCoin(activity.getAddCoinVal());
            maVo.setCreatedBy(activity.getUpdatedBy());
            maVo.setUpdatedBy(activity.getUpdatedBy());

            activityMapper.upsertNewMember(maVo);
        }

        // 활동 스케줄 갱신
        res += activityScheduleMapper.deleteOldSchedule(activity.getId());

        List<ActivitySchedule> newScheduleList = Utils.SET_ACTIVITY_SCHEDULE(activity, schedule);

        newScheduleList.forEach(s -> {
            activityScheduleMapper.register(s);
        });

        return res;
    }

    @Transactional
    public int modifyScheduleOneShot(String activityId, List<String> memIdList, String oneShotDate, String updatedBy) {
        int res = 0;
        // 활동 등록 안되어 있고, 스케줄 있으면 신규등록
        Activity activity = activityMapper.getById(activityId);
        for (String memId : memIdList) {
            MemberActivityVO maVo = new MemberActivityVO();
            maVo.setMemberId(memId);
            maVo.setCenterId(activity.getCenterId());
            maVo.setActivityId(activity.getId());
            maVo.setActivityStatus("활동중");
            maVo.setAccumCoin(activity.getAddCoinVal());
            maVo.setCreatedBy(updatedBy);
            maVo.setUpdatedBy(updatedBy);

            res = activityMapper.upsertNewMember(maVo);
        }

        // 활동 스케줄 갱신
        LocalDate targetDate = LocalDate.parse(oneShotDate, DateTimeFormatter.ofPattern("yyyy-MM-dd"));

        List<ActivitySchedule> newScheduleList = new ArrayList<>();
        for (String memId : memIdList) {
            ActivitySchedule newSchedule = new ActivitySchedule();
            newSchedule.setActivityId(activity.getId());
            newSchedule.setMemberId(memId);
            newSchedule.setActivityDate(targetDate);
            newSchedule.setHasPeriod("N");
            if (DayOfWeek.MONDAY.equals(targetDate.getDayOfWeek())) {
                newSchedule.setDay("mon");
                newSchedule.setStart(activity.getMonFrom());
                newSchedule.setEnd(activity.getMonTo());
            } else if (DayOfWeek.TUESDAY.equals(targetDate.getDayOfWeek())) {
                newSchedule.setDay("tue");
                newSchedule.setStart(activity.getTueFrom());
                newSchedule.setEnd(activity.getTueTo());
            } else if (DayOfWeek.WEDNESDAY.equals(targetDate.getDayOfWeek())) {
                newSchedule.setDay("wed");
                newSchedule.setStart(activity.getWedFrom());
                newSchedule.setEnd(activity.getWedTo());
            } else if (DayOfWeek.THURSDAY.equals(targetDate.getDayOfWeek())) {
                newSchedule.setDay("thu");
                newSchedule.setStart(activity.getThuFrom());
                newSchedule.setEnd(activity.getThuTo());
            } else if (DayOfWeek.FRIDAY.equals(targetDate.getDayOfWeek())) {
                newSchedule.setDay("fri");
                newSchedule.setStart(activity.getFriFrom());
                newSchedule.setEnd(activity.getFriTo());
            }
            newSchedule.setCreatedBy(updatedBy);
            newSchedule.setUpdatedBy(updatedBy);

            newScheduleList.add(newSchedule);
        }
        newScheduleList.forEach(s -> activityScheduleMapper.register(s));

        return res;
    }

    public int changeMemberActivityStatus(String memberId, String activityId, String activityStatus, String updatedBy) {

        return activityMapper.changeMemberActivityStatus(memberId, activityId, activityStatus, updatedBy);
    }

    @Transactional
    public int changeMemberAccumStatus(String memberId, String activityId, String accumStatus, String updatedBy) {

        int res = activityMapper.changeMemberAccumStatus(memberId, activityId, accumStatus, updatedBy);

        Activity activity = apiActivityMapper.getAddCoinInfo(activityId);
        CoinLeger coinLeger = new CoinLeger();
        coinLeger.setCenterId(activity.getCenterId());
        coinLeger.setActivityId(activityId);
        coinLeger.setMemberId(memberId);
        coinLeger.setUpdatedBy(updatedBy);

        Member member = new Member();
        member.setCoin(activity.getAddCoinVal());
        member.setId(memberId);
        member.setUpdatedBy(updatedBy);

        if ("OK".equals(accumStatus)) {
            coinLeger.setType("적립");
            coinLeger.setCoin(activity.getAddCoinVal());
            coinLeger.setPaid(activity.getAddCoinVal() * Constants.BASE_COIN_PRICE);
            coinLeger.setCreatedBy(updatedBy);

            res += apiActivityMapper.addCoinLedger(coinLeger);
            res += apiActivityMapper.updateCoinOfMember(member);
        } else if ("NG".equals(accumStatus)) {
            res += apiActivityMapper.delCoinLedger(coinLeger);
            res += apiActivityMapper.minusCoinOfMember(member);
        }
        return res;
    }

    public List<ActivityVO> getAll4Stat(String centerId, String fromDate, String toDate, String keyword) {
        List<Activity> activities = activityMapper.selectAll4Stat(centerId, fromDate, toDate, keyword);

        List<ActivityVO> lst = new ArrayList<>();
        long count = countAll4Stat(centerId, fromDate, toDate, keyword);
        if (null != activities && activities.size() > 0) {
            Map<String, String> map = new HashMap<>();
            for (Activity act : activities) {
                map.clear();
                map.put("activityId", act.getId());
                ActivityVO vo = new ActivityVO();
                vo.setActivity(act);
                List<CoinLeger> coinLegersList = coinLegerMapper.selectCoinSumByType(map);
                if (coinLegersList != null && !coinLegersList.isEmpty()) {
                    coinLegersList.forEach(coin -> {
                        if (Constants.COIN_PLUS.equals(coin.getType())) {
                            vo.setCoinPlusSum(vo.getCoinPlusSum() + coin.getCoinSum());
                        } else if (Constants.COIN_MINUS.equals(coin.getType())) {
                            vo.setCoinPlusSum(vo.getCoinPlusSum() + coin.getCoinSum());
                        } else if (Constants.COIN_USED.equals(coin.getType())) {
                            vo.setCoinMinusSum(coin.getCoinSum());
                        }
                        vo.setUpdatedAt(coin.getUpdatedAt());
                    });
                }

                if (vo.getCoinPlusSum() == 0 && vo.getCoinMinusSum() == 0) {
                    count--;
                    continue;
                }

                vo.setNo(count--);
                lst.add(vo);
            }
        }

        return lst;
    }

    public long countAll4Stat(String centerId, String fromDate, String toDate, String keyword) {
        return activityMapper.countAll4Stat(centerId, fromDate, toDate, keyword);
    }

    public List<ActivitySchedule> cntMembersByActivityId(String activityId) {
        return activityScheduleMapper.cntMembersByActivityId(activityId);
    }

    public List<ActivitySchedule> getMembersByActivityDate(String activityId, String activityDate) {
        return activityScheduleMapper.selectMembersByActivityDate(activityId, activityDate);
    }

    public int activityScheduleAttend(String memberId, String activityId, String activityDate, String updatedBy) {
        return activityScheduleMapper.updateScheduleAttend(memberId, activityId, activityDate, updatedBy);
    }

    public int cancelScheduleAttend(String memberId, String activityId, String activityDate, String updatedBy) {
        return activityScheduleMapper.cancelScheduleAttend(memberId, activityId, activityDate, updatedBy);
    }

    public int activityScheduleDelete(String memberId, String activityId, String activityDate, String updatedBy) {
        return activityScheduleMapper.deleteActivitiesSchedule(memberId, activityId, activityDate, updatedBy);
    }
}