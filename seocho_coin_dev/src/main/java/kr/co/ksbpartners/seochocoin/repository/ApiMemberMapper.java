package kr.co.ksbpartners.seochocoin.repository;

import kr.co.ksbpartners.seochocoin.entity.Admin;
import kr.co.ksbpartners.seochocoin.entity.Member;
import kr.co.ksbpartners.seochocoin.vo.ActivityInfo;
import kr.co.ksbpartners.seochocoin.vo.MyActivityInfo;
import kr.co.ksbpartners.seochocoin.vo.MyCoinInfo;
import kr.co.ksbpartners.seochocoin.vo.UsageInfo;
import org.apache.ibatis.annotations.Mapper;
import org.mybatis.spring.boot.autoconfigure.MybatisAutoConfiguration;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface ApiMemberMapper {

    Member getTokenById(String uid);

    List<Member> getTokenByMobile(String mobile);

    List<Member> checkMobile4Modify(String uid, String mobile);

    Member getById(String uid);

    Member loginByMobile(String mobile);

    int updateToken(Member member);

    int updatePwd(Member member);

    Member getRankingById(String uid);

    Member getRankingAll(String uid);

    List<MyActivityInfo> getActivitiesByUid(String uid);

    MyActivityInfo getActivityInfoById(String uid, String aid);

    List<UsageInfo> getAllUsageList();

    UsageInfo getAllUsageListById(String id);

    List<MyCoinInfo> getCoinHistoryByUid(String uid);

    MyCoinInfo getCoinInfoById(String id);

    Member getMaxId();

    int register(Member member);

    int modify(Member member);

    int modifyCenter(Member member);

    int modifyKeyword(Member member);

    int delete(Member member);
}