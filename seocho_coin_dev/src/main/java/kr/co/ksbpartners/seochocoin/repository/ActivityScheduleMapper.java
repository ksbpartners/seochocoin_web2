package kr.co.ksbpartners.seochocoin.repository;

import kr.co.ksbpartners.seochocoin.entity.Activity;
import kr.co.ksbpartners.seochocoin.entity.ActivitySchedule;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface ActivityScheduleMapper {

    List<ActivitySchedule> selectMembersByActivityId(String activityId);

    List<ActivitySchedule> selectAllByMemberId(String memberId, String activityId, String fromDate, String toDate);

    int register(ActivitySchedule schedule);

    int update(ActivitySchedule schedule);

    int delete(ActivitySchedule schedule);

    int deleteOldSchedule(String activityId);

    int deleteMembersSchedules(List<String>ids, String updatedBy);

    int deleteActivitiesSchedules(List<String>ids, String updatedBy);

    int deleteActivitiesSchedulesByCenterId(List<String>ids, String updatedBy);

    int deleteActivityMembersSchedules(String activityId, List<String>ids, String updatedBy);

    int deleteActivityMembersSchedule(String activityId, String memberId);

    List<ActivitySchedule> cntMembersByActivityId(String activityId);

    List<ActivitySchedule> selectMembersByActivityDate(String activityId, String activityDate);

    int updateScheduleAttend(String memberId, String activityId, String activityDate, String updatedBy);

    int cancelScheduleAttend(String memberId, String activityId, String activityDate, String updatedBy);

    int deleteActivitiesSchedule(String memberId, String activityId, String activityDate, String updatedBy);
}