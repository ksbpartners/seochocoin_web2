package kr.co.ksbpartners.seochocoin.entity;

import lombok.Data;
import org.apache.ibatis.type.Alias;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.Date;

@Data
@Alias("admin")
public class Admin extends EntityBase implements Serializable {
    private static final long serialVersionUID = 1L;

    private String id;
    @NotEmpty
    private String name;
    private String pwd;
    private String type;
    private String centerId;

    private String birth;
    private String sex;
    private String phone;
    private String mobile;
    private String email;
    private String postalCode;
    private String addr;
    private String addrDetail;


    public Admin() {
    }

    public Admin(String id, String name) {
        this.id = id;
        this.name = name;
    }

}
