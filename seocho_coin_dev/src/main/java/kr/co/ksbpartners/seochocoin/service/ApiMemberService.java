package kr.co.ksbpartners.seochocoin.service;

import kr.co.ksbpartners.seochocoin.constants.Constants;
import kr.co.ksbpartners.seochocoin.entity.Admin;
import kr.co.ksbpartners.seochocoin.entity.Member;
import kr.co.ksbpartners.seochocoin.repository.AdminMapper;
import kr.co.ksbpartners.seochocoin.repository.ApiMemberMapper;
import kr.co.ksbpartners.seochocoin.vo.ActivityInfo;
import kr.co.ksbpartners.seochocoin.vo.MyActivityInfo;
import kr.co.ksbpartners.seochocoin.vo.MyCoinInfo;
import kr.co.ksbpartners.seochocoin.vo.UsageInfo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class ApiMemberService extends AbstractService {

    @Autowired
    private ApiMemberMapper mapper;

    public Member getTokenById(String uid) {
        return mapper.getTokenById(uid);
    }

    public List<Member> getTokenByMobile(String mobile) {
        return mapper.getTokenByMobile(mobile);
    }

    public List<Member> checkMobile4Modify(String uid, String mobile) {
        return mapper.checkMobile4Modify(uid, mobile);
    }

    public int updateToken(Member member) {
        return mapper.updateToken(member);
    }

    public int updatePwd(Member member) {
        return mapper.updatePwd(member);
    }

    public Member getById(String uid) {
        return mapper.getById(uid);
    }

    public Member loginByMobile(String mobile) {
        return mapper.loginByMobile(mobile);
    }

    public Member getRankingById(String uid) {
        return mapper.getRankingById(uid);
    }

    public Member getRankingAll(String uid) {
        return mapper.getRankingAll(uid);
    }

    public List<MyActivityInfo> getActivitiesByUid(String uid) {
        return mapper.getActivitiesByUid(uid);
    }

    public MyActivityInfo getActivityInfoById(String uid, String aid) {
        return mapper.getActivityInfoById(uid, aid);
    }

    public List<UsageInfo> getAllUsageList() {
        return mapper.getAllUsageList();
    }

    public UsageInfo getAllUsageListById(String id) {
        return mapper.getAllUsageListById(id);
    }

    public List<MyCoinInfo> getCoinHistoryByUid(String uid) {
        return mapper.getCoinHistoryByUid(uid);
    }

    public MyCoinInfo getCoinInfoById(String id) {
        return mapper.getCoinInfoById(id);
    }

    public int modify(Member member) {
        // timestamp
        LocalDateTime timeStamp = LocalDateTime.now();
        member.setUpdatedAt(timeStamp);

        return mapper.modify(member);
    }

    public int modifyCenter(Member member) {
        // timestamp
        LocalDateTime timeStamp = LocalDateTime.now();
        member.setUpdatedAt(timeStamp);

        return mapper.modifyCenter(member);
    }

    public int modifyKeyword(Member member) {
        // timestamp
        LocalDateTime timeStamp = LocalDateTime.now();
        member.setUpdatedAt(timeStamp);

        return mapper.modifyKeyword(member);
    }
}