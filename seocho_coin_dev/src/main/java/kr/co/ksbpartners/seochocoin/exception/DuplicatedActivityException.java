package kr.co.ksbpartners.seochocoin.exception;

import org.springframework.dao.DuplicateKeyException;

public class DuplicatedActivityException extends DuplicateKeyException {
    public DuplicatedActivityException(String msg) {
        super(msg);
    }

    public DuplicatedActivityException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
