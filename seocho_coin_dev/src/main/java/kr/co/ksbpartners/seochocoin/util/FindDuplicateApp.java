package kr.co.ksbpartners.seochocoin.util;

import java.sql.*;
import java.util.*;

public class FindDuplicateApp {
    public static void main(String[] args) {
        FindDuplicateApp app = new FindDuplicateApp();
        app.start();
    }

    public void start() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }

        int totalRows = 0;
        try (Connection conn = DriverManager.getConnection("jdbc:mysql://182.162.27.8:3306/secho_db_dev?serverTimezone=JST",
                "seocho_coin", "Astyle12!")) {
            PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM members WHERE del_flg='N'");
            ResultSet rs = pstmt.executeQuery();
            Map<String, Integer> mm = new HashMap<>();
            while (rs.next()) {
                String mobile = rs.getString("mobile");
                if (mobile.indexOf("-") > -1) {
                    StringTokenizer st = new StringTokenizer(mobile, "-");
                    mobile = "";
                    try {
                        while (st.hasMoreTokens()) {
                            mobile += st.nextToken();
                        }
                    } catch (NoSuchElementException ex) {
                        System.err.println(ex.getMessage());
                        System.out.println("====>" + mobile);
                    }
                }

                Integer count = Integer.valueOf(0);
                if (mm.containsKey(mobile)) {
                    count = mm.get(mobile);
                }

                mm.put(mobile, ++count);

                totalRows++;
            }

            Iterator<String> it = mm.keySet().iterator();
            int sum = 0;
            while (it.hasNext()) {
                String mobile = it.next();
                Integer count = mm.get(mobile);
                if (count > 1) {
                    System.out.println(mobile + "=" + count);
                }

                sum += count;
            }
            System.out.println("-------------");
            System.out.println("SUM=" + sum);
        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
