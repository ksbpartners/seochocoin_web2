package kr.co.ksbpartners.seochocoin.controller;

import kr.co.ksbpartners.seochocoin.entity.Admin;
import kr.co.ksbpartners.seochocoin.entity.Center;
import kr.co.ksbpartners.seochocoin.service.AdminService;
import kr.co.ksbpartners.seochocoin.service.CenterService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.*;

@Controller
@RequestMapping("/dist")
public class DistCenterController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private AdminService adminService;

    @Autowired
    private CenterService centerService;


    @GetMapping("/center-list")
    public String centerList(@RequestParam(required = false) String keyword,
                             @RequestParam(required = false) String fromDate,
                             @RequestParam(required = false) String toDate,
                             @RequestParam(required = false) String type, Model model) {
        logger.info("센터 > 센터 목록. keyword={} fromDate={} toDate={}", keyword, fromDate, toDate);

        List<Center> centers = centerService.getAll(keyword, fromDate, toDate, type);
        Map<String, Object> types = centerService.groupByType(null, null, null, null);

        model.addAttribute("types", types);
        model.addAttribute("centers", centers);
        model.addAttribute("keyword", keyword);
        model.addAttribute("fromDate", fromDate);
        model.addAttribute("toDate", toDate);
        model.addAttribute("type", type);
        return "dist/center-list";
    }

    @GetMapping("/center-download")
    public ResponseEntity<InputStreamResource> centerExcelDownload(@RequestParam(required = false) String keyword,
                                                                   @RequestParam(required = false) String fromDate,
                                                                   @RequestParam(required = false) String toDate,
                                                                   @RequestParam(required = false) String type, Model model)
            throws IOException {
        logger.info("센터 > 센터 목록 > 엑셀 다운로드");

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        List<Center> centers = centerService.getAll(keyword, fromDate, toDate, type);

        ByteArrayInputStream in = centerService.generateCenterListFile(centers);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "attachment; filename=centerList.xlsx");

        return ResponseEntity
                .ok()
                .headers(headers)
                .body(new InputStreamResource(in));
    }

    @GetMapping("/center-register")
    public String centerDetail(@RequestParam(required = false) String id, Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String adminId = auth.getName();

        Center center = centerService.getById(id);

        model.addAttribute("center", center);
        return "dist/center-register";
    }

    @GetMapping("/center-register-edit")
    public String centerUpdate(@RequestParam(required = false) String id, Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        Center center = centerService.getById(id);
        List<Admin> admins = adminService.getAll();

        model.addAttribute("center", center);
        model.addAttribute("admins", admins);
        return "dist/center-register-edit";
    }

    @PostMapping("/center-register-edit")
    public String updateCenter(@ModelAttribute("center") @Validated Center center,
                               BindingResult bindingResult, Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (bindingResult.hasErrors()) {
            List<Admin> admins = adminService.getAll();
            model.addAttribute("admins", admins);
            return "dist/center-register-edit";
        }
        String adminId = auth.getName();

        center.setUpdatedBy(adminId);
        centerService.modify(center);

        return centerDetail(center.getId(), model);
    }

    @GetMapping("/center-search")
    @ResponseBody
    public String centerSearch(@RequestParam("centerName") String name, Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        Center center = centerService.getByName(name);
        String res = "";
        if (center != null) {
            res = center.getName();
        }
        return res;
    }

    @GetMapping("/center-register-add")
    public String centerAdd(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        Center center = new Center();
        List<Admin> admins = adminService.getAll();

        model.addAttribute("center", center);
        model.addAttribute("admins", admins);
        return "dist/center-register-add";
    }

    @PostMapping("/center-register-add")
    public String addCenter(@ModelAttribute("center") @Validated Center center,
                            BindingResult bindingResult,
                            @RequestParam("continue") String isContinue, Model model) throws Exception {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (bindingResult.hasErrors()) {
            List<Admin> admins = adminService.getAll();
            model.addAttribute("admins", admins);
            return "dist/center-register-add";
        }
        String adminId = auth.getName();

        center.setCreatedBy(adminId);
        center.setUpdatedBy(adminId);
        center.setId(centerService.add(center));

        if ("Y".equals(isContinue)) {
            return centerAdd(model);
        } else {
            return centerDetail(center.getId(), model);
        }
    }

    @PostMapping("/center-delete")
    public String deleteActivity(@RequestParam List<String> delIds,
                                 @RequestParam(required = false) String keyword,
                                 @RequestParam(required = false) String fromDate,
                                 @RequestParam(required = false) String toDate,
                                 @RequestParam(required = false) String type, Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        // ファイルが空の場合は異常終了
        if (delIds.isEmpty()) {
            // TODO error
        }

        centerService.remove(delIds, auth.getName());

        return centerList(keyword, fromDate, toDate, type, model);
    }

}
