package kr.co.ksbpartners.seochocoin.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class NcomInfo implements Serializable {
    private static final long serialVersionUID = 1L;

    private String error;
    private String token;
    private String name;
    private String center_id;
    private String mobile;
    private long coin_amount;
    private long total_amount;
    private String tx_date;
    private String tx_id;
    private List<NcomInfoTxContents> tx_contents;

    private long balance;

    public NcomInfo() {
    }

    public NcomInfo(String error) {
        this.error = error;
    }

    public NcomInfo(String token, String name, String mobile) {
        this.token = token;
        this.name = name;
        this.mobile = mobile;
    }

    public NcomInfo(String name, String mobile, long coin) {
        this.name = name;
        this.mobile = mobile;
        this.balance = coin;
    }

    public NcomInfo(String token, String name, String mobile, long balance) {
        this.token = token;
        this.name = name;
        this.mobile = mobile;
        this.balance = balance;
    }

    public String getCenter_id() {
        return center_id;
    }

    public void setCenter_id(String center_id) {
        this.center_id = center_id;
    }

    public long getCoin_amount() {
        return coin_amount;
    }

    public void setCoin_amount(long coin_amount) {
        this.coin_amount = coin_amount;
    }

    public String getTx_id() {
        return tx_id;
    }

    public void setTx_id(String tx_id) {
        this.tx_id = tx_id;
    }
}