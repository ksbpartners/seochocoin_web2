package kr.co.ksbpartners.seochocoin.service;

import kr.co.ksbpartners.seochocoin.entity.*;
import kr.co.ksbpartners.seochocoin.exception.DuplicatedActivityException;
import kr.co.ksbpartners.seochocoin.repository.ActivityMapper;
import kr.co.ksbpartners.seochocoin.repository.ActivityScheduleMapper;
import kr.co.ksbpartners.seochocoin.repository.ApiActivityMapper;
import kr.co.ksbpartners.seochocoin.repository.ApiMemberMapper;
import kr.co.ksbpartners.seochocoin.vo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.SQLIntegrityConstraintViolationException;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class ApiActivityService extends AbstractService {

    @Autowired
    private ApiMemberMapper mapper;

    @Autowired
    private ApiActivityMapper apiActivityMapper;

    @Autowired
    private ActivityScheduleMapper activityScheduleMapper;

    public int registMember(MemberActivityVO vo) throws DuplicatedActivityException {
        // timestamp
        LocalDateTime timeStamp = LocalDateTime.now();
        vo.setCreatedAt(timeStamp);
        vo.setUpdatedAt(timeStamp);

        return apiActivityMapper.registMember(vo);
    }

    public void registActivitySchedule(List<ActivitySchedule> newScheduleList) {
        newScheduleList.forEach(s -> activityScheduleMapper.register(s));
    }

    public int cancelMember(MemberActivityVO vo) {
        cancelActivitySchedule(vo);
        return apiActivityMapper.cancelMember(vo);
    }

    public int cancelActivitySchedule(MemberActivityVO vo) {
        return apiActivityMapper.cancelActivitySchedule(vo);
    }

    public List<ActivityInfo> getAllActivities(ActivityInfo activityInfo) {
        return apiActivityMapper.getAllActivities(activityInfo);
    }

    public ActivityInfo getAllActivitiesById(String aid) {
        return apiActivityMapper.getAllActivitiesById(aid);
    }

    public ActivitySchedule getAttendActivity(ActivitySchedule activitySchedule) {
        return apiActivityMapper.getAttendActivity(activitySchedule);
    }

    public int setAttendActivity(ActivitySchedule activitySchedule) {
        LocalDateTime timeStamp = LocalDateTime.now();
        activitySchedule.setUpdatedAt(timeStamp);
        return apiActivityMapper.setAttendActivity(activitySchedule);
    }

    public Activity getAddCoinInfo(String activityId) {
        return apiActivityMapper.getAddCoinInfo(activityId);
    }

    public int addCoinLedger(CoinLeger coinLeger) {
        LocalDateTime timeStamp = LocalDateTime.now();
        coinLeger.setCreatedAt(timeStamp);
        coinLeger.setUpdatedAt(timeStamp);

        return apiActivityMapper.addCoinLedger(coinLeger);
    }

    @Transactional
    public int addCoinUsage(CoinUsage coinUsage) {
        LocalDateTime timeStamp = LocalDateTime.now();
        coinUsage.setCreatedAt(timeStamp);
        coinUsage.setUpdatedAt(timeStamp);

        return apiActivityMapper.addCoinUsage(coinUsage);
    }

    public int updateCoinOfMember(Member member) {
        LocalDateTime timeStamp = LocalDateTime.now();
        member.setUpdatedAt(timeStamp);

        return apiActivityMapper.updateCoinOfMember(member);
    }

    @Transactional
    public int delCoinUsage(CoinUsage coinUsage) {
        return apiActivityMapper.delCoinUsage(coinUsage);
    }

    @Transactional
    public int minusCoinOfMember(Member member) {
        LocalDateTime timeStamp = LocalDateTime.now();
        member.setUpdatedAt(timeStamp);

        return apiActivityMapper.minusCoinOfMember(member);
    }

    public List<CoinUsage> getCoinUsageInfo(CoinUsage coinUsage) {
        return apiActivityMapper.getCoinUsageInfo(coinUsage);
    }
}