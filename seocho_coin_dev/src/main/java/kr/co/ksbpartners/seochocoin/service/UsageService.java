package kr.co.ksbpartners.seochocoin.service;

import kr.co.ksbpartners.seochocoin.constants.Constants;
import kr.co.ksbpartners.seochocoin.entity.*;
import kr.co.ksbpartners.seochocoin.repository.*;
import kr.co.ksbpartners.seochocoin.util.excel.ExcelPOIUtil;
import kr.co.ksbpartners.seochocoin.vo.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

@Service
public class UsageService extends AbstractService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private UsageMapper usageMapper;

    @Autowired
    private CoinLegerMapper coinLegerMapper;

    public Usage getById(String id) {
        return usageMapper.getById(id);
    }

    @Transactional
    public String add(Usage usage) throws Exception {
        // timestamp
        LocalDateTime timeStamp = LocalDateTime.now();
        // get id
        Usage entity = usageMapper.getMaxId();
        String newId = getNewId(Constants.PREFIX_USAGE_ID, null == entity ? "" : entity.getId());

        Usage newUsage = new Usage();
        BeanUtils.copyProperties(usage, newUsage);
        newUsage.setId(newId);
        newUsage.setCreatedAt(timeStamp);
        newUsage.setUpdatedAt(timeStamp);

        int ret = usageMapper.register(newUsage);
        if (ret != 1) {
            throw new Exception("Failed to generate new data.");
        }

        return newUsage.getId();
    }

    public int modify(Usage usage) {
        // timestamp
        LocalDateTime timeStamp = LocalDateTime.now();
        usage.setUpdatedAt(timeStamp);

        return usageMapper.modify(usage);
    }

    public List<UsageVO> getAll(String centerId, String fromDate, String toDate, String keyword, String activityId) {
        List<UsageVO> ret = new ArrayList<>();

        List<Usage> usageList = usageMapper.selectAll(centerId, fromDate, toDate, keyword, activityId);
        for (Usage usage : usageList) {
            UsageVO vo = new UsageVO();
            vo.setUsage(usage);
            vo.setCoinLegerList(coinLegerMapper.selectByUsageId(usage.getId(), fromDate, toDate, keyword));
            vo.setNumOfMembers(null != vo.getCoinLegerList() ? vo.getCoinLegerList().size() : 0);

            ret.add(vo);
        }
        return ret;
    }

    public List<Usage> getAllByCenterId(String centerId) {
        return usageMapper.selectAll(centerId, null, null, null, null);
    }
    public List<CoinLeger> getMemberList(String usageId, String fromDate, String toDate, String keyword) {
        return coinLegerMapper.selectByUsageId(usageId, fromDate, toDate, keyword);
    }

    public ByteArrayInputStream generateUsageListFile(List<Usage> usageList) throws IOException {
        logger.info("generate Excel file.");

        return ExcelPOIUtil.usagesToExcel(usageList);
    }

    public ByteArrayInputStream genMemberListFile(List<CoinLeger> list) throws IOException {
        logger.info("generate Excel file.");

        return ExcelPOIUtil.usageMemberToExcel(list);
    }

    @Transactional
    public int uploadUsageListFile(Center cent, MultipartFile multipartFile) throws Exception {
        logger.info("load file data to db.");

        final String fileType = "usageList";

        StringBuffer filePath = new StringBuffer("/uploadfiles").append(File.separator).append(fileType);

        // file upload to server
        String fileName = getFileName(multipartFile.getOriginalFilename());
        File uploadDir = mkdirs(filePath);
        File uploadFile = new File(uploadDir.getPath() + "/" +  fileName);

        try (BufferedOutputStream uploadFileStream
                     = new BufferedOutputStream(new FileOutputStream(uploadFile))) {

            byte[] bytes = multipartFile.getBytes();
            uploadFileStream.write(bytes);

        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }

        // upload to data base
        if (!uploadFile.exists()) {
            throw new Exception("일괄등록 대상 파일을 읽을 수 없습니다.");
        }

        int uploadCnt;
        Map<Integer, List<CellVO>> map = ExcelPOIUtil.readExcel(uploadFile.getAbsolutePath());
        AtomicInteger cnt = new AtomicInteger(2);
        int sum = 0;
        for (Map.Entry<Integer, List<CellVO>> e : map.entrySet()) {
            if (e.getKey() != 0) {
                int i = insertIfNotExist(cnt.getAndIncrement(), cent, e.getValue());
                sum += i;
            }
        }
        uploadCnt = sum;

        return uploadCnt;
    }

    private int insertIfNotExist(int rowNum, Center cent, List<CellVO> cellVal) throws Exception {
        // TODO more handling
        if (StringUtils.isEmpty(cellVal.get(0).getContent())
                || StringUtils.isEmpty(cellVal.get(1).getContent())) {
            // 등록활동ID
            // 사용처명
            throw new Exception(rowNum + "행의 1, 2열은 필수 항목입니다.");
        }

        // 컬럼갯수 포지션은 다운로드와 동일
        int pos = 0;
        Usage data = new Usage();
        try {
            // usageId
            // get id
            Usage entity = usageMapper.getMaxId();
            String newId = getNewId(Constants.PREFIX_USAGE_ID, null == entity ? "" : entity.getId());
            data.setId(newId);
            // centerId
            data.setCenterId(cent.getId());

            // 활동ID
            data.setActivityId(cellVal.get(pos++).getContent());
            // 사용처명
            data.setName(cellVal.get(pos++).getContent());
            // 담당자
            data.setInCharge(cellVal.get(pos++).getContent());
            // 문의
            data.setContact(cellVal.get(pos++).getContent());
            // 구분
            data.setType(cellVal.get(pos++).getContent());
            // 사용처내용
            data.setDetail(cellVal.get(pos++).getContent());
            // 지역
            data.setArea(cellVal.get(pos++).getContent());
            // 가격
            data.setPrice(new BigDecimal(cellVal.get(pos++).getContent()).intValue());
            // 사용코인
            data.setUseCoin(new BigDecimal(cellVal.get(pos++).getContent()).intValue());
            // 게시여부
            data.setOpenActivity(cellVal.get(pos++).getContent());
        } catch (Exception e) {
            throw new Exception(rowNum + "행, " + pos + "열의 형식이 잘못 입력되어 업로드가 실패했습니다.");
        }

        //
        data.setCreatedBy("uploader");
        data.setUpdatedBy("uploader");
        data.setDelFlg(Constants.NO);

        int ret = 0;
        try {
            ret = usageMapper.register(data);
        } catch (Exception e) {
            throw new Exception(rowNum + "행의 형식이 잘못 입력되어 업로드가 실패했습니다.\n 상세내용: " + e.getMessage());
        }

        return ret;
    }

    public int remove(List<String> ids, String updatedBy) {
        // delete member
        int res = usageMapper.delete(ids, updatedBy);

        return res;
    }

    public List<UsageVO> getAll4Stat(String centerId, String fromDate, String toDate, String keyword) {
        List<Usage> usages = usageMapper.selectAll4Stat(centerId, fromDate, toDate, keyword);

        List<UsageVO> lst = new ArrayList<>();
        long count = countAll4Stat(centerId, fromDate, toDate, keyword);
        if (null != usages && usages.size() > 0) {
            Map<String, String> map = new HashMap<>();
            for (Usage usage : usages) {
                map.clear();
                map.put("activityId", usage.getId());
                UsageVO vo = new UsageVO();
                vo.setUsage(usage);
                List<CoinLeger> coinLegersList = coinLegerMapper.selectCoinSumByType(map);
                if (coinLegersList != null && !coinLegersList.isEmpty()) {
                    coinLegersList.forEach(coin -> {
                        if (Constants.COIN_PLUS.equals(coin.getType())) {
                            vo.setCoinPlusSum(vo.getCoinPlusSum() + coin.getCoinSum());
                        } else if (Constants.COIN_MINUS.equals(coin.getType())) {
                            vo.setCoinPlusSum(vo.getCoinPlusSum() + coin.getCoinSum());
                        } else if (Constants.COIN_USED.equals(coin.getType())) {
                            vo.setCoinMinusSum(coin.getCoinSum());
                        }

                        vo.setUpdatedAt(coin.getUpdatedAt());
                    });
                }

                if (vo.getCoinPlusSum() == 0 && vo.getCoinMinusSum() == 0) {
                    count--;
                    continue;
                }

                vo.setNo(count--);
                lst.add(vo);
            }
        }

        return lst;
    }

    public long countAll4Stat(String centerId, String fromDate, String toDate, String keyword) {
        return usageMapper.countAll4Stat(centerId, fromDate, toDate, keyword);
    }
}