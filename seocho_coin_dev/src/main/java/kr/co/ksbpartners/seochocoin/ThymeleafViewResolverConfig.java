package kr.co.ksbpartners.seochocoin;

import kr.co.ksbpartners.seochocoin.util.thymeleaf.CustUtil;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ThymeleafViewResolverConfig {
    @Bean
    public CustUtil fooDialect() {
        return new CustUtil();
    }
}
