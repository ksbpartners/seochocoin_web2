package kr.co.ksbpartners.seochocoin.entity;

import lombok.Data;
import org.apache.ibatis.type.Alias;

import java.io.Serializable;

@Data
@Alias("market")
public class Usage extends EntityBase implements Serializable {
    private static final long serialVersionUID = 1L;

    private String id;
    private String centerId;
    private String centerNm;
    private String activityId;
    private String activityNm;

    private String name;
    private String inCharge;
    private String contact;
    private String type;
    private String detail;

    private String area;
    private String addr;

    private long price;
    private long useCoin;

    private String openActivity;


    public Usage() {
    }

    public Usage(String id, String centerId, String activityId) {
        this.id = id;
        this.centerId = centerId;
        this.activityId = activityId;
    }

}