package kr.co.ksbpartners.seochocoin.util.excel;

import kr.co.ksbpartners.seochocoin.entity.*;
import kr.co.ksbpartners.seochocoin.vo.*;
import kr.co.ksbpartners.seochocoin.vo.CellVO;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.springframework.util.StringUtils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.IntStream;


import java.io.ByteArrayOutputStream;



public class ExcelPOIUtil {


    public static ByteArrayInputStream activityToExcel(List<Activity> list) throws IOException {
        String[] COLUMNs = {
                "activityId",
                "centerId",
                "활동명",
                "활동유형",
                "카테고리",
                "담당자",
                "상태",
                "지역",
                "활동내용",
                "시작일",
                "종료일",

                "활동횟수(일회성, Y/N)",
                "월(Y/N)", "시작", "종료",
                "화(Y/N)", "시작", "종료",
                "수(Y/N)", "시작", "종료",
                "목(Y/N)", "시작", "종료",
                "금(Y/N)", "시작", "종료",

                "정원(명)",
                "코인 사용(Y/N)",
                "코인 사용",
                "코인 적립(Y/N)",
                "코인 적립",
                "활동환료후 승인(Y/N)",
                "출결확인 필요(Y/N)",
                "출석률",
                "가격정보 사용(Y/N)",
                "가격",
                "사용처 자동등록(Y/N)",
                "게시(Y/N)"
        };
        try(
                Workbook workbook = new XSSFWorkbook();
                ByteArrayOutputStream out = new ByteArrayOutputStream();
        ){
            CreationHelper createHelper = workbook.getCreationHelper();

            Sheet sheet = workbook.createSheet("activities");

            Font headerFont = workbook.createFont();
            headerFont.setBold(true);
            headerFont.setColor(IndexedColors.DARK_BLUE.getIndex());

            CellStyle headerCellStyle = workbook.createCellStyle();
            headerCellStyle.setFont(headerFont);

            int rowIdx = 0;
            // Row for Header
            Row headerRow = sheet.createRow(rowIdx++);

            // Header
            for (int col = 0; col < COLUMNs.length; col++) {
                Cell cell = headerRow.createCell(col);
                cell.setCellValue(COLUMNs[col]);
                cell.setCellStyle(headerCellStyle);
            }

            // CellStyle for Age
            CellStyle numCellStyle = workbook.createCellStyle();
            numCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("#,###"));

            for (Activity data : list) {
                Row row = sheet.createRow(rowIdx++);
                int cellIdx = 0;
                //"Id",
                row.createCell(cellIdx++).setCellValue(data.getId());
                //"centerId",
                row.createCell(cellIdx++).setCellValue(data.getCenterId());
                //"활동명",
                row.createCell(cellIdx++).setCellValue(data.getName());
                //"활동유형",
                row.createCell(cellIdx++).setCellValue(data.getType());
                //"카테고리",
                row.createCell(cellIdx++).setCellValue(data.getCategory());
                //"담당자",
                row.createCell(cellIdx++).setCellValue(data.getInCharge());
                //"상태",
                row.createCell(cellIdx++).setCellValue(data.getStatus());
                //"지역",
                row.createCell(cellIdx++).setCellValue(data.getArea());
                //"활동내용",
                row.createCell(cellIdx++).setCellValue(data.getDetail());
                //"시작일",
                row.createCell(cellIdx++).setCellValue(data.getStartDate());
                //"종료일",
                row.createCell(cellIdx++).setCellValue(data.getEndDate());

                //"활동횟수(일회성)",
                row.createCell(cellIdx++).setCellValue(data.getHasPeriod());
                // MON - FRI
                row.createCell(cellIdx++).setCellValue(data.getMon());
                row.createCell(cellIdx++).setCellValue(data.getMonFrom());
                row.createCell(cellIdx++).setCellValue(data.getMonTo());
                row.createCell(cellIdx++).setCellValue(data.getTue());
                row.createCell(cellIdx++).setCellValue(data.getTueFrom());
                row.createCell(cellIdx++).setCellValue(data.getTueTo());
                row.createCell(cellIdx++).setCellValue(data.getWed());
                row.createCell(cellIdx++).setCellValue(data.getWedFrom());
                row.createCell(cellIdx++).setCellValue(data.getWedTo());
                row.createCell(cellIdx++).setCellValue(data.getThu());
                row.createCell(cellIdx++).setCellValue(data.getThuFrom());
                row.createCell(cellIdx++).setCellValue(data.getThuTo());
                row.createCell(cellIdx++).setCellValue(data.getFri());
                row.createCell(cellIdx++).setCellValue(data.getFriFrom());
                row.createCell(cellIdx++).setCellValue(data.getFriTo());

                //"정원(명)",
                row.createCell(cellIdx++).setCellValue(data.getLimt());
                row.getCell(cellIdx-1).setCellStyle(numCellStyle);
                //"코인 사용(Y/N)",
                row.createCell(cellIdx++).setCellValue(data.getUseCoin());
                //"2000",
                row.createCell(cellIdx++).setCellValue(data.getUseCoinVal());
                row.getCell(cellIdx-1).setCellStyle(numCellStyle);
                //"코인 적립(Y/N)",
                row.createCell(cellIdx++).setCellValue(data.getAddCoin());
                //"2000",
                row.createCell(cellIdx++).setCellValue(data.getAddCoinVal());
                row.getCell(cellIdx-1).setCellStyle(numCellStyle);
                //"활동환료후 승인(Y/N)",
                row.createCell(cellIdx++).setCellValue(data.getAddCoinAfter());
                //"출결확인 필요(Y/N)",
                row.createCell(cellIdx++).setCellValue(data.getAttendCheck());
                //"80",
                row.createCell(cellIdx++).setCellValue(data.getAttendRate());
                row.getCell(cellIdx-1).setCellStyle(numCellStyle);
                //"가격정보 사용(Y/N)",
                row.createCell(cellIdx++).setCellValue(data.getUsePriceInfo());
                //"2500",
                row.createCell(cellIdx++).setCellValue(data.getPrice());
                row.getCell(cellIdx-1).setCellStyle(numCellStyle);
                //"사용처 자동등록(Y/N)",
                row.createCell(cellIdx++).setCellValue(data.getAutoAddMarket());
                //"게시(Y/N)"
                row.createCell(cellIdx++).setCellValue(data.getOpenActivity());
            }

            workbook.write(out);
            return new ByteArrayInputStream(out.toByteArray());
        }
    }




    public static ByteArrayInputStream centersToExcel(List<Center> centers, Map<String, Admin> adminMap) throws IOException {
        String[] COLUMNs = {"센터ID", "담당자ID", "담당자", "센터명", "지역", "구분", "우편번호", "주소", "비고", "등록일"};
        try(
                Workbook workbook = new XSSFWorkbook();
                ByteArrayOutputStream out = new ByteArrayOutputStream();
        ){
            CreationHelper createHelper = workbook.getCreationHelper();

            Sheet sheet = workbook.createSheet("members");

            Font headerFont = workbook.createFont();
            headerFont.setBold(true);
            headerFont.setColor(IndexedColors.DARK_BLUE.getIndex());

            CellStyle headerCellStyle = workbook.createCellStyle();
            headerCellStyle.setFont(headerFont);

            // Row for Header
            Row headerRow = sheet.createRow(0);

            // Header
            for (int col = 0; col < COLUMNs.length; col++) {
                Cell cell = headerRow.createCell(col);
                cell.setCellValue(COLUMNs[col]);
                cell.setCellStyle(headerCellStyle);
            }

            int rowIdx = 1;
            for (Center data : centers) {
                Row row = sheet.createRow(rowIdx++);

                // Id
                row.createCell(0).setCellValue(data.getId());
                // 담당자 ID
                row.createCell(1).setCellValue(data.getAdminId());
                // 담당자
                Admin admin = null != adminMap ? adminMap.get(data.getAdminId()) : null;
                row.createCell(2).setCellValue(null != admin ? admin.getName() : "");
                // 센터명
                row.createCell(3).setCellValue(data.getName());
                // 지역
                row.createCell(4).setCellValue(data.getArea());
                // 구분
                row.createCell(5).setCellValue(data.getType());
                // 우편번호
                row.createCell(6).setCellValue(data.getPostalCode());
                // 주소
                row.createCell(7).setCellValue(data.getAddr());
                // 비고
                row.createCell(8).setCellValue(data.getDescription());
                // 등록일
                row.createCell(9).setCellValue(data.getCreatedAt().toString());

            }

            workbook.write(out);
            return new ByteArrayInputStream(out.toByteArray());
        }
    }


    public static ByteArrayInputStream membersToExcel(List<Member> memberList) throws IOException {
        String[] COLUMNs = {"Id", "회원명", "나이", "성별", "연락처", "등록센터1", "등록센터2", "등록센터3", "등록일", "최근 접속일", "활동 등록수", "보유코인"};
        try(
                Workbook workbook = new XSSFWorkbook();
                ByteArrayOutputStream out = new ByteArrayOutputStream();
        ){
            CreationHelper createHelper = workbook.getCreationHelper();

            Sheet sheet = workbook.createSheet("members");

            Font headerFont = workbook.createFont();
            headerFont.setBold(true);
            headerFont.setColor(IndexedColors.DARK_BLUE.getIndex());

            CellStyle headerCellStyle = workbook.createCellStyle();
            headerCellStyle.setFont(headerFont);

            // Row for Header
            Row headerRow = sheet.createRow(0);

            // Header
            for (int col = 0; col < COLUMNs.length; col++) {
                Cell cell = headerRow.createCell(col);
                cell.setCellValue(COLUMNs[col]);
                cell.setCellStyle(headerCellStyle);
            }

            // CellStyle for Age
            CellStyle numCellStyle = workbook.createCellStyle();
            numCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("#,###"));

            int rowIdx = 1;
            for (Member member : memberList) {
                Row row = sheet.createRow(rowIdx++);

                row.createCell(0).setCellValue(member.getId());
                row.createCell(1).setCellValue(member.getName());

                Cell ageCell = row.createCell(2);
                ageCell.setCellValue(member.getAge());
                ageCell.setCellStyle(numCellStyle);

                row.createCell(3).setCellValue(StringUtils.isEmpty(member.getSex()) ? "" : member.getSex().toString());
                row.createCell(4).setCellValue(member.getMobile());
                row.createCell(5).setCellValue(member.getCenter1Nm());
                row.createCell(6).setCellValue(member.getCenter2Nm());
                row.createCell(7).setCellValue(member.getCenter3Nm());
                row.createCell(8).setCellValue(member.getCreatedAt().toString());
                row.createCell(9).setCellValue(member.getUpdatedAt().toString());

                Cell cntCell = row.createCell(10);
                cntCell.setCellValue(member.getNumOfActivity());
                cntCell.setCellStyle(numCellStyle);

                Cell coinCell = row.createCell(11);
                coinCell.setCellValue(member.getCoin());
                coinCell.setCellStyle(numCellStyle);
            }

            workbook.write(out);
            return new ByteArrayInputStream(out.toByteArray());
        }
    }

    public static ByteArrayInputStream statMembersToExcel(List<MemberVO> memberList, String[] COLUMNs) throws IOException {
        try(
                Workbook workbook = new XSSFWorkbook();
                ByteArrayOutputStream out = new ByteArrayOutputStream();
        ){
            CreationHelper createHelper = workbook.getCreationHelper();

            Sheet sheet = workbook.createSheet("stat_members");

            Font headerFont = workbook.createFont();
            headerFont.setBold(true);
            headerFont.setColor(IndexedColors.DARK_BLUE.getIndex());

            CellStyle headerCellStyle = workbook.createCellStyle();
            headerCellStyle.setFont(headerFont);

            // Row for Header
            Row headerRow = sheet.createRow(0);

            // Header
            for (int col = 0; col < COLUMNs.length; col++) {
                Cell cell = headerRow.createCell(col);
                cell.setCellValue(COLUMNs[col]);
                cell.setCellStyle(headerCellStyle);
            }

            // CellStyle for Age
            CellStyle numCellStyle = workbook.createCellStyle();
            numCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("#,###"));

            int rowIdx = 1;
            for (MemberVO member : memberList) {
                int colIdx = 0;
                Row row = sheet.createRow(rowIdx++);

                row.createCell(colIdx++).setCellValue(member.getNo());
                if (COLUMNs.length == 6) {
                    row.createCell(colIdx++).setCellValue(member.getMember().getCenter1Nm());
                }
                row.createCell(colIdx++).setCellValue(member.getMember().getName());

                Cell coinPlusCell = row.createCell(colIdx++);
                coinPlusCell.setCellValue(member.getCoinPlusSum());
                coinPlusCell.setCellStyle(numCellStyle);

                Cell coinMinusCell = row.createCell(colIdx++);
                coinMinusCell.setCellValue(member.getCoinPlusSum());
                coinMinusCell.setCellStyle(numCellStyle);

                row.createCell(colIdx++).setCellValue(member.getUpdatedAt().toString());
            }

            workbook.write(out);
            return new ByteArrayInputStream(out.toByteArray());
        }
    }

    public static ByteArrayInputStream statActivitiesToExcel(List<ActivityVO> activityList, String[] COLUMNs) throws IOException {
        try(
                Workbook workbook = new XSSFWorkbook();
                ByteArrayOutputStream out = new ByteArrayOutputStream();
        ){
            CreationHelper createHelper = workbook.getCreationHelper();

            Sheet sheet = workbook.createSheet("stat_activities");

            Font headerFont = workbook.createFont();
            headerFont.setBold(true);
            headerFont.setColor(IndexedColors.DARK_BLUE.getIndex());

            CellStyle headerCellStyle = workbook.createCellStyle();
            headerCellStyle.setFont(headerFont);

            // Row for Header
            Row headerRow = sheet.createRow(0);

            // Header
            for (int col = 0; col < COLUMNs.length; col++) {
                Cell cell = headerRow.createCell(col);
                cell.setCellValue(COLUMNs[col]);
                cell.setCellStyle(headerCellStyle);
            }

            // CellStyle for Age
            CellStyle numCellStyle = workbook.createCellStyle();
            numCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("#,###"));

            int rowIdx = 1;
            for (ActivityVO activity : activityList) {
                int colIdx = 0;
                Row row = sheet.createRow(rowIdx++);

                row.createCell(colIdx++).setCellValue(activity.getNo());
                if (COLUMNs.length == 6) {
                    row.createCell(colIdx++).setCellValue(activity.getActivity().getCenterNm());
                }
                row.createCell(colIdx++).setCellValue(activity.getActivity().getName());

                Cell coinPlusCell = row.createCell(colIdx++);
                coinPlusCell.setCellValue(activity.getCoinPlusSum());
                coinPlusCell.setCellStyle(numCellStyle);

                Cell coinMinusCell = row.createCell(colIdx++);
                coinMinusCell.setCellValue(activity.getCoinPlusSum());
                coinMinusCell.setCellStyle(numCellStyle);

                row.createCell(colIdx++).setCellValue(activity.getUpdatedAt().toString());
            }

            workbook.write(out);
            return new ByteArrayInputStream(out.toByteArray());
        }
    }

    public static ByteArrayInputStream statUsagesToExcel(List<UsageVO> usageList, String[] COLUMNs) throws IOException {
        try(
                Workbook workbook = new XSSFWorkbook();
                ByteArrayOutputStream out = new ByteArrayOutputStream();
        ){
            CreationHelper createHelper = workbook.getCreationHelper();

            Sheet sheet = workbook.createSheet("stat_activities");

            Font headerFont = workbook.createFont();
            headerFont.setBold(true);
            headerFont.setColor(IndexedColors.DARK_BLUE.getIndex());

            CellStyle headerCellStyle = workbook.createCellStyle();
            headerCellStyle.setFont(headerFont);

            // Row for Header
            Row headerRow = sheet.createRow(0);

            // Header
            for (int col = 0; col < COLUMNs.length; col++) {
                Cell cell = headerRow.createCell(col);
                cell.setCellValue(COLUMNs[col]);
                cell.setCellStyle(headerCellStyle);
            }

            // CellStyle for Age
            CellStyle numCellStyle = workbook.createCellStyle();
            numCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("#,###"));

            int rowIdx = 1;
            for (UsageVO usage : usageList) {
                int colIdx = 0;
                Row row = sheet.createRow(rowIdx++);

                row.createCell(colIdx++).setCellValue(usage.getNo());
                if (COLUMNs.length == 6) {
                    row.createCell(colIdx++).setCellValue(usage.getUsage().getCenterNm());
                }
                row.createCell(colIdx++).setCellValue(usage.getUsage().getName());

                Cell coinPlusCell = row.createCell(colIdx++);
                coinPlusCell.setCellValue(usage.getCoinPlusSum());
                coinPlusCell.setCellStyle(numCellStyle);

                Cell coinMinusCell = row.createCell(colIdx++);
                coinMinusCell.setCellValue(usage.getCoinPlusSum());
                coinMinusCell.setCellStyle(numCellStyle);

                row.createCell(colIdx++).setCellValue(usage.getUpdatedAt().toString());
            }

            workbook.write(out);
            return new ByteArrayInputStream(out.toByteArray());
        }
    }

    public static ByteArrayInputStream memberActivityToExcel(List<MemberActivityVO> list) throws IOException {
        String[] COLUMNs = {"활동처명", "사용자명(ID)", "연락처", "결제상태", "상태", "출석률", "등록일", "적립코인"};
        try(
                Workbook workbook = new XSSFWorkbook();
                ByteArrayOutputStream out = new ByteArrayOutputStream();
        ){
            CreationHelper createHelper = workbook.getCreationHelper();

            Sheet sheet = workbook.createSheet("memberActivities");

            Font headerFont = workbook.createFont();
            headerFont.setBold(true);
            headerFont.setColor(IndexedColors.DARK_BLUE.getIndex());

            CellStyle headerCellStyle = workbook.createCellStyle();
            headerCellStyle.setFont(headerFont);

            // Row for Header
            Row headerRow = sheet.createRow(0);

            // Header
            for (int col = 0; col < COLUMNs.length; col++) {
                Cell cell = headerRow.createCell(col);
                cell.setCellValue(COLUMNs[col]);
                cell.setCellStyle(headerCellStyle);
            }

            // CellStyle for Age
            CellStyle numCellStyle = workbook.createCellStyle();
            numCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("#,###.##"));

            int rowIdx = 1;
            for (MemberActivityVO mavo : list) {
                Row row = sheet.createRow(rowIdx++);

                row.createCell(0).setCellValue(mavo.getActivityName());
                row.createCell(1).setCellValue(mavo.getMemberName()+"("+mavo.getMemberId()+")");
                row.createCell(2).setCellValue(mavo.getMobile());
                row.createCell(3).setCellValue(mavo.getPaymentStatus());
                row.createCell(4).setCellValue(mavo.getActivityStatus());

                BigDecimal accumRate = new BigDecimal(mavo.getAttendedActivity()).multiply(new BigDecimal(100))
                        .divide(new BigDecimal(mavo.getTotActivity()), 0, BigDecimal.ROUND_UP);
                row.createCell(5).setCellValue(accumRate + " %");
                row.getCell(5).setCellStyle(numCellStyle);

                row.createCell(6).setCellValue(mavo.getCreatedAt().toString());

                // TODO: 적립상태체크
                Cell coinCell = row.createCell(7);
                coinCell.setCellValue(mavo.getAccumCoin());
                coinCell.setCellStyle(numCellStyle);
            }

            workbook.write(out);
            return new ByteArrayInputStream(out.toByteArray());
        }
    }

    public static ByteArrayInputStream memberActivityScheduleToExcel(List<ActivitySchedule> list) throws IOException {
        String[] COLUMNs = {"사용자명(ID)", "활동처명", "활동날짜", "요일", "시작", "종료", "출석", "등록일"};
        try(
                Workbook workbook = new XSSFWorkbook();
                ByteArrayOutputStream out = new ByteArrayOutputStream();
        ){
            CreationHelper createHelper = workbook.getCreationHelper();

            Sheet sheet = workbook.createSheet("memberActivitySchedules");

            Font headerFont = workbook.createFont();
            headerFont.setBold(true);
            headerFont.setColor(IndexedColors.DARK_BLUE.getIndex());

            CellStyle headerCellStyle = workbook.createCellStyle();
            headerCellStyle.setFont(headerFont);

            // Row for Header
            Row headerRow = sheet.createRow(0);

            // Header
            for (int col = 0; col < COLUMNs.length; col++) {
                Cell cell = headerRow.createCell(col);
                cell.setCellValue(COLUMNs[col]);
                cell.setCellStyle(headerCellStyle);
            }

            int rowIdx = 1;
            for (ActivitySchedule asvo : list) {
                Row row = sheet.createRow(rowIdx++);

                row.createCell(0).setCellValue(asvo.getMemberName()+"("+asvo.getMemberId()+")");
                row.createCell(1).setCellValue(asvo.getActivityName());
                row.createCell(2).setCellValue(asvo.getActivityDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
                row.createCell(3).setCellValue(asvo.getDay());
                row.createCell(4).setCellValue(asvo.getStart());
                row.createCell(5).setCellValue(asvo.getEnd());
                row.createCell(6).setCellValue(asvo.getAttendedAt() != null ? "출석" : "");

                row.createCell(7).setCellValue(asvo.getCreatedAt().toString());
            }

            workbook.write(out);
            return new ByteArrayInputStream(out.toByteArray());
        }
    }

    public static ByteArrayInputStream usagesToExcel(List<Usage> usageList) throws IOException {
        String[] COLUMNs = {"Id", "등록센터", "등록활동", "사용처명", "담당자", "문의", "구분", "사용처내용", "지역", "가격", "사용코인", "게시 여부", "등록일시"};
        try(
                Workbook workbook = new XSSFWorkbook();
                ByteArrayOutputStream out = new ByteArrayOutputStream();
        ){
            CreationHelper createHelper = workbook.getCreationHelper();

            Sheet sheet = workbook.createSheet("usages");

            Font headerFont = workbook.createFont();
            headerFont.setBold(true);
            headerFont.setColor(IndexedColors.DARK_BLUE.getIndex());

            CellStyle headerCellStyle = workbook.createCellStyle();
            headerCellStyle.setFont(headerFont);

            // Row for Header
            Row headerRow = sheet.createRow(0);

            // Header
            for (int col = 0; col < COLUMNs.length; col++) {
                Cell cell = headerRow.createCell(col);
                cell.setCellValue(COLUMNs[col]);
                cell.setCellStyle(headerCellStyle);
            }

            // CellStyle for Age
            CellStyle numCellStyle = workbook.createCellStyle();
            numCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("#,###"));

            int rowIdx = 1;
            for (Usage usage : usageList) {
                Row row = sheet.createRow(rowIdx++);

                row.createCell(0).setCellValue(usage.getId());
                row.createCell(1).setCellValue(usage.getCenterId());
                row.createCell(2).setCellValue(usage.getActivityId());

                row.createCell(3).setCellValue(usage.getName());
                row.createCell(4).setCellValue(usage.getInCharge());
                row.createCell(5).setCellValue(usage.getContact());
                row.createCell(6).setCellValue(usage.getType());
                row.createCell(7).setCellValue(usage.getDetail());
                row.createCell(8).setCellValue(usage.getArea());

                Cell priceCell = row.createCell(9);
                priceCell.setCellValue(usage.getPrice());
                priceCell.setCellStyle(numCellStyle);

                Cell coinCell = row.createCell(10);
                coinCell.setCellValue(usage.getUseCoin());
                coinCell.setCellStyle(numCellStyle);

                row.createCell(11).setCellValue(usage.getOpenActivity());
                row.createCell(12).setCellValue(usage.getCreatedAt().toString());

            }

            workbook.write(out);
            return new ByteArrayInputStream(out.toByteArray());
        }
    }

    public static ByteArrayInputStream usageMemberToExcel(List<CoinLeger> list) throws IOException {
        String[] COLUMNs = {"사용처명", "사용자명(ID)", "연락처", "코인사용", "등록일"};
        try(
                Workbook workbook = new XSSFWorkbook();
                ByteArrayOutputStream out = new ByteArrayOutputStream();
        ){
            CreationHelper createHelper = workbook.getCreationHelper();

            Sheet sheet = workbook.createSheet("usageMembers");

            Font headerFont = workbook.createFont();
            headerFont.setBold(true);
            headerFont.setColor(IndexedColors.DARK_BLUE.getIndex());

            CellStyle headerCellStyle = workbook.createCellStyle();
            headerCellStyle.setFont(headerFont);

            // Row for Header
            Row headerRow = sheet.createRow(0);

            // Header
            for (int col = 0; col < COLUMNs.length; col++) {
                Cell cell = headerRow.createCell(col);
                cell.setCellValue(COLUMNs[col]);
                cell.setCellStyle(headerCellStyle);
            }

            // CellStyle for Age
            CellStyle numCellStyle = workbook.createCellStyle();
            numCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("#,###.##"));

            int rowIdx = 1;
            for (CoinLeger mavo : list) {
                Row row = sheet.createRow(rowIdx++);

                row.createCell(0).setCellValue(mavo.getActivityName());
                row.createCell(1).setCellValue(mavo.getMemberName()+"("+mavo.getMemberId()+")");
                row.createCell(2).setCellValue(mavo.getMemberMobile());

                row.createCell(3).setCellValue(mavo.getPaid());
                row.getCell(3).setCellStyle(numCellStyle);

                row.createCell(4).setCellValue(mavo.getCreatedAt().toString());
            }

            workbook.write(out);
            return new ByteArrayInputStream(out.toByteArray());
        }
    }

    static public Map<Integer, List<CellVO>> readExcel(String fileLocation) throws IOException {

        Map<Integer, List<CellVO>> data = new HashMap<>();
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(new File(fileLocation));

            if (fileLocation.endsWith(".xls")) {
                data = readHSSFWorkbook(fis);
            } else if (fileLocation.endsWith(".xlsx")) {
                data = readXSSFWorkbook(fis);
            }
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                }
            } catch (IOException e) {
                // Ignore ...
            }
        }

        int maxNrCols = data.values().stream()
                .mapToInt(List::size)
                .max()
                .orElse(0);

        data.values().stream()
                .filter(ls -> ls.size() < maxNrCols)
                .forEach(ls -> {
                    IntStream.range(ls.size(), maxNrCols)
                            .forEach(i -> ls.add(new CellVO("")));
                });

        return data;
    }

    private static String readCellContent(Cell cell) {
        String content;
        switch (cell.getCellTypeEnum()) {
            case STRING:
                content = cell.getStringCellValue();
                break;
            case NUMERIC:
                if (DateUtil.isCellDateFormatted(cell)) {
                    content = cell.getDateCellValue() + "";
                } else {
                    content = (long) cell.getNumericCellValue() + "";
                }
                break;
            case BOOLEAN:
                content = cell.getBooleanCellValue() + "";
                break;
            case FORMULA:
                content = cell.getCellFormula() + "";
                break;
            default:
                content = "";
        }
        return content;
    }

    private static Map<Integer, List<CellVO>> readHSSFWorkbook(FileInputStream fis) throws IOException {
        Map<Integer, List<CellVO>> data = new HashMap<>();
        HSSFWorkbook workbook = null;
        try {
            workbook = new HSSFWorkbook(fis);

            HSSFSheet sheet = workbook.getSheetAt(0);
            for (int i = sheet.getFirstRowNum(); i <= sheet.getLastRowNum(); i++) {
                HSSFRow row = sheet.getRow(i);
                data.put(i, new ArrayList<>());
                if (row != null) {
                    for (int j = 0; j < row.getLastCellNum(); j++) {
                        HSSFCell cell = row.getCell(j);
                        if (cell != null) {
                            HSSFCellStyle cellStyle = cell.getCellStyle();

                            CellVO cellVO = new CellVO();

//                            HSSFColor bgColor = cellStyle.getFillForegroundColorColor();
//                            if (bgColor != null) {
//                                short[] rgbColor = bgColor.getTriplet();
//                                cellVO.setBgColor("rgb(" + rgbColor[0] + "," + rgbColor[1] + "," + rgbColor[2] + ")");
//                            }
//                            HSSFFont font = cell.getCellStyle()
//                                    .getFont(workbook);
//                            cellVO.setTextSize(font.getFontHeightInPoints() + "");
//                            if (font.getBold()) {
//                                cellVO.setTextWeight("bold");
//                            }
//                            HSSFColor textColor = font.getHSSFColor(workbook);
//                            if (textColor != null) {
//                                short[] rgbColor = textColor.getTriplet();
//                                cellVO.setTextColor("rgb(" + rgbColor[0] + "," + rgbColor[1] + "," + rgbColor[2] + ")");
//                            }

                            if (j == 3) {
                                String mobile = readCellContent(cell);
                                if (mobile.indexOf("-") != -1) {
                                    StringTokenizer st = new StringTokenizer(mobile, "-");
                                    StringBuilder sb = new StringBuilder();
                                    while (st.hasMoreTokens()) {
                                        sb.append(st.nextToken());
                                    }

                                    cellVO.setContent(sb.toString());
                                } else {
                                    cellVO.setContent(mobile);
                                }
                            } else {
                                cellVO.setContent(readCellContent(cell));
                            }

                            data.get(i).add(cellVO);
                        } else {
                            data.get(i).add(new CellVO(""));
                        }
                    }
                }
            }
        } finally {
            if (workbook != null) {
                workbook.close();
            }
        }
        return data;
    }

    private static Map<Integer, List<CellVO>> readXSSFWorkbook(FileInputStream fis) throws IOException {
        XSSFWorkbook workbook = null;
        Map<Integer, List<CellVO>> data = new HashMap<>();
        try {

            workbook = new XSSFWorkbook(fis);
            XSSFSheet sheet = workbook.getSheetAt(0);

            for (int i = sheet.getFirstRowNum(); i <= sheet.getLastRowNum(); i++) {
                XSSFRow row = sheet.getRow(i);
                data.put(i, new ArrayList<>());
                if (row != null) {
                    for (int j = 0; j < row.getLastCellNum(); j++) {
                        XSSFCell cell = row.getCell(j);
                        if (cell != null) {
                            XSSFCellStyle cellStyle = cell.getCellStyle();

                            CellVO cellVO = new CellVO();
//                            XSSFColor bgColor = cellStyle.getFillForegroundColorColor();
//                            if (bgColor != null) {
//                                byte[] rgbColor = bgColor.getRGB();
//                                cellVO.setBgColor("rgb(" + (rgbColor[0] < 0 ? (rgbColor[0] + 0xff) : rgbColor[0]) + "," + (rgbColor[1] < 0 ? (rgbColor[1] + 0xff) : rgbColor[1]) + "," + (rgbColor[2] < 0 ? (rgbColor[2] + 0xff) : rgbColor[2]) + ")");
//                            }
//                            XSSFFont font = cellStyle.getFont();
//                            cellVO.setTextSize(font.getFontHeightInPoints() + "");
//                            if (font.getBold()) {
//                                cellVO.setTextWeight("bold");
//                            }
//                            XSSFColor textColor = font.getXSSFColor();
//                            if (textColor != null) {
//                                byte[] rgbColor = textColor.getRGB();
//                                cellVO.setTextColor("rgb(" + (rgbColor[0] < 0 ? (rgbColor[0] + 0xff) : rgbColor[0]) + "," + (rgbColor[1] < 0 ? (rgbColor[1] + 0xff) : rgbColor[1]) + "," + (rgbColor[2] < 0 ? (rgbColor[2] + 0xff) : rgbColor[2]) + ")");
//                            }
                            if (j == 3) {
                                String mobile = readCellContent(cell);
                                if (mobile.indexOf("-") != -1) {
                                    StringTokenizer st = new StringTokenizer(mobile, "-");
                                    StringBuilder sb = new StringBuilder();
                                    while (st.hasMoreTokens()) {
                                        sb.append(st.nextToken());
                                    }

                                    cellVO.setContent(sb.toString());
                                } else {
                                    cellVO.setContent(mobile);
                                }
                            } else {
                                cellVO.setContent(readCellContent(cell));
                            }
                            data.get(i).add(cellVO);
                        } else {
                            data.get(i).add(new CellVO(""));
                        }
                    }
                }
            }
        } finally {
            if (workbook != null) {
                workbook.close();
            }
        }
        return data;
    }

}