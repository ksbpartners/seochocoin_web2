package kr.co.ksbpartners.seochocoin.vo;

import kr.co.ksbpartners.seochocoin.entity.EntityBase;
import lombok.Data;

import java.io.Serializable;

@Data
public class MemberActivityVO extends EntityBase implements Serializable {

    private String memberId;
    private String memberName;
    private String mobile;
    private String centerId;
    private String activityId;
    private String activityName;
    private String activityStatus;
    private String paymentStatus;
    private String accumStatus;
    private long accumCoin;
    private long totActivity;
    private long attendedActivity;

    public MemberActivityVO() { }

}
