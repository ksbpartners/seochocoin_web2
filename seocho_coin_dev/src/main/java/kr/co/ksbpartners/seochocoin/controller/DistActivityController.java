package kr.co.ksbpartners.seochocoin.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/dist")
public class DistActivityController {
    @GetMapping("/activity-list")
    public String activityList() {
        return "dist/activity-list";
    }

    @GetMapping("/activity-register")
    public String activityRegister() {
        return "dist/activity-register";
    }

    @GetMapping("/activity-register-add")
    public String activityRegisterAdd() {
        return "dist/activity-register-add";
    }

    @GetMapping("/activity-register-edit")
    public String activityRegisterEdit() {
        return "dist/activity-register-edit";
    }
}
