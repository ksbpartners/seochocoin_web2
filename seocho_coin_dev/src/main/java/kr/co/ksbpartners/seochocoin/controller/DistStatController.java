package kr.co.ksbpartners.seochocoin.controller;

import kr.co.ksbpartners.seochocoin.common.userdetails.CentUser;
import kr.co.ksbpartners.seochocoin.common.userdetails.DistUser;
import kr.co.ksbpartners.seochocoin.entity.Center;
import kr.co.ksbpartners.seochocoin.service.ActivityService;
import kr.co.ksbpartners.seochocoin.service.MemberService;
import kr.co.ksbpartners.seochocoin.service.UsageService;
import kr.co.ksbpartners.seochocoin.util.excel.ExcelPOIUtil;
import kr.co.ksbpartners.seochocoin.vo.ActivityVO;
import kr.co.ksbpartners.seochocoin.vo.MemberVO;
import kr.co.ksbpartners.seochocoin.vo.UsageVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping("/dist")
public class DistStatController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    MemberService memberService;

    @Autowired
    ActivityService activityService;

    @Autowired
    UsageService usageService;

    @GetMapping("/stat-activity")
    public String statActivity(@RequestParam(required = false) String keyword,
                               @RequestParam(required = false) String fromDate,
                               @RequestParam(required = false) String toDate,
                               Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        List<ActivityVO> lst = null;
        DistUser user = (DistUser)auth.getPrincipal();
        if (null != user) {
            lst = activityService.getAll4Stat(null, fromDate, toDate, keyword);
        }

        model.addAttribute("activities", lst);
        model.addAttribute("fromDate", fromDate);
        model.addAttribute("toDate", toDate);
        model.addAttribute("keyword", keyword);

        return "dist/stat-activity";
    }

    @GetMapping("/stat-activity-download")
    public ResponseEntity<InputStreamResource> statActivityDownload(@RequestParam(required = false) String keyword,
                                                                @RequestParam(required = false) String fromDate,
                                                                @RequestParam(required = false) String toDate,
                                                                Model model) throws IOException {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        List<ActivityVO> activityList = null;
        DistUser user = (DistUser)auth.getPrincipal();
        if (null != user) {
            activityList = activityService.getAll4Stat(null, fromDate, toDate, keyword);
        }

        ByteArrayInputStream in = ExcelPOIUtil.statActivitiesToExcel(activityList, new String[]{"No", "센터명", "활동명", "코인적립", "코인사용", "최근 사용일"});

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "attachment; filename=statActivityList.xlsx");

        return ResponseEntity
                .ok()
                .headers(headers)
                .body(new InputStreamResource(in));
    }

    @GetMapping("/stat-usage")
    public String statUsage(@RequestParam(required = false) String keyword,
                            @RequestParam(required = false) String fromDate,
                            @RequestParam(required = false) String toDate,
                            Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        List<UsageVO> lst = null;
        DistUser user = (DistUser)auth.getPrincipal();
        if (null != user) {
            lst = usageService.getAll4Stat(null, fromDate, toDate, keyword);
        }

        model.addAttribute("usages", lst);
        model.addAttribute("fromDate", fromDate);
        model.addAttribute("toDate", toDate);
        model.addAttribute("keyword", keyword);

        return "dist/stat-usage";
    }

    @GetMapping("/stat-usage-download")
    public ResponseEntity<InputStreamResource> statUsageDownload(@RequestParam(required = false) String keyword,
                                                                    @RequestParam(required = false) String fromDate,
                                                                    @RequestParam(required = false) String toDate,
                                                                    Model model) throws IOException {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        List<UsageVO> usageList = null;
        DistUser user = (DistUser)auth.getPrincipal();
        if (null != user) {
            usageList = usageService.getAll4Stat(null, fromDate, toDate, keyword);
        }

        ByteArrayInputStream in = ExcelPOIUtil.statUsagesToExcel(usageList, new String[]{"No", "센터명", "사용처명", "코인적립", "코인사용", "최근 사용일"});

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "attachment; filename=statUsageList.xlsx");

        return ResponseEntity
                .ok()
                .headers(headers)
                .body(new InputStreamResource(in));
    }

    @GetMapping("/stat-user")
    public String statUuser(@RequestParam(required = false) String keyword,
                            @RequestParam(required = false) String fromDate,
                            @RequestParam(required = false) String toDate,
                            Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        List<MemberVO> lst = null;
        DistUser user = (DistUser)auth.getPrincipal();
        if (null != user) {
            lst = memberService.getAll4Stat(null, fromDate, toDate, keyword);
        }

        model.addAttribute("members", lst);
        model.addAttribute("fromDate", fromDate);
        model.addAttribute("toDate", toDate);
        model.addAttribute("keyword", keyword);

        return "dist/stat-user";
    }

    @GetMapping("/stat-user-download")
    public ResponseEntity<InputStreamResource> statUserDownload(@RequestParam(required = false) String keyword,
                                                                @RequestParam(required = false) String fromDate,
                                                                @RequestParam(required = false) String toDate,
                                                                Model model) throws IOException {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        List<MemberVO> memberList = null;
        DistUser user = (DistUser)auth.getPrincipal();
        if (null != user) {
            memberList = memberService.getAll4Stat(null, fromDate, toDate, keyword);
        }

        ByteArrayInputStream in = ExcelPOIUtil.statMembersToExcel(memberList, new String[]{"No", "센터명", "사용자명", "코인적립", "코인사용", "최근 사용일"});

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "attachment; filename=statMemberList.xlsx");

        return ResponseEntity
                .ok()
                .headers(headers)
                .body(new InputStreamResource(in));
    }
}
