package kr.co.ksbpartners.seochocoin.vo;

import kr.co.ksbpartners.seochocoin.entity.Activity;
import kr.co.ksbpartners.seochocoin.entity.EntityBase;
import lombok.Data;

import java.util.List;

@Data
public class ActivityVO extends EntityBase {
    Activity activity;
    List<MemberActivityVO> memberActivities;

    long numOfMembers;
    long coinUsed;
    long coinAdded;

    private long no;
    private long coinPlusSum;
    private long coinMinusSum;
}
