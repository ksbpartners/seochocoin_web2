package kr.co.ksbpartners.seochocoin.controller;

import kr.co.ksbpartners.seochocoin.constants.Constants;
import kr.co.ksbpartners.seochocoin.entity.ActivityCategory;
import kr.co.ksbpartners.seochocoin.entity.Center;
import kr.co.ksbpartners.seochocoin.entity.Member;
import kr.co.ksbpartners.seochocoin.repository.CenterMapper;
import kr.co.ksbpartners.seochocoin.service.*;
import kr.co.ksbpartners.seochocoin.util.Utils;
import kr.co.ksbpartners.seochocoin.util.constraint.PasswordConstraintValidator;
import kr.co.ksbpartners.seochocoin.util.crypto.CryptoUtil;
import kr.co.ksbpartners.seochocoin.vo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.text.DecimalFormat;
import java.util.*;

@RestController
@RequestMapping("/api")
public class ApiMemberController {
    DecimalFormat df = new DecimalFormat("#,###");

    @Autowired
    ApiMemberService apiMemberService;

    @Autowired
    ActivityService activityService;

    @Autowired
    ApiActivityService apiActivityService;

    @Autowired
    MemberService memberService;

    @Autowired
    CenterMapper centerMapper;

    @Autowired
    ActivityCategoryService activityCategoryService;

    public void setApiMemberService(ApiMemberService apiMemberService) {
        this.apiMemberService = apiMemberService;
    }

    public void setActivityService(ActivityService activityService) {
        this.activityService = activityService;
    }

    @RequestMapping("/test")
    public String test() {
        return "OK";
    }

    @PostMapping("/login")
    public Map<String, Object> login(String mobile, String pwd) {
        Map<String, Object> result = new HashMap<>();
        Member member = apiMemberService.loginByMobile(mobile);

        if (member == null) {
            member = apiMemberService.loginByMobile(Utils.CONVERT_MOBILE2(mobile));
        }

        if (member != null) {
            String dbPwd = null;
            try {
                dbPwd = CryptoUtil.decryptAES256(member.getPwd(), Constants.SALT_KEY);
            } catch (Exception e) {
                dbPwd = member.getPwd();
            }

            if (dbPwd.equals(pwd)) {
                result.put(Constants.RESULT, Constants.RESULT_OK);
                try {
                    String token = CryptoUtil.encryptAES256(member.getId(), Constants.SALT_KEY);

                    member.setToken(token);
                    member.setUpdatedBy(member.getId());
                    apiMemberService.updateToken(member);

                    result.put(Constants.TOKEN, token);
                    result.put(Constants.UID, member.getId());
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            } else {
                result.put(Constants.RESULT, Constants.LOGIN_FAIL);
            }
        } else {
            result.put(Constants.RESULT, Constants.LOGIN_FAIL);
        }

        return result;
    }

    @PostMapping("/logout")
    public Map<String, Object> logout(String uid) {
        Map<String, Object> result = new HashMap<>();
        Member member = new Member();
        member.setToken("logout");
        member.setId(uid);
        member.setUpdatedBy(uid);

        apiMemberService.updateToken(member);
        result.put(Constants.RESULT, Constants.RESULT_OK);

        return result;
    }

    @PostMapping("/checkToken")
    public Map<String, Object> checkTokenByMobile(String mobile, String token) {
        Map<String, Object> result = new HashMap<>();
        List<Member> member = apiMemberService.getTokenByMobile(mobile);

        if (member != null && !member.isEmpty() && member.get(0).getToken().equals(token)) {
            try {
                String dev = CryptoUtil.decryptAES256(token, Constants.SALT_KEY);
                if (member.get(0).getId().equals(dev)) {
                    result.put(Constants.RESULT, Constants.RESULT_OK);
                    result.put(Constants.TOKEN, member.get(0).getToken());
                    result.put(Constants.UID, member.get(0).getId());
                } else {
                    result.put(Constants.RESULT, Constants.RESULT_NG);
                }
            } catch (Exception e) {
                e.printStackTrace();
                result.put(Constants.RESULT, Constants.RESULT_NG);
            }
        }

        return result;
    }

    @RequestMapping("/checkMobile")
    public Map<String, Object> checkMobile(String mobile) {
        Map<String, Object> result = new HashMap<>();

        List<Member> _member = apiMemberService.getTokenByMobile(mobile);

        if (_member != null && !_member.isEmpty() && !_member.get(0).getId().isEmpty()) {
            result.put(Constants.RESULT, Constants.DUPLICATE_KEY);
        } else {
            result.put(Constants.RESULT, Constants.RESULT_OK);
        }

        return result;
    }

    @PostMapping("/joinMember")
    public Map<String, Object> joinMember(@RequestBody Member member) throws Exception {
        Map<String, Object> result = new HashMap<>();

        StringTokenizer st = new StringTokenizer(member.getCenter(), "|");
        member.setCenter1(st.nextToken());
        member.setCenter1Nm(st.nextToken());
        member.setCreatedBy(member.getName());
        member.setUpdatedBy(member.getName());

        if (member.getBirth() != null && !StringUtils.isEmpty(member.getBirth())) {
            member.setBirth(Utils.CHANGE_DATEFORMAT_HYPEN(member.getBirth()));
        }

        member.setPwd(CryptoUtil.encryptAES256(member.getPwd(), Constants.SALT_KEY));

        List<Member> _member = apiMemberService.getTokenByMobile(member.getMobile());
        if (_member != null && !_member.isEmpty() && !_member.get(0).getId().isEmpty()) {
            result.put(Constants.RESULT, Constants.DUPLICATE_KEY);
        } else {
            if (!StringUtils.isEmpty(memberService.add(member))) {
                result.put(Constants.RESULT, Constants.RESULT_OK);
            } else {
                result.put(Constants.RESULT, Constants.RESULT_NG);
            }
        }

        return result;
    }

    @PostMapping("/getInfo")
    public Map<String, Object> getInfoMain(String uid, String token) {
        Map<String, Object> result = checkToken(uid, token);

        if (!result.containsKey(Constants.RESULT)) {
            Member _member = apiMemberService.getById(uid);

            Member _rank = apiMemberService.getRankingById(uid);
            if (_rank == null) {
                _rank = new Member();
                _rank.setRanking(0);
            }

            result.put(Constants.RESULT, Constants.RESULT_OK);
            result.put("uid", _member.getId());
            result.put("name", _member.getName());
            result.put("coin", df.format(_member.getCoin()));
            result.put("ranking", df.format(_rank.getRanking()));
        }

        return result;
    }

    @GetMapping("/getInfo/{uid}")
    public Map<String, Object> getInfoMain(@PathVariable  String uid) {
        Map<String, Object> result = new HashMap<>();

            Member _member = apiMemberService.getById(uid);

            Member _rank = apiMemberService.getRankingById(uid);
            if (_rank == null) {
                _rank = new Member();
                _rank.setRanking(0);
            }

            result.put(Constants.RESULT, Constants.RESULT_OK);
            result.put("uid", _member.getId());
            result.put("name", _member.getName());
            result.put("coin", df.format(_member.getCoin()));
            result.put("ranking", df.format(_rank.getRanking()));

        return result;
    }

    @RequestMapping("/setting")
    public Map<String, Object> getInfoSetting(String uid, String token) {
        Map<String, Object> result = checkToken(uid, token);

        if (!result.containsKey(Constants.RESULT)) {
            Member _member = apiMemberService.getById(uid);

            result.put(Constants.RESULT, Constants.RESULT_OK);
            result.put("uid", _member.getId());
            result.put("name", _member.getName());
            result.put("mobile", _member.getMobile());
            result.put("birth", Utils.CHANGE_DATEFORMAT_NOHYPEN(_member.getBirth()));
            result.put("postal_code", _member.getPostalCode());
            result.put("addr_doro", _member.getAddrDoro());
            result.put("addr_jibun", _member.getAddrJibun());
            result.put("addr_detail", _member.getAddrDetail());
        }

        return result;
    }

    @PostMapping("/join/center")
    public Map<String, Object> getInfoCenter4Join() {
        Map<String, Object> result = new HashMap<>();
        // retrieve center list
        List<Center> centerList = centerMapper.selectAll(null, null, null, null);

        Map<String, String> centerMap = new HashMap<>();
        for (Center _c : centerList) {
            centerMap.put(_c.getId(), _c.getName());
        }

        result.put(Constants.RESULT, Constants.RESULT_OK);
        result.put("all_centers", centerList);

        return result;
    }

    @PostMapping("/setting/center")
    public Map<String, Object> getInfoCenter(String uid, String token) {
        Map<String, Object> result = checkToken(uid, token);

        if (!result.containsKey(Constants.RESULT)) {
            // retrieve center list
            List<Center> centerList = centerMapper.selectAll(null, null, null, null);

            Map<String, String> centerMap = new HashMap<>();
            for (Center _c : centerList) {
                centerMap.put(_c.getId(), _c.getName());
            }

            Member _member = apiMemberService.getById(uid);

            List<Center> centers = new ArrayList<Center>();
            if (_member.getCenter1() != null && !_member.getCenter1().isEmpty()) {
                centers.add(new Center(_member.getCenter1(), centerMap.get(_member.getCenter1())));
            }
            if (_member.getCenter2() != null && !_member.getCenter2().isEmpty()) {
                centers.add(new Center(_member.getCenter2(), centerMap.get(_member.getCenter2())));
            }
            if (_member.getCenter3() != null && !_member.getCenter3().isEmpty()) {
                centers.add(new Center(_member.getCenter3(), centerMap.get(_member.getCenter3())));
            }

            result.put(Constants.RESULT, Constants.RESULT_OK);
            result.put("uid", _member.getId());
            result.put("my_centers", centers);
            result.put("all_centers", centerList);
        }

        return result;
    }

    @PostMapping("/setting/keyword")
    public Map<String, Object> getInfoKeyword(String uid, String token, String type) {
        Map<String, Object> result = checkToken(uid, token);

        if (!result.containsKey(Constants.RESULT)) {
            // retrieve keyword(category) list
            List<ActivityCategoryVO> keywordList = activityCategoryService.getAllByType(type);

            SortedMap<String, String> keywordMap = new TreeMap<>();
            for (ActivityCategoryVO _ac : keywordList) {
                keywordMap.put(_ac.getId(), _ac.getActName());
            }

            Member _member = apiMemberService.getById(uid);

            List<ActivityCategory> keywords = new ArrayList<>();
            if (_member.getKeyword1() != null && !_member.getKeyword1().isEmpty()) {
                keywords.add(new ActivityCategory(_member.getKeyword1(), keywordMap.get(_member.getKeyword1())));
            }
            if (_member.getKeyword2() != null && !_member.getKeyword2().isEmpty()) {
                keywords.add(new ActivityCategory(_member.getKeyword2(), keywordMap.get(_member.getKeyword2())));
            }
            if (_member.getKeyword3() != null && !_member.getKeyword3().isEmpty()) {
                keywords.add(new ActivityCategory(_member.getKeyword3(), keywordMap.get(_member.getKeyword3())));
            }

            result.put(Constants.RESULT, Constants.RESULT_OK);
            result.put("uid", _member.getId());
            result.put("my_keywords", keywords);
            result.put("all_keywords", keywordList);
        }

        return result;
    }

    @PutMapping("/setting/pwd")
    public Map<String, Object> modifyPwd(String uid, String token,
                                      @RequestBody  Member member) {
        Map<String, Object> result = checkToken(member.getId(), member.getToken());

        if (!result.containsKey(Constants.RESULT)) {
            Member _member = apiMemberService.getById(member.getId());

            String dbPwd = null;
            try {
                dbPwd = CryptoUtil.decryptAES256(_member.getPwd(), Constants.SALT_KEY);
            } catch (Exception e) {
                dbPwd = _member.getPwd();
            }

            if (_member != null && dbPwd.equals(member.getOld_pwd())) {
                member.setUpdatedBy(member.getId());

                try {
                    PasswordConstraintValidator validator = new PasswordConstraintValidator();
                    String pwd = member.getPwd();
                    if (!validator.isValid(pwd, null)) {
                        throw new Exception("비밀번호는 영문,숫자,특수문자 조합 8자리 이상이어야 합니다.");
                    } else {
                        member.setPwd(CryptoUtil.encryptAES256(member.getPwd(), Constants.SALT_KEY));
                    }

                    apiMemberService.updatePwd(member);

                    result.put(Constants.RESULT, Constants.RESULT_OK);
                } catch (Exception e) {
                    e.printStackTrace();
                    result.put(Constants.RESULT, Constants.WRONG_PWD_TYPE);
                }
            } else {
                result.put(Constants.RESULT, Constants.WRONG_PWD);
            }
        }

        return result;
    }

    @PutMapping("/setting")
    public Map<String, Object> modify(String uid, String token,
                                      @RequestBody  Member member,
                                      HttpServletRequest request) {
        Map<String, Object> result = checkToken(member.getId(), member.getToken());

        if (!result.containsKey(Constants.RESULT)) {
            member.setUpdatedBy(member.getId());

            if (member.getBirth() != null && !StringUtils.isEmpty(member.getBirth())) {
                member.setBirth(Utils.CHANGE_DATEFORMAT_HYPEN(member.getBirth()));
            }

            apiMemberService.modify(member);

            result.put(Constants.RESULT, Constants.RESULT_OK);
        }

        return result;
    }

    @PutMapping("/setting/keyword")
    public Map<String, Object> modifyKeyword(String uid, String token,
                                            @RequestBody  Member member) {
        Map<String, Object> result = checkToken(member.getId(), member.getToken());

        if (!result.containsKey(Constants.RESULT)) {
            member.setUpdatedBy(member.getId());

            Utils.SET_KEYWORD(member);
            apiMemberService.modifyKeyword(member);

            result.put(Constants.RESULT, Constants.RESULT_OK);
        }

        return result;
    }

    @PutMapping("/setting/center")
    public Map<String, Object> modifyCenter(String uid, String token,
                                            @RequestBody  Member member) {
        Map<String, Object> result = checkToken(member.getId(), member.getToken());

        if (!result.containsKey(Constants.RESULT)) {
            member.setUpdatedBy(member.getId());

            if (member.getCenter() != null && !member.getCenter().isEmpty()) {
                Utils.SET_CENTER(member);
                apiMemberService.modifyCenter(member);
            }

            result.put(Constants.RESULT, Constants.RESULT_OK);
        }

        return result;
    }

    @PostMapping("/usage_list")
    public Map<String, Object> getUsageList(String uid, String token) {
        Map<String, Object> result = checkToken(uid, token);

        if (!result.containsKey(Constants.RESULT)) {
//            List<UsageInfo> coinList = apiMemberService.getAllUsageList();
//
//            result.put(Constants.RESULT, Constants.RESULT_OK);
//            result.put(Constants.LIST, coinList);
//            result.put(Constants.COUNT, coinList != null && !coinList.isEmpty() ? coinList.size() : 0);
            /*
             * this usage_list will be used from 2020
             */
            result.put(Constants.RESULT, Constants.RESULT_OK);
            result.put(Constants.COUNT, 0);
        }

        return result;
    }

    @PostMapping("/usage_list/{id}")
    public Map<String, Object> getUsageList(String uid, String token,
                                               @PathVariable String id) {
        Map<String, Object> result = checkToken(uid, token);

        if (!result.containsKey(Constants.RESULT)) {
            UsageInfo usageInfo = apiMemberService.getAllUsageListById(id);
            usageInfo.setPrice(df.format(Integer.parseInt(usageInfo.getPrice())));

            result.put(Constants.RESULT, Constants.RESULT_OK);
            result.put(Constants.USAGE, usageInfo);
        }

        return result;
    }

    @PostMapping("/mylist/activities")
    public Map<String, Object> getMyActivities(String uid, String token) {
        Map<String, Object> result = checkToken(uid, token);

        if (!result.containsKey(Constants.RESULT)) {
            List<MyActivityInfo> activityList = apiMemberService.getActivitiesByUid(uid);

            result.put(Constants.RESULT, Constants.RESULT_OK);
            result.put(Constants.LIST, activityList);
            result.put(Constants.COUNT, activityList != null && !activityList.isEmpty() ? activityList.size() : 0);
        }

        return result;
    }

    @PostMapping("/mylist/activities/{a_id}")
    public Map<String, Object> getMyActivities(String uid, String token,
                                               @PathVariable String a_id) {
        Map<String, Object> result = checkToken(uid, token);

        if (!result.containsKey(Constants.RESULT)) {
            MyActivityInfo activityInfo = apiMemberService.getActivityInfoById(uid, a_id);
            activityInfo.setCoinPrice(df.format(Long.parseLong(activityInfo.getAddCoinVal()) * Constants.BASE_COIN_PRICE));
            activityInfo.setTimeTables(Utils.SET_TIMETABLES(activityInfo));

            result.put(Constants.RESULT, Constants.RESULT_OK);
            result.put(Constants.ACTIVITY, activityInfo);
        }

        return result;
    }

    @PostMapping("/mylist/coin")
    public Map<String, Object> getMyCoinHistory(String uid, String token) {
        Map<String, Object> result = checkToken(uid, token);

        if (!result.containsKey(Constants.RESULT)) {
            List<MyCoinInfo> coinList = apiMemberService.getCoinHistoryByUid(uid);

            result.put(Constants.RESULT, Constants.RESULT_OK);
            result.put(Constants.LIST, coinList);
            result.put(Constants.COUNT, coinList != null && !coinList.isEmpty() ? coinList.size() : 0);
        }

        return result;
    }

    @PostMapping("/mylist/coin/{c_id}")
    public Map<String, Object> getMyCoin(String uid, String token,
                                         @PathVariable String c_id) {
        Map<String, Object> result = checkToken(uid, token);

        if (!result.containsKey(Constants.RESULT)) {
            MyCoinInfo coinInfo = apiMemberService.getCoinInfoById(c_id);
            coinInfo.setPaid(df.format(Integer.parseInt(coinInfo.getPaid())));

            result.put(Constants.RESULT, Constants.RESULT_OK);
            result.put(Constants.COIN, coinInfo);
        }

        return result;
    }

    @PostMapping("/myinfo")
    public Map<String, Object> getMyInfo(String uid, String token) {
        Map<String, Object> result = checkToken(uid, token);

        if (!result.containsKey(Constants.RESULT)) {
            List<MyActivityInfo> activityList = apiMemberService.getActivitiesByUid(uid);
            List<MyCoinInfo> coinList = apiMemberService.getCoinHistoryByUid(uid);

            result.put(Constants.RESULT, Constants.RESULT_OK);
            result.put(Constants.AC_LIST, activityList);
            result.put(Constants.COIN_LIST, coinList);
            result.put(Constants.AC_COUNT, coinList != null && !coinList.isEmpty() ? coinList.size() : 0);
            result.put(Constants.COIN_COUNT, coinList != null && !coinList.isEmpty() ? coinList.size() : 0);
        }

        return result;
    }

    private Map<String, Object> checkToken(String uid, String token) {
        Map<String, Object> result = new HashMap<>();

        Member _member = apiMemberService.getTokenById(uid);
        if (_member == null) {
            result.put(Constants.RESULT, Constants.NOT_EXISTS);
        } else {
            if (token == null || !token.equals(_member.getToken())) {
                result.put(Constants.RESULT, Constants.EXPIRED_TOKEN);
            } else {
                result.clear();
            }
        }

        return result;
    }
}
