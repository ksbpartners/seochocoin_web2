package kr.co.ksbpartners.seochocoin.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class NcomInfoTxContents implements Serializable {
    private String usage_id;
    private String usage_title;
    private long coin_amount;
    private String etc;

    public NcomInfoTxContents() {}

    public NcomInfoTxContents(String usage_id, String usage_title, String etc) {
        this.usage_id = usage_id;
        this.usage_title = usage_title;
        this.etc = etc;
    }
}
