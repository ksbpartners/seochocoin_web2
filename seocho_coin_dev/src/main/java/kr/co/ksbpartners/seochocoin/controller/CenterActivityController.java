package kr.co.ksbpartners.seochocoin.controller;


import kr.co.ksbpartners.seochocoin.common.userdetails.CentUser;
import kr.co.ksbpartners.seochocoin.constants.Constants;
import kr.co.ksbpartners.seochocoin.entity.*;
import kr.co.ksbpartners.seochocoin.service.*;
import kr.co.ksbpartners.seochocoin.vo.ActivityCategoryVO;
import kr.co.ksbpartners.seochocoin.vo.ActivityScheduleVO;
import kr.co.ksbpartners.seochocoin.vo.ActivityVO;
import kr.co.ksbpartners.seochocoin.vo.MemberActivityVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/cent")
public class CenterActivityController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    ActivityService activityService;

    @Autowired
    ActivityCategoryService activityCategoryService;

    @Autowired
    CoinLegerService coinLegerService;

    @Autowired
    MemberService memberService;

    @Autowired
    ApiActivityService apiActivityService;

    @GetMapping("/activity-list")
    public String activityList(@RequestParam(required = false) String keyword,
                                         @RequestParam(required = false) String fromDate,
                                         @RequestParam(required = false) String toDate,
                                         Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        List<ActivityVO> lst = null;
        Center cent = ((CentUser)auth.getPrincipal()).getCenter();
        if (null != cent) {
            lst = activityService.getAll(cent.getId(), fromDate, toDate, keyword);
        }

        model.addAttribute("activities", lst);
        model.addAttribute("fromDate", fromDate);
        model.addAttribute("toDate", toDate);
        model.addAttribute("keyword", keyword);
        return "cent/activity-list";
    }

    @GetMapping("/activity-register-list")
    public String activityRegisterList(@RequestParam(required = false) String activityId,
                                 @RequestParam(required = false) String keyword,
                                 @RequestParam(required = false) String fromDate,
                                 @RequestParam(required = false) String toDate,
                                 Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Center cent = ((CentUser)auth.getPrincipal()).getCenter();

        List<Activity> lst = null;
        Activity activity = null;
        List<ActivitySchedule> schedules = null;
        List<HashMap<String, String>> scheduleMapList = null;
        List<MemberActivityVO> memberActivities = null;
        if (null != cent) {
            // 활동처명 리스트 취득
            lst = activityService.getActivityListByCenterId(cent.getId());
            // 대상 활동정보
            if (null != activityId) {
                activity = lst.stream().filter(v -> activityId.equals(v.getId())).findFirst().orElse(null);

                // retrieve coin
                List<CoinLeger> tot = coinLegerService.getTotCoin(activity.getId(), null, null);
                if (tot != null) {
                    for (CoinLeger coinLeger : tot) {
                        if ("적립".equals(coinLeger.getType())) {
                            activity.setAddCoinVal(coinLeger.getCoin());
                        } else if ("사용".equals(coinLeger.getType())) {
                            activity.setUseCoinVal(coinLeger.getCoin());
                        }
                    }
                }

                schedules = activityService.cntMembersByActivityId(activityId);
                // 달력표시
                scheduleMapList = new ArrayList<>();
                for (ActivitySchedule schedule : schedules) {
                    HashMap scheduleMap = new HashMap<String, String>();
                    scheduleMap.put("title", "  " + schedule.getMemberName() + " 명");
                    scheduleMap.put("start", schedule.getActivityDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))
                            + "T" + schedule.getStart());
                    scheduleMap.put("end", schedule.getActivityDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))
                            + "T" + schedule.getEnd());
                    scheduleMapList.add(scheduleMap);
                }
            }
            // 대상 활동내 사용자 리스트 취득
            memberActivities = activityService.getMemberActivityList(cent.getId(), activityId, fromDate, toDate, keyword);
        }

        model.addAttribute("activityId", activityId);
        model.addAttribute("fromDate", fromDate);
        model.addAttribute("toDate", toDate);
        model.addAttribute("keyword", keyword);
        model.addAttribute("activities", lst);
        model.addAttribute("activity", activity);
        model.addAttribute("memberActivities", memberActivities);
        model.addAttribute("calendarSchedule", scheduleMapList);
        return "cent/activity-register-list";
    }

    @GetMapping("/activity-schedule-modal")
    public String activityScheduleModal(@RequestParam String activityId,
                                       @RequestParam String activityDate,
                                       Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Center cent = ((CentUser)auth.getPrincipal()).getCenter();
        List<ActivitySchedule> schedules = null;
        if (null != cent) {
            schedules = activityService.getMembersByActivityDate(activityId, activityDate);
        }

        model.addAttribute("schedules", schedules);
        return "cent/activity-schedule-modal";
    }

    @GetMapping("/activity-add")
    public String activityAdd(@RequestParam(required = false) String id, Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Center cent = ((CentUser)auth.getPrincipal()).getCenter();

        List<Activity> lst = null;
        List<ActivityCategoryVO> categoty_list = null;
        Activity ret = null;
        if (null != cent) {
            // 활동처명 리스트 취득
            lst = activityService.getActivityListByCenterId(cent.getId());
            // 카테고리(키워드) 리스트 취득
            categoty_list = activityCategoryService.getAllByType(Constants.CATEGORY_ACTIVITY);
        }
        if (StringUtils.isEmpty(id)) {
            ret = new Activity();

            model.addAttribute("is_modified", "modified");
        } else {
            ret = activityService.getActivityById(id);
            List<MemberActivityVO> memberActivityList = activityService.getMemberActivityList(cent.getId(), id, null, null, null);

            model.addAttribute("is_modified", memberActivityList == null || memberActivityList.isEmpty() ? "modified" : "");
        }

        model.addAttribute("categories", categoty_list);
        model.addAttribute("activities", lst);
        model.addAttribute("activity", ret);

        return "cent/activity-add";
    }

    @GetMapping("/activity-load")
    @ResponseBody
    public Activity activityLoad(@RequestParam String id) {
        Activity  ret= activityService.getActivityById(id);
        return ret;
    }

    @PostMapping("/activity-add")
    public String activityAdd(@ModelAttribute("activity") @Validated Activity activity,
                              BindingResult bindingResult, Model model) throws Exception {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (bindingResult.hasErrors()) {
            return "cent/activity-add";
        }

        activity.setUpdatedBy(auth.getName());
        if ("Y".equals(activity.getOpenActivity())) {
            activity.setStatus("모집중");
        } else {
            activity.setStatus("모집전");
        }

        if (StringUtils.isEmpty(activity.getId())) {
            Center cent = ((CentUser)auth.getPrincipal()).getCenter();
            activity.setCenterId(cent.getId());
            activity.setCreatedBy(auth.getName());

            activity.setId(activityService.add(activity));
        } else {
            activityService.modify(activity);
        }

        return activityAdd(activity.getId(), model);
    }

    @GetMapping("/activity-regist")
    public String activityRegist(@RequestParam(required = false) String id, Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Center cent = ((CentUser)auth.getPrincipal()).getCenter();

        List<Activity> lst = null;
        ActivityScheduleVO schedule = null;
        List<ActivitySchedule> schedules = null;
        if (null != cent) {
            // 활동처명 리스트 취득
            lst = activityService.getActivityListByCenterId(cent.getId());
        }
        if (!StringUtils.isEmpty(id)) {
            schedule = activityService.getDaySchedule(id);
            schedules = activityService.getMembersByActivityDate(id, null);
        } else {
            schedule = new ActivityScheduleVO();
//            schedules = new ArrayList<>();
        }

        model.addAttribute("activities", lst);
        model.addAttribute("schedule", schedule);
        model.addAttribute("schedules", schedules);

        return "cent/activity-regist";
    }

    @PostMapping("/activity-regist")
    public String activityRegist(@ModelAttribute("schedule") @Validated ActivityScheduleVO schedule,
                                 BindingResult bindingResult, Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (bindingResult.hasErrors()) {
            return "cent/activity-regist";
        }

        schedule.getActivity().setUpdatedBy(auth.getName());
        // 중복삭제
        schedule.setMonMemIds(schedule.getMonMemIds().stream().distinct().collect(Collectors.toList()));
        schedule.setTueMemIds(schedule.getTueMemIds().stream().distinct().collect(Collectors.toList()));
        schedule.setWedMemIds(schedule.getWedMemIds().stream().distinct().collect(Collectors.toList()));
        schedule.setThuMemIds(schedule.getThuMemIds().stream().distinct().collect(Collectors.toList()));
        schedule.setFriMemIds(schedule.getFriMemIds().stream().distinct().collect(Collectors.toList()));

        activityService.modifySchedule(schedule);

        return activityRegist(schedule.getActivity().getId(), model);
    }

    @GetMapping("/activity-regist-oneShot")
    public String activityRegistOneShot(@RequestParam String activityId,
                                        @RequestParam List<String> oneMemIds,
                                        @RequestParam String oneShotDate,
                                        Model model,
                                        RedirectAttributes redirectAttribute) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        // 중복삭제
        oneMemIds = oneMemIds.stream().distinct().collect(Collectors.toList());

        activityService.modifyScheduleOneShot(activityId, oneMemIds, oneShotDate, auth.getName());

        redirectAttribute.addAttribute("id", activityId);
        return "redirect:activity-regist";
    }

    @GetMapping("/activity-download")
    public ResponseEntity<InputStreamResource> activityDownload(@RequestParam(required = false) String keyword,
                                                                    @RequestParam(required = false) String fromDate,
                                                                    @RequestParam(required = false) String toDate,
                                                                    Model model) throws IOException {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        List<ActivityVO> lst = new ArrayList<>();
        Center cent = ((CentUser)auth.getPrincipal()).getCenter();
        if (null != cent) {
            lst = activityService.getAll(cent.getId(), fromDate, toDate, keyword);
        }
        List<Activity> actList = new ArrayList<>();
        if (null != lst && !lst.isEmpty()) {
            for (ActivityVO data : lst) {
                actList.add(data.getActivity());
            }
        }

        ByteArrayInputStream in = activityService.generateActivityListFile(actList);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "attachment; filename=activityList.xlsx");

        return ResponseEntity
                .ok()
                .headers(headers)
                .body(new InputStreamResource(in));
    }

    @PostMapping("/activity-upload")
    public String activityUpload(@RequestParam("elmFile") MultipartFile multipartFile,
                                    RedirectAttributes redirectAttributes) {
        // ファイルが空の場合は異常終了
        if (multipartFile.isEmpty()) {
            // TODO error
        }
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Center cent = ((CentUser)auth.getPrincipal()).getCenter();

        logger.info("upload file {}", multipartFile.getOriginalFilename());
        int uploadCnt = 0;
        String msg = null;
        try {
            uploadCnt = activityService.uploadActivityListFile(cent, multipartFile);
            msg = "업로드 성공: " + multipartFile.getOriginalFilename() + " 등록 수: " + uploadCnt + "건";
        } catch (Exception e) {
            msg = e.getMessage();
        }

        logger.info(msg);

        redirectAttributes.addFlashAttribute("message", msg);
        return "redirect:/cent/activity-list";
    }

    /**
     * 활동별 회원 내역 다운로드
     */
    @GetMapping("/activityMember-download")
    public ResponseEntity<InputStreamResource> activityMemberDownload(@RequestParam(required = false) String activityId,
                                                                      @RequestParam(required = false) String keyword,
                                                                      @RequestParam(required = false) String fromDate,
                                                                      @RequestParam(required = false) String toDate,
                                                                      Model model) throws IOException {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        Center cent = ((CentUser)auth.getPrincipal()).getCenter();
        List<MemberActivityVO> memberActivities = null;
        if (null != cent) {
            // 대상 활동내 사용자 리스트 취득
            memberActivities = activityService.getMemberActivityList(cent.getId(), activityId, fromDate, toDate, keyword);
        }

        ByteArrayInputStream in = activityService.genMemberActivityListFile(memberActivities);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "attachment; filename=activityMemberList.xlsx");

        return ResponseEntity
                .ok()
                .headers(headers)
                .body(new InputStreamResource(in));
    }

    /**
     * 회원별 활동 내역 다운로드
     */
    @GetMapping("/member-activity-download")
    public ResponseEntity<InputStreamResource> memberActivityDownload(@RequestParam String memberId,
                                                                      @RequestParam String activityId,
                                                                      @RequestParam(required = false) String memFromDate,
                                                                      @RequestParam(required = false) String memToDate,
                                                                      Model model) throws IOException {

        List<ActivitySchedule> ret = memberService.getMemberScheduleList(memberId, activityId, memFromDate, memToDate);

        ByteArrayInputStream in = activityService.genMemberActivityScheduleListFile(ret);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "attachment; filename=memberActivitySchedules.xlsx");

        return ResponseEntity
                .ok()
                .headers(headers)
                .body(new InputStreamResource(in));
    }

    @PostMapping("/activity-delete")
    public String deleteActivity(@RequestParam List<String> delIds,
                               @RequestParam(required = false) String keyword,
                               @RequestParam(required = false) String fromDate,
                               @RequestParam(required = false) String toDate, Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        // ファイルが空の場合は異常終了
        if (delIds.isEmpty()) {
            // TODO error
        }

        activityService.remove(delIds, auth.getName());

        return activityList(keyword, fromDate, toDate, model);
    }

    @PostMapping("/activity-member-delete")
    public String deleteActivityMember(@RequestParam List<String> delIds,
                                       @RequestParam("activityId") String activityId,
                                       @RequestParam(required = false) String keyword,
                                       @RequestParam(required = false) String fromDate,
                                       @RequestParam(required = false) String toDate, Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        // ファイルが空の場合は異常終了
        if (delIds.isEmpty()) {
            // TODO error
        }

        activityService.removeMembers(activityId, delIds, auth.getName());

        return activityRegisterList(activityId, keyword, fromDate, toDate, model);
    }

    @GetMapping("/member-activity-statusChange")
    public String changeMemberActivityStatus(@RequestParam String memberId,
                                             @RequestParam String activityId,
                                             @RequestParam String activityStatus,
                                             @RequestParam(required = false) String viewActivityId,
                                             @RequestParam(required = false) String keyword,
                                             @RequestParam(required = false) String fromDate,
                                             @RequestParam(required = false) String toDate, Model model,
                                             RedirectAttributes redirectAttribute) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if (Constants.ACTIVITY_JOIN_OK.equals(activityStatus)) {
            activityService.changeMemberActivityStatus(memberId, activityId, activityStatus, auth.getName());
        } else {
            // 상태를 취소 대신 데이터를 삭제 (activity_schedule, member_activity)
            activityService.removeActivityMemberAndSchedule(activityId, memberId, auth.getName());
        }

        redirectAttribute.addAttribute("activityId", viewActivityId);
        redirectAttribute.addAttribute("keyword", keyword);
        redirectAttribute.addAttribute("fromDate", fromDate);
        redirectAttribute.addAttribute("toDate", toDate);
        return "redirect:activity-register-list";
    }

    @GetMapping("/member-accum-statusChange")
    public String changeMemberAccumStatus(@RequestParam String memberId,
                                             @RequestParam String activityId,
                                             @RequestParam String accumStatus,
                                             @RequestParam(required = false) String viewActivityId,
                                             @RequestParam(required = false) String keyword,
                                             @RequestParam(required = false) String fromDate,
                                             @RequestParam(required = false) String toDate, Model model,
                                             RedirectAttributes redirectAttribute) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        activityService.changeMemberAccumStatus(memberId, activityId, accumStatus, auth.getName());

        redirectAttribute.addAttribute("activityId", viewActivityId);
        redirectAttribute.addAttribute("keyword", keyword);
        redirectAttribute.addAttribute("fromDate", fromDate);
        redirectAttribute.addAttribute("toDate", toDate);
        return "redirect:activity-register-list";
    }

    @GetMapping("/activity-schedule-attend")
    public String activityScheduleAttend(@RequestParam String memberId,
                                          @RequestParam String activityId,
                                          @RequestParam String activityDate, Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        auth.getAuthorities().forEach(v -> {
            if ("ROLE_CENT".equals(((GrantedAuthority) v).getAuthority())) {
                activityService.activityScheduleAttend(memberId, activityId, activityDate, auth.getName());

                Activity activity = activityService.getActivityById(activityId);
                updateCoinLeger(activity, memberId, activityDate, Constants.COIN_ADD);
            }
        });

        return activityScheduleModal(activityId, activityDate, model);
    }

    @GetMapping("/activity-schedule-attend-cancel")
    public String activityScheduleAttendCancel(@RequestParam String memberId,
                                         @RequestParam String activityId,
                                         @RequestParam String activityDate, Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        activityService.cancelScheduleAttend(memberId, activityId, activityDate, auth.getName());

        Activity activity = activityService.getActivityById(activityId);
        updateCoinLeger(activity, memberId, activityDate, Constants.COIN_CANCEL);

        return activityScheduleModal(activityId, activityDate, model);
    }

    private void updateCoinLeger(Activity activity, String memberId, String activityDate, int addOrCancle) {
        if ("N".equals(activity.getAttendCheck())) {
            CoinLeger coinLeger = new CoinLeger();
            coinLeger.setCenterId(activity.getCenterId());
            coinLeger.setActivityId(activity.getId());
            coinLeger.setMemberId(memberId);
            coinLeger.setType(addOrCancle == Constants.COIN_ADD ? "적립" : "적립취소");
            coinLeger.setCoin(addOrCancle * activity.getAddCoinVal());
            coinLeger.setPaid(addOrCancle * activity.getAddCoinVal() * Constants.BASE_COIN_PRICE);
            coinLeger.setActivityDate(LocalDate.parse(activityDate));
            coinLeger.setCreatedBy("sys");
            coinLeger.setUpdatedBy("sys");

            apiActivityService.addCoinLedger(coinLeger);

            Member member = new Member();
            member.setCoin(activity.getAddCoinVal());
            member.setId(memberId);
            member.setUpdatedBy("sys");
            if (addOrCancle == Constants.COIN_ADD) {
                apiActivityService.updateCoinOfMember(member);
            } else {
                apiActivityService.minusCoinOfMember(member);
            }
        }
    }

    @GetMapping("/activity-schedule-delete")
    public String activityScheduleDelete(@RequestParam String memberId,
                                         @RequestParam String activityId,
                                         @RequestParam String activityDate, Model model,
                                         RedirectAttributes redirectAttribute) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        activityService.activityScheduleDelete(memberId, activityId, activityDate, auth.getName());

        redirectAttribute.addAttribute("id", activityId);
        return "redirect:activity-regist";
    }


}
