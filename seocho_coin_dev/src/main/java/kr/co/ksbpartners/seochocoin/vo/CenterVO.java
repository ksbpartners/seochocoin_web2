package kr.co.ksbpartners.seochocoin.vo;

import kr.co.ksbpartners.seochocoin.entity.Admin;
import kr.co.ksbpartners.seochocoin.entity.Center;
import lombok.Data;

@Data
public class CenterVO {
    Center center;
    Admin admin;
}
