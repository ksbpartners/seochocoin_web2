package kr.co.ksbpartners.seochocoin.entity;

import lombok.Data;
import org.apache.ibatis.type.Alias;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Alias("activity_schedule")
public class ActivitySchedule extends EntityBase implements Serializable {
    private static final long serialVersionUID = 1L;

    private String memberId;
    private String memberName;
    private String memberMobile;
    private String activityId;
    private String activityName;
    private transient LocalDate activityDate;
    private String day;
    private String start;
    private String end;
    private transient LocalDateTime attendedAt;
    private String hasPeriod;

    public ActivitySchedule() {
    }

    public ActivitySchedule(String id, String day) {
        this.activityId = id;
        this.day = day;
    }

}
