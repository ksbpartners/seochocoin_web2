package kr.co.ksbpartners.seochocoin.util.excel;

import lombok.Data;

@Data
public class CellVO {
    private String content;
//    private String textColor;
//    private String bgColor;
//    private String textSize;
//    private String textWeight;

    public CellVO() { }
    public CellVO(String content) {
        this.content = content;
    }

    public long getLongVal() {
        try {
            return Long.parseLong(content);
        } catch (NumberFormatException e) {
            return 0;
        }

    }

    public int getintVal() {
        try {
            return Integer.parseInt(content);
        } catch (NumberFormatException e) {
            return 0;
        }

    }
}
