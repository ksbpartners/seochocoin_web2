package kr.co.ksbpartners.seochocoin.common.userdetails;

import kr.co.ksbpartners.seochocoin.entity.Center;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

public class DistUser extends User {

    public DistUser(String adminName, String username, String password, boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
        this.adminName = adminName;
    }

    private String adminName;
    public String getAdminName() {
        return this.adminName;
    }

}
