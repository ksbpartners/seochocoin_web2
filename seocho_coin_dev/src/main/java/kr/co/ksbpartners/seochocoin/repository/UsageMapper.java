package kr.co.ksbpartners.seochocoin.repository;

import kr.co.ksbpartners.seochocoin.entity.Usage;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface UsageMapper {

    Usage getById(String id);

    Usage getMaxId();

    int register(Usage usage);

    int modify(Usage usage);

    List<Usage> selectAll(String centerId, String fromDate, String toDate, String keyword, String activityId);

    int delete(List<String> ids, String updatedBy);

    List<Usage> selectAll4Stat(String centerId, String fromDate, String toDate, String keyword);
    long countAll4Stat(String centerId, String fromDate, String toDate, String keyword);
}