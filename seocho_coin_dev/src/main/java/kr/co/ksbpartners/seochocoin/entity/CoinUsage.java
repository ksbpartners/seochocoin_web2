package kr.co.ksbpartners.seochocoin.entity;

import lombok.Data;
import org.apache.ibatis.type.Alias;

import java.io.Serializable;
import java.time.LocalDate;

@Data
@Alias("coin_usage")
public class CoinUsage extends EntityBase implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String centerId;
    private String usageId;
    private String usageTitle;
    private String etc;
    private String memberId;
    private String memberName;
    private String memberMobile;
    private String type;
    private String txId;

    private long paid;
    private long coin;
    private String usageDate;

    private long coinSum;

    public CoinUsage() {
    }

    public CoinUsage(String centerId, String usageId, String memberId, String type, String userId) {
        super(userId);
        this.centerId = centerId;
        this.usageId = usageId;
        this.memberId = memberId;
        this.type = type;
    }
    
}
