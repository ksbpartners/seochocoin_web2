package kr.co.ksbpartners.seochocoin.util.crypto;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.ByteBuffer;
import java.security.AlgorithmParameters;
import java.security.SecureRandom;
import java.util.Base64;

public class CryptoUtil {
    public static String encryptAES256(String msg, String key) throws Exception {
        byte bytes[] = new byte[20];
        byte[] saltBytes = bytes;
        String result = msg;

        while (true) {
            SecureRandom random = new SecureRandom();
            random.nextBytes(bytes);

            SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            PBEKeySpec spec = new PBEKeySpec(key.toCharArray(), saltBytes, 100, 256);

            SecretKey secretKey = factory.generateSecret(spec);
            SecretKeySpec secret = new SecretKeySpec(secretKey.getEncoded(), "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, secret);

            AlgorithmParameters params = cipher.getParameters();

            byte[] ivBytes = params.getParameterSpec(IvParameterSpec.class).getIV();
            byte[] encryptedTextBytes = cipher.doFinal(msg.getBytes("UTF-8"));
            byte[] buffer = new byte[saltBytes.length + ivBytes.length + encryptedTextBytes.length];

            System.arraycopy(saltBytes, 0, buffer, 0, saltBytes.length);
            System.arraycopy(ivBytes, 0, buffer, saltBytes.length, ivBytes.length);
            System.arraycopy(encryptedTextBytes, 0, buffer, saltBytes.length + ivBytes.length, encryptedTextBytes.length);

            result = Base64.getEncoder().encodeToString(buffer);

            if (result.indexOf("+") == -1) {
                break;
            }
        }

        return result;
    }

    public static String decryptAES256(String msg, String key) throws Exception {
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        ByteBuffer buffer = ByteBuffer.wrap(Base64.getDecoder().decode(msg));
        byte[] saltBytes = new byte[20];
        buffer.get(saltBytes, 0, saltBytes.length);
        byte[] ivBytes = new byte[cipher.getBlockSize()];
        buffer.get(ivBytes, 0, ivBytes.length);
        byte[] encryoptedTextBytes = new byte[buffer.capacity() - saltBytes.length - ivBytes.length];
        buffer.get(encryoptedTextBytes);

        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        PBEKeySpec spec = new PBEKeySpec(key.toCharArray(), saltBytes, 100, 256);

        SecretKey secretKey = factory.generateSecret(spec);
        SecretKeySpec secret = new SecretKeySpec(secretKey.getEncoded(), "AES");

        cipher.init(Cipher.DECRYPT_MODE, secret, new IvParameterSpec(ivBytes));

        byte[] decryptedTextBytes = cipher.doFinal(encryoptedTextBytes);

        return new String(decryptedTextBytes);
    }

    public static void main(String[] args) throws Exception {
        String key = "seochocoin";

        String encrypted = CryptoUtil.encryptAES256("test1234" +
                "", key);
        System.out.println("AES-256 : enc - " + encrypted);
        System.out.println("AES-256 : dec - " + CryptoUtil.decryptAES256(encrypted, key));

        System.out.println(CryptoUtil.decryptAES256("alKv6mRWfg8oyVqN1s9NK8dxafJFcxemEQJ0p8MVhdpA01rtAknsEZ8wvQGYDGl3f9Ushg==", key));
        System.out.println(CryptoUtil.decryptAES256("22kL0oF7vdIzqEyMykQZIAi9SYZ484atb/uOIsBo7kct18NKKlIBIHcrgIE76W4vhEgU8g==", key));
    }
}
