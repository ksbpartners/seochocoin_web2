package kr.co.ksbpartners.seochocoin.util.thymeleaf;

import org.thymeleaf.context.IExpressionContext;
import org.thymeleaf.expression.IExpressionObjectFactory;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class ThymeleafExpressionObjectFactory implements IExpressionObjectFactory {
    private static final String thymeleafExpressionObjectName = "helper";

    @Override
    public Set<String> getAllExpressionObjectNames() {
        Set<String> nameSet = new HashSet<>();
        nameSet.add(thymeleafExpressionObjectName);
        return nameSet;
    }

    @Override
    public Object buildObject(IExpressionContext context, String expressionObjectName) {
        if (Objects.equals(thymeleafExpressionObjectName, expressionObjectName)) {
            return new CustUtil();
        }
        return null;
    }

    @Override
    public boolean isCacheable(String expressionObjectName) {
        return false;
    }
}
