package kr.co.ksbpartners.seochocoin.service;

import kr.co.ksbpartners.seochocoin.constants.Constants;
import kr.co.ksbpartners.seochocoin.entity.Admin;
import kr.co.ksbpartners.seochocoin.repository.AdminMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class AdminService extends AbstractService {

    @Autowired
    private AdminMapper mapper;

    public Admin getById(String id) {
        return mapper.getById(id);
    }

    public List<Admin> getAll() {
        return mapper.selectAll();
    }

    public String add(Admin admin) throws Exception{
        // timestamp
        LocalDateTime timeStamp = LocalDateTime.now();
        // get id
        Admin entity = mapper.getMaxId();
        String newId = getNewId(Constants.PREFIX_ADMIN_ID, null == entity ? "" : entity.getId());

        Admin newAdmin = new Admin();
        BeanUtils.copyProperties(admin, newAdmin);
        newAdmin.setId(newId);
        newAdmin.setCreatedAt(timeStamp);
        newAdmin.setUpdatedAt(timeStamp);

        int ret = mapper.register(newAdmin);
        if (ret != 1) {
            throw new Exception("Fail to generate new data.");
        }

        return newAdmin.getId();
    }

    public int modify(Admin admin) {
        // timestamp
        LocalDateTime timeStamp = LocalDateTime.now();
        admin.setUpdatedAt(timeStamp);

        return mapper.modify(admin);
    }

    public int remove(Admin admin) {
        // timestamp
        LocalDateTime timeStamp = LocalDateTime.now();
        admin.setUpdatedAt(timeStamp);

        return mapper.delete(admin);
    }


    public Admin getByDistId(String id) {
        return mapper.getByDistId(id);
    }
}