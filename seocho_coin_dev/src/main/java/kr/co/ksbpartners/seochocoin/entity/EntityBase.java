package kr.co.ksbpartners.seochocoin.entity;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class EntityBase implements Serializable {
    private static final long serialVersionUID = 1L;

    private transient LocalDateTime createdAt;
    private String createdBy;
    private transient LocalDateTime updatedAt;
    private String updatedBy;
    private String delFlg;

    public EntityBase() { }
    public EntityBase(String userId) {
        this.createdBy = userId;
        this.updatedBy = userId;
    }

}
