package kr.co.ksbpartners.seochocoin.repository;

import kr.co.ksbpartners.seochocoin.entity.*;
import kr.co.ksbpartners.seochocoin.vo.*;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface ApiActivityMapper {

    int registMember(MemberActivityVO vo);

    int cancelMember(MemberActivityVO vo);

    int cancelActivitySchedule(MemberActivityVO vo);

    List<ActivityInfo> getAllActivities(ActivityInfo activityInfo);

    ActivityInfo getAllActivitiesById(String aid);

    ActivitySchedule getAttendActivity(ActivitySchedule activitySchedule);

    int setAttendActivity(ActivitySchedule activitySchedule);

    Activity getAddCoinInfo(String activityId);

    int addCoinLedger(CoinLeger coinLeger);

    int delCoinLedger(CoinLeger coinLeger);

    int updateBaseCoinLedger(CoinLeger coinLeger);

    int updateCoinOfMember(Member member);

    int minusCoinOfMember(Member member);

    /* Coin Usage */
    int addCoinUsage(CoinUsage coinUsage);

    int delCoinUsage(CoinUsage coinUsage);

    List<CoinUsage> getCoinUsageInfo(CoinUsage coinUsage);
}