package kr.co.ksbpartners.seochocoin.controller;

import kr.co.ksbpartners.seochocoin.service.MemberService;
import kr.co.ksbpartners.seochocoin.vo.MemberVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping("/dist")
public class DistMemberController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private MemberService memberService;


    /**  센터 > 사용자 목록 */
    @GetMapping("/member-list")
    public String memberList(@RequestParam(required = false) String keyword, Model model) {
        logger.info("센터 > 사용자 목록. keyword={}", keyword);

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String adminId = auth.getName();

        List<MemberVO> members = memberService.getAll(null, keyword);

        model.addAttribute("members", members);
        model.addAttribute("keyword", keyword);
        return "dist/member-list";
    }

    @GetMapping("/member-download")
    public ResponseEntity<InputStreamResource> memberExcelDownload(@RequestParam("keyword")  String keyword)
            throws IOException {
        logger.info("센터 > 사용자 목록 > 엑셀 다운로드");

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        List<MemberVO> members = memberService.getAll(null, keyword);

        ByteArrayInputStream in = memberService.generateMemberListFile(members);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "attachment; filename=memeberList.xlsx");

        return ResponseEntity
                .ok()
                .headers(headers)
                .body(new InputStreamResource(in));
    }
}
