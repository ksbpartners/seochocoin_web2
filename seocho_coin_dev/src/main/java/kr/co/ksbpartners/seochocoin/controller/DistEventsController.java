package kr.co.ksbpartners.seochocoin.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/dist")
public class DistEventsController {
    @GetMapping("/events-add")
    public String eventsAdd() {
        return "dist/events-add";
    }

    @GetMapping("/events-list")
    public String eventsList() {
        return "dist/events-list";
    }

    @GetMapping("/faq-add")
    public String faqAdd() {
        return "dist/faq-add";
    }

    @GetMapping("/faq-list")
    public String faqList() {
        return "dist/faq-list";
    }

    @GetMapping("/notice-add")
    public String noticeAdd() {
        return "dist/notice-add";
    }

    @GetMapping("/notice-list")
    public String noticeList() {
        return "dist/notice-list";
    }
}
