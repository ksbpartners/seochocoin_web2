package kr.co.ksbpartners.seochocoin.vo;

import kr.co.ksbpartners.seochocoin.entity.*;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Data
public class MemberVO extends EntityBase implements Serializable {

    private Member member;
    private List<Activity> activityList;
    private int numOfActivity;

    private long no;
    private long coinPlusSum;
    private long coinMinusSum;

    private List<ActivitySchedule> scheduleList;

    private List<CoinLeger> coinLegerList;

    public MemberVO() { }

}
