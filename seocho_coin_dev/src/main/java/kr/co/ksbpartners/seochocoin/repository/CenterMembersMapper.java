package kr.co.ksbpartners.seochocoin.repository;

import kr.co.ksbpartners.seochocoin.entity.CenterMembers;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface CenterMembersMapper {

    CenterMembers selectCenterMembersById(String centerId, String memberId);

    List<CenterMembers> selectAllCenterMembers();

    int insertCenterMembers(CenterMembers centerMembers);

    int updateCenterMembers(CenterMembers centerMembers);

    int deleteCenterMembers(CenterMembers centerMembers);
}