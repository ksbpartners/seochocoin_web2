package kr.co.ksbpartners.seochocoin.repository;

import kr.co.ksbpartners.seochocoin.entity.*;
import kr.co.ksbpartners.seochocoin.vo.ActivityInfo;
import kr.co.ksbpartners.seochocoin.vo.MemberActivityVO;
import kr.co.ksbpartners.seochocoin.vo.NcomInfo;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface ApiNcomMapper {

    NcomInfo checkToken();

    Member getInfoByUser(String mobile);

    int addPaidTx(NcomInfo ncom);

    int cancelPaidTx(NcomInfo ncom);

    PaymentTx getPaidTxById(NcomInfo ncom);
}