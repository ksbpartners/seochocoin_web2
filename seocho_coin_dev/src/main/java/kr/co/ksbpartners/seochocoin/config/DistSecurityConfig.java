package kr.co.ksbpartners.seochocoin.config;

import kr.co.ksbpartners.seochocoin.service.DistUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.access.AccessDeniedHandler;

@Configuration
@EnableWebSecurity
@Order(5)
public class DistSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private DistUserDetailsService distUserDetailsService;

    @Autowired
    private AccessDeniedHandler distAccessDeniedHandler;

    //フォームの値と比較するDBから取得したパスワードは暗号化されているのでフォームの値も暗号化するために利用
    @Bean
//    @Qualifier("passwordEncoder")
    public BCryptPasswordEncoder distPasswordEncoder() {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        return bCryptPasswordEncoder;
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers(
                "/api/**",
                "/ncom/**",
                "/webjars/**",
                "/images/**",
                "/css/**",
                "/js/**"
        );
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http    .antMatcher("/dist/**")
                .authorizeRequests()
                .antMatchers("/dist/login").permitAll()
                .antMatchers("/dist/**").hasRole("DIST")
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/dist/login") //ログインページはコントローラを経由しないのでViewNameとの紐付けが必要
                .loginProcessingUrl("/dist/sign_in") //フォームのSubmitURL、このURLへリクエストが送られると認証処理が実行される
                .usernameParameter("username") //リクエストパラメータのname属性を明示
                .passwordParameter("password")
                .successForwardUrl("/dist/index")
                .failureUrl("/dist/login?error")
                .permitAll()
                .and()
                .logout()
                .logoutUrl("/dist/logout")
                .logoutSuccessUrl("/dist/login?logout")
                .permitAll();

        http.exceptionHandling().accessDeniedHandler(distAccessDeniedHandler);
    }

    @Autowired
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(distUserDetailsService).passwordEncoder(distPasswordEncoder());
//        auth
//                .inMemoryAuthentication()
//                .withUser("adm000002").password("{noop}pass").roles("DIST");
    }

}