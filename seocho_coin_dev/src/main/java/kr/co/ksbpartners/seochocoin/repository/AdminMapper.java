package kr.co.ksbpartners.seochocoin.repository;

import kr.co.ksbpartners.seochocoin.entity.Admin;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface AdminMapper {

    Admin getById(String id);

    Admin getMaxId();

    List<Admin> selectAll();

    int register(Admin member);

    int modify(Admin member);

    int modifyPwd(String pwd, String id, String updatedBy);

    int delete(Admin member);

    Admin getByDistId(String id);
}