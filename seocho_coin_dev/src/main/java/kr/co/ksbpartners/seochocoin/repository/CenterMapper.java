package kr.co.ksbpartners.seochocoin.repository;

import kr.co.ksbpartners.seochocoin.entity.Center;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface CenterMapper {

    Center getById(String id);

    Center getByName(String name);

    Center getByAdminId(String adminId);

    Center getMaxId();

    List<Center> selectAll(String keyword, String fromDate, String toDate, String type);

    List<Map<String, String>> groupByType(String keyword, String fromDate, String toDate, String type);

    int register(Center member);

    int modify(Center member);

    int delete(List<String> ids, String updatedBy);

    Map<String, Long> cntAllByCenterId(String id);

    List<Map<String, Long>> cntAllCenter();
}