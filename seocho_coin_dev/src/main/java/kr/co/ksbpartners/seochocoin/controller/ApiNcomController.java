package kr.co.ksbpartners.seochocoin.controller;

import kr.co.ksbpartners.seochocoin.constants.Constants;
import kr.co.ksbpartners.seochocoin.entity.*;
import kr.co.ksbpartners.seochocoin.repository.ApiActivityMapper;
import kr.co.ksbpartners.seochocoin.service.*;
import kr.co.ksbpartners.seochocoin.util.Utils;
import kr.co.ksbpartners.seochocoin.vo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.DecimalFormat;
import java.util.*;

@RestController
@RequestMapping("/ncom/api")
public class ApiNcomController {
    DecimalFormat df = new DecimalFormat("#,###");

    @Autowired
    ApiNcomService apiNcomService;

    @Autowired
    ApiMemberService apiMemberService;

    @Autowired
    ApiActivityService apiActivityService;

    @PostMapping("/getInfo")
    public Map<String, Object> getInfo(@RequestBody NcomInfo ncom) {
        Map<String, Object> result = new HashMap<>();

        NcomInfo _ncom = apiNcomService.checkToken();

        if (_ncom != null && _ncom.getToken().equals(ncom.getToken())) {
            Member _member = apiNcomService.getInfoByUser(ncom.getMobile());

            if (_member == null) {
                result.put("error", "Data dose not exists.");
            } else {
                result.put("name", _member.getName());
                result.put("mobile", _member.getMobile());
                result.put("balance", _member.getCoin());
            }
        } else {
            result.put("error", "Invalid token.");
        }

        return result;
    }

    @PostMapping("/storePaid")
    public Map<String, Object> storePaidInfo(@RequestBody NcomInfo ncom) {
        Map<String, Object> result = new HashMap<>();

        NcomInfo _ncom = apiNcomService.checkToken();

        if (_ncom != null && _ncom.getToken().equals(ncom.getToken())) {
            Member member = apiMemberService.loginByMobile(ncom.getMobile());

            if (member != null && member.getName().equals(ncom.getName())) {
                if (member.getCoin() < ncom.getCoin_amount()) {
                    result.put("result", Constants.INSUFFICIENT_COIN);
                } else {
                    for (NcomInfoTxContents txContents : ncom.getTx_contents()) {
                        CoinUsage coinUsage = new CoinUsage();
                        coinUsage.setCenterId(ncom.getCenter_id());
                        coinUsage.setUsageId(txContents.getUsage_id());
                        coinUsage.setUsageTitle(txContents.getUsage_title());
                        coinUsage.setEtc(txContents.getEtc());
                        coinUsage.setMemberId(member.getId());
                        coinUsage.setTxId(ncom.getTx_id());
                        coinUsage.setType("사용");
                        coinUsage.setPaid(txContents.getCoin_amount() * Constants.BASE_COIN_PRICE);
                        coinUsage.setCoin(txContents.getCoin_amount());
                        coinUsage.setUsageDate(ncom.getTx_date());
                        coinUsage.setCreatedBy("ncom");
                        coinUsage.setUpdatedBy("ncom");

                        // use coin
                        apiActivityService.addCoinUsage(coinUsage);
                    }

                    member.setCoin(ncom.getCoin_amount());
                    member.setUpdatedBy("ncom");
                    apiActivityService.minusCoinOfMember(member);

                    ncom.setCoin_amount(ncom.getCoin_amount());
                    if (apiNcomService.storePaidTx(ncom)) {
                        result.put("result", Constants.RESULT_OK);
                    } else {
                        result.put("result", Constants.RESULT_NG);
                    }
                }
            } else {
                result.put("result", Constants.NOT_EXISTS);
            }
        } else {
            result.put("error", "Invalid token.");
        }

        return result;
    }

    @PostMapping("/cancelPaid")
    public Map<String, Object> cancelPaidInfo(@RequestBody NcomInfo ncom) {
        Map<String, Object> result = new HashMap<>();

        NcomInfo _ncom = apiNcomService.checkToken();

        if (_ncom != null && _ncom.getToken().equals(ncom.getToken())) {
            Member member = apiMemberService.loginByMobile(ncom.getMobile());

            PaymentTx _paymentTx = apiNcomService.getPaidTxById(ncom);

            if (_paymentTx != null) {
                if (apiNcomService.cancelPaidTx(ncom)) {

                    CoinUsage coinUsage = new CoinUsage();
                    coinUsage.setCenterId(ncom.getCenter_id());
                    coinUsage.setMemberId(member.getId());
                    coinUsage.setTxId(ncom.getTx_id());
                    coinUsage.setType("사용취소");
                    coinUsage.setUpdatedBy("ncom");

                    // transaction cancel -> payback coin
                    apiActivityService.delCoinUsage(coinUsage);

                    member.setCoin(_paymentTx.getCoinAmount());
                    member.setUpdatedBy("ncom");
                    apiActivityService.updateCoinOfMember(member);

                    result.put("result", Constants.RESULT_OK);
                } else {
                    result.put("result", Constants.RESULT_NG);
                }
            } else {
                result.put("result", Constants.NOT_EXISTS);
            }
        } else {
            result.put("error", "Invalid token.");
        }

        return result;
    }
}
