package kr.co.ksbpartners.seochocoin.vo;

import lombok.Data;

import java.util.List;

@Data
public class MyActivityInfo extends BaseActivityInfo {
    private String activityId;
    private String centerName;
    private String activityName;
    private String activityType;
    private String activityCategory;
    private String activityArea;
    private String activityLimit;
    private String accumCoin;
    private String addCoinVal;
    private String coinPrice;
    private String activityStatus;

    private String startDate;
    private String endDate;
    private String address;

    private long cnt;

    private String createdAt;
    private String createdAtStr;

    private String mon;
    private String monFrom;
    private String monTo;

    private String tue;
    private String tueFrom;
    private String tueTo;

    private String wed;
    private String wedFrom;
    private String wedTo;

    private String thu;
    private String thuFrom;
    private String thuTo;

    private String fri;
    private String friFrom;
    private String friTo;

    private List<String> timeTables;

    public MyActivityInfo(String centerName,
                          String activityName,
                          String activityType,
                          String activityCategory,
                          String activityArea,
                          String activityLimit,
                          String accumCoin,
                          String createdAt) {
        this.centerName = centerName;
        this.activityName = activityName;
        this.activityType = activityType;
        this.activityCategory = activityCategory;
        this.activityArea = activityArea;
        this.activityLimit = activityLimit;
        this.accumCoin = accumCoin;
        this.createdAt = createdAt;
    }
}
