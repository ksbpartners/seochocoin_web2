package kr.co.ksbpartners.seochocoin.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/cent")
public class CenterEventsController {
    @GetMapping("/events-add")
    public String eventsAdd() {
        return "cent/events-add";
    }

    @GetMapping("/events-list")
    public String eventsList() {
        return "cent/events-list";
    }

    @GetMapping("/faq-add")
    public String faqAdd() {
        return "cent/faq-add";
    }

    @GetMapping("/faq-list")
    public String faqList() {
        return "cent/faq-list";
    }

    @GetMapping("/notice-add")
    public String noticeAdd() {
        return "cent/notice-add";
    }

    @GetMapping("/notice-list")
    public String noticeList() {
        return "cent/notice-list";
    }
}
