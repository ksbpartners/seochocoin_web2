package kr.co.ksbpartners.seochocoin.vo;

import lombok.Data;

@Data
public class MyCoinInfo {
    private String id;
    private String activityName;
    private String activityType;
    private String activityCategory;
    private String activityArea;
    private String coinType;
    private String paid;
    private String coin;
    private String createdAt;
    private String createdAtStr;

    public MyCoinInfo(String activityName,
                      String activityType,
                      String activityCategory,
                      String activityArea,
                      String coinType,
                      String paid,
                      String coin,
                      String createdAt) {
        this.activityName = activityName;
        this.activityType = activityType;
        this.activityCategory = activityCategory;
        this.activityArea = activityArea;
        this.coinType = coinType;
        this.paid = paid;
        this.coin = coin;
        this.createdAt = createdAt;
    }
}
