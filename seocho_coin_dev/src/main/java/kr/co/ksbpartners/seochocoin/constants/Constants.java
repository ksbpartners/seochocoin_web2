package kr.co.ksbpartners.seochocoin.constants;

public interface Constants {

    String PREFIX_ADMIN_ID = "adm";
    String PREFIX_MEMBER_ID = "mem";
    String PREFIX_CENTER_ID = "cen";
    String PREFIX_ACTIVITY_ID = "act";
    String PREFIX_USAGE_ID = "usa";

    String YES = "Y";
    String NO = "N";

    /**
     * Coin add or cancel
     */
    int COIN_ADD = 1;
    int COIN_CANCEL = -1;

    /**
     * Value
     */
    int RESULT_OK = 1;
    int RESULT_NG = 0;
    int RESULT_OK_4_REGIST = 11;

    int NOT_EXISTS = 91;
    int DUPLICATE_KEY = 92;
    int EXPIRED_TOKEN = 93;
    int WRONG_PWD = 94;
    int NOT_REGIST = 95;
    int ALREADY_ATTENDED = 96;
    int WRONG_PWD_TYPE = 97;
    int INSUFFICIENT_PARAM = 98;
    int LOGIN_FAIL = 99;
    int INSUFFICIENT_COIN = 101;

    int STATUS_OK = 0;
    int STATUS_ALREADY_REGIST = 1;

    /**
     * Key
     */
    String RESULT = "result";
    String TOKEN = "token";
    String UID = "uid";
    String ERROR_MSG = "error_msg";

    String STATUS = "status";
    String LIST = "list";
    String COUNT = "count";
    String COIN_LIST = "coin_list";
    String AC_LIST = "ac_list";
    String COIN_COUNT = "coin_count";
    String AC_COUNT = "ac_count";
    String AC_ID = "aid";
    String AC_CATE_LIST = "category_list";

    String ACTIVITY = "activity";
    String USAGE = "usage";
    String COIN = "coin";

    int BASE_COIN_PRICE = 200;
    String SALT_KEY = "seochocoin";

    /**
     * Constants
     */
    String CATEGORY_ACTIVITY = "활동";
    String CATEGORY_USAGE = "사용처";

    String COIN_PLUS = "적립";
    String COIN_MINUS = "적립취소";
    String COIN_USED = "사용";

    String ACTIVITY_STATUS_JOIN = "신청중";
    String ACTIVITY_STATUS_ACTIVE = "활동중";
    String ACTIVITY_STATUS_CANCEL = "취소";
    String ACTIVITY_JOIN_OK = "OK";
    String ACTIVITY_JOIN_NG = "NG";
}
