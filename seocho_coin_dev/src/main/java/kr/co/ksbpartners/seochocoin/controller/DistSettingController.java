package kr.co.ksbpartners.seochocoin.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/dist")
public class DistSettingController {
    @GetMapping("/setting-data")
    public String settingData() {
        return "dist/setting-data";
    }

    @GetMapping("/setting-log")
    public String settingLog() {
        return "dist/setting-log";
    }
}
