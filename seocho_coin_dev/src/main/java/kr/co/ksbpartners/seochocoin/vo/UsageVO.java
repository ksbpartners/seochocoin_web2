package kr.co.ksbpartners.seochocoin.vo;

import kr.co.ksbpartners.seochocoin.entity.CoinLeger;
import kr.co.ksbpartners.seochocoin.entity.EntityBase;
import kr.co.ksbpartners.seochocoin.entity.Usage;
import lombok.Data;

import java.util.List;

@Data
public class UsageVO extends EntityBase {

    Usage usage;
    private List<CoinLeger> coinLegerList;

    long numOfMembers;
    long coinUsed;
    long coinAdded;

    private long no;
    private long coinPlusSum;
    private long coinMinusSum;
}
