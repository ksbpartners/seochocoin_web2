package kr.co.ksbpartners.seochocoin.util.thymeleaf;

import java.util.ArrayList;
import java.util.Arrays;

public class CustUtil {
    public String[] trimArray(final ArrayList<String> text) {

        return text.stream()
                .filter(s -> (s != null && s.length() > 0))
                .toArray(String[]::new);
    }
}
