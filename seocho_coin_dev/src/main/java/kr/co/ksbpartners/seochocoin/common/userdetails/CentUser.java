package kr.co.ksbpartners.seochocoin.common.userdetails;

import kr.co.ksbpartners.seochocoin.entity.Center;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

public class CentUser extends User {

    public CentUser(Center center, String username, String password, boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
        this.center = center;
    }

    private Center center;
    public Center getCenter() {
        return this.center;
    }

}
