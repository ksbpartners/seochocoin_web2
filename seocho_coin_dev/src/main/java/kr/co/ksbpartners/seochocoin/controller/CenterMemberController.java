package kr.co.ksbpartners.seochocoin.controller;

import kr.co.ksbpartners.seochocoin.common.userdetails.CentUser;
import kr.co.ksbpartners.seochocoin.constants.Constants;
import kr.co.ksbpartners.seochocoin.entity.ActivitySchedule;
import kr.co.ksbpartners.seochocoin.entity.Center;
import kr.co.ksbpartners.seochocoin.entity.Member;
import kr.co.ksbpartners.seochocoin.service.AdminService;
import kr.co.ksbpartners.seochocoin.service.ApiMemberService;
import kr.co.ksbpartners.seochocoin.service.CenterService;
import kr.co.ksbpartners.seochocoin.service.MemberService;
import kr.co.ksbpartners.seochocoin.util.Utils;
import kr.co.ksbpartners.seochocoin.util.crypto.CryptoUtil;
import kr.co.ksbpartners.seochocoin.vo.MemberVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.*;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

@Controller
@RequestMapping("/cent")
public class CenterMemberController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private MemberService memberService;

    @Autowired
    ApiMemberService apiMemberService;

    @Autowired
    private CenterService centerService;

    @GetMapping("/member-list")
    public String memberList(@RequestParam(required = false) String keyword, Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String adminId = auth.getName();

        List<MemberVO> members = null;
        Center cent = ((CentUser)auth.getPrincipal()).getCenter();
        if (null != cent) {
            members = memberService.getAll(cent.getId(), keyword);
        }
        model.addAttribute("members", members);
        model.addAttribute("keyword", keyword);
        return "cent/member-list";
    }

    @GetMapping("/member-search-modal")
    public String memberSearch(@RequestParam(required = false) String keyword, Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String adminId = auth.getName();

        List<MemberVO> members = null;
        Center cent = ((CentUser)auth.getPrincipal()).getCenter();
        if (null != cent) {
            members = memberService.getAll(cent.getId(), keyword);
        }
        model.addAttribute("members", members);
        model.addAttribute("keyword", keyword);
        return "cent/member-search-modal";
    }

    @GetMapping("/member-detail")
    public String memberDetail(@RequestParam String id, Model model) {
        MemberVO ret = memberService.getDetail(id);

        List<HashMap<String, String>> scheduleMapList = new ArrayList<>();
        for (ActivitySchedule schedule : ret.getScheduleList()) {
            HashMap scheduleMap = new HashMap<String, String>();
            scheduleMap.put("title", schedule.getActivityName());
            scheduleMap.put("start", schedule.getActivityDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))
                    + "T" + schedule.getStart());
            scheduleMap.put("end", schedule.getActivityDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))
                    + "T" + schedule.getEnd());
            if (schedule.getAttendedAt() != null) {
                scheduleMap.put("color", "green");
                scheduleMap.put("textColor", "white");
            }
            scheduleMapList.add(scheduleMap);
        }

        model.addAttribute("memberDetail", ret);
        model.addAttribute("calendarSchedule", scheduleMapList);
        return "cent/member-detail";
    }

    @GetMapping("/member-schedule-change")
    @ResponseBody
    public List<HashMap<String, String>> memberScheduleChange(@RequestParam("memberId") String memberId,
                                                              @RequestParam("activityId") String activityId) {
        List<ActivitySchedule> ret = memberService.getMemberScheduleList(memberId, activityId, null, null);

        List<HashMap<String, String>> scheduleMapList = new ArrayList<>();
        for (ActivitySchedule schedule : ret) {
            HashMap scheduleMap = new HashMap<String, String>();
            scheduleMap.put("title", schedule.getActivityName());
            scheduleMap.put("start", schedule.getActivityDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))
                    + "T" + schedule.getStart());
            scheduleMap.put("end", schedule.getActivityDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))
                    + "T" + schedule.getEnd());
            if (schedule.getAttendedAt() != null) {
                scheduleMap.put("color", "green");
                scheduleMap.put("textColor", "white");
            }
            scheduleMapList.add(scheduleMap);
        }

        return scheduleMapList;
    }

    @GetMapping("/member-register")
    public String memberRegister(@RequestParam(required = false) String id, Model model) {
        Member ret = null;
        if (StringUtils.isEmpty(id)) {
            ret = new Member();
        } else {
            ret = memberService.getById(id);
        }

        List<Center> centers = centerService.getAll(null, null, null, null);

        model.addAttribute("member", ret);
        model.addAttribute("centers", centers);
        return "cent/member-register";
    }

    private String goToMemberRegisterPage(Model model) {
        List<Center> centers = centerService.getAll(null, null, null, null);
        model.addAttribute("centers", centers);
        return "cent/member-register";
    }

    @PostMapping("/member-register")
    public String registerMember(@ModelAttribute("member") @Validated Member member,
                                  BindingResult bindingResult, Model model) throws Exception {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if (bindingResult.hasErrors()) {
            return goToMemberRegisterPage(model);
        }

        member.setUpdatedBy(auth.getName());
        member.setMobile(Utils.CONVERT_MOBILE1(member.getMobile()));
        if (StringUtils.isEmpty(member.getId())) {
            List<Member> _member = apiMemberService.getTokenByMobile(member.getMobile());
            if (_member != null && !_member.isEmpty() && !_member.get(0).getId().isEmpty()) {
                FieldError ssoError = new FieldError("member", "mobile", "이미 등록된 전화번호입니다. 전화번호를 확인해 주세요.");
                bindingResult.addError(ssoError);
                return goToMemberRegisterPage(model);
            } else {

                member.setCreatedBy(auth.getName());
                if (StringUtils.isEmpty(member.getPwd())) {
                    member.setPwd(CryptoUtil.encryptAES256("pass", Constants.SALT_KEY));
                } else {
                    member.setPwd(CryptoUtil.encryptAES256(member.getPwd(), Constants.SALT_KEY));
                }
                member.setId(memberService.add(member));
            }
        } else {
            List<Member> _memberList = apiMemberService.checkMobile4Modify(member.getId(), member.getMobile());
            if (_memberList != null && !_memberList.isEmpty()) {
//                FieldError error = new FieldError("member", "mobile", "이미 등록된 전화번호입니다. 전화번호를 확인해 주세요.");
//                bindingResult.addError(error);
                bindingResult.rejectValue("mobile", "", "이미 등록된 전화번호입니다. 전화번호를 확인해 주세요.");
                return goToMemberRegisterPage(model);
            } else {
                if (!StringUtils.isEmpty(member.getPwd())) {
                    member.setPwd(CryptoUtil.encryptAES256(member.getPwd(), Constants.SALT_KEY));
                }
                memberService.modify(member);
            }
        }

        return memberDetail(member.getId(), model);
    }


    @GetMapping("/member-update")
    public String updateMember() {
        Member member = new Member();
        memberService.modify(member);

        return "cent/member-register";
    }


    @PostMapping("/member-delete")
    public String deleteMember(@RequestParam List<String> delIds,
                               @RequestParam("keyword") String keyword, Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        // ファイルが空の場合は異常終了
        if (delIds.isEmpty()) {
            // TODO error
        }

        memberService.remove(delIds, auth.getName());

        return memberList(keyword, model);
    }



    @GetMapping("/member-download")
    public ResponseEntity<InputStreamResource> memberExcelDownload(@RequestParam("keyword") String keyword)
            throws IOException {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        List<MemberVO> memberList = null;
        Center cent = ((CentUser)auth.getPrincipal()).getCenter();
        if (null != cent) {
            memberList = memberService.getAll(cent.getId(), keyword);
        }

        ByteArrayInputStream in = memberService.generateMemberListFile(memberList);
        // return IOUtils.toByteArray(in);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "attachment; filename=memeberList.xlsx");

        return ResponseEntity
                .ok()
                .headers(headers)
                .body(new InputStreamResource(in));
    }


    @PostMapping("/member-upload")
    public String memberExcelUpload(@RequestParam("elmFile") MultipartFile multipartFile,
                               RedirectAttributes redirectAttributes) {
        // ファイルが空の場合は異常終了
        if (multipartFile.isEmpty()) {
            // TODO error
        }

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Center cent = ((CentUser)auth.getPrincipal()).getCenter();

        logger.info("upload file {}", multipartFile.getOriginalFilename());
        int uploadCnt = 0;
        String msg = null;
        try {
            uploadCnt = memberService.uploadMemberListFile(multipartFile, cent.getId());
            msg = "업로드 성공: " + multipartFile.getOriginalFilename() + " 등록 수: " + uploadCnt + "건";
        } catch (Exception e) {
            msg = e.getMessage();
        }
        logger.info(msg);

        redirectAttributes.addFlashAttribute("message", msg);

        return "redirect:/cent/member-list";
    }

}
