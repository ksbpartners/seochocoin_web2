package kr.co.ksbpartners.seochocoin.service;

import kr.co.ksbpartners.seochocoin.common.userdetails.CentUser;
import kr.co.ksbpartners.seochocoin.entity.Center;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service("centerUserDetailsService")
public class CenterUserDetailsService implements UserDetailsService {

    @Autowired
    private CenterService centerService;

//    @Autowired
//    private PasswordEncoder passwordEncoder;

//    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
//        this.passwordEncoder = passwordEncoder;
//    }

    public void setCenterService(CenterService centerService) {
        this.centerService = centerService;
    }

    @Transactional
    @Override
    public UserDetails loadUserByUsername(String username)
            throws UsernameNotFoundException {

        Center center = centerService.getByAdminId(username);
        List<GrantedAuthority> authorities = new ArrayList<>();
        if (center == null) {
            throw new UsernameNotFoundException("User " + username + " was not found in the database");
        }
        authorities.add(new SimpleGrantedAuthority("ROLE_CENT"));

        return buildUserForAuthentication(center, authorities);
    }

    private User buildUserForAuthentication(Center center, List<GrantedAuthority> authorities) {
        if (center != null) {
//            return new User(center.getAdminId(), center.getPwd(), true, true, true, true, authorities);
            return new CentUser(center, center.getAdminId(), center.getPwd(), true, true, true, true, authorities);
        } else {
            return null;
        }
    }

}
