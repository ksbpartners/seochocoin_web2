package kr.co.ksbpartners.seochocoin.vo;

import kr.co.ksbpartners.seochocoin.entity.Activity;
import kr.co.ksbpartners.seochocoin.entity.ActivitySchedule;
import lombok.Data;

import java.util.List;

@Data
public class ActivityScheduleVO {
    Activity activity;
    List<ActivitySchedule> activitySchedules;
    List<String> monMemIds;
    List<String> tueMemIds;
    List<String> wedMemIds;
    List<String> thuMemIds;
    List<String> friMemIds;

    List<String> monMemNms;
    List<String> tueMemNms;
    List<String> wedMemNms;
    List<String> thuMemNms;
    List<String> friMemNms;

}
