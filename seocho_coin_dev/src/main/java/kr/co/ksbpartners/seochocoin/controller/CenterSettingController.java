package kr.co.ksbpartners.seochocoin.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/cent")
public class CenterSettingController {
    @GetMapping("/setting-data")
    public String settingData() {
        return "cent/setting-data";
    }

    @GetMapping("/setting-log")
    public String settingLog() {
        return "cent/setting-log";
    }
}
