package kr.co.ksbpartners.seochocoin.service;

import ch.qos.logback.core.rolling.helper.IntegerTokenConverter;
import kr.co.ksbpartners.seochocoin.constants.Constants;
import kr.co.ksbpartners.seochocoin.entity.Admin;
import kr.co.ksbpartners.seochocoin.entity.Center;
import kr.co.ksbpartners.seochocoin.repository.*;
import kr.co.ksbpartners.seochocoin.util.excel.ExcelPOIUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.util.StringUtils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class CenterService extends AbstractService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private CenterMapper centerMapper;

    @Autowired
    private AdminMapper adminMapper;

    @Autowired
    private MemberMapper memberMapper;

    @Autowired
    private ActivityMapper activityMapper;

    @Autowired
    private ActivityScheduleMapper activityScheduleMapper;

    @Autowired
    @Qualifier("distPasswordEncoder")
    private BCryptPasswordEncoder bCryptPasswordEncoder;


    public Center getById(String id) {
        return centerMapper.getById(id);
    }

    public Center getByName(String name) {
        return centerMapper.getByName(name);
    }

    public Center getByAdminId(String adminId) {
        return centerMapper.getByAdminId(adminId);
    }

    public List<Center> getAll(String keyword, String fromDate, String toDate, String type) {
        return centerMapper.selectAll(keyword, fromDate, toDate, type);
    }

    public Map<String, Object> groupByType(String keyword, String fromDate, String toDate, String type) {
        List<Map<String, String>> inList = centerMapper.groupByType(keyword, fromDate, toDate, type);
        Map<String, Object> res = new HashMap<>();
        for (Map<String, String> in : inList) {
            res.put(in.get("type"), in.get("cnt"));
        }
        return res;
    }

    @Transactional
    public String add(Center center) throws Exception {
        // timestamp
        LocalDateTime timeStamp = LocalDateTime.now();

        // new admin
        // get id
        Admin adminMax = adminMapper.getMaxId();
        String newAdminId = getNewId(Constants.PREFIX_ADMIN_ID, null == adminMax ? "" : adminMax.getId());

        // get id
        Center entity = centerMapper.getMaxId();
        String newId = getNewId(Constants.PREFIX_CENTER_ID, null == entity ? "" : entity.getId());

        Admin admin = new Admin();
        admin.setId(newAdminId);
        admin.setName(center.getName() + " 관리자");
        String password = bCryptPasswordEncoder.encode(center.getPwd());
        admin.setPwd(password);
        admin.setCenterId(newId);
        admin.setType("cent");
        admin.setCreatedBy(center.getCreatedBy());
        admin.setUpdatedBy(center.getUpdatedBy());

        int ret = adminMapper.register(admin);
        if (ret != 1) {
            throw new Exception("Fail to generate new data.");
        }

        center.setAdminId(admin.getId());

        Center newCenter = new Center();
        BeanUtils.copyProperties(center, newCenter);
        newCenter.setId(newId);
        newCenter.setCreatedAt(timeStamp);
        newCenter.setUpdatedAt(timeStamp);

        ret = centerMapper.register(newCenter);
        if (ret != 1) {
            throw new Exception("Fail to generate new data.");
        }

        return newCenter.getId();
    }

    @Transactional
    public int modify(Center center) {
        // timestamp
        LocalDateTime timeStamp = LocalDateTime.now();
        center.setUpdatedAt(timeStamp);
        int ret = centerMapper.modify(center);
        if (!StringUtils.isEmpty(center.getPwd())) {
            String password = bCryptPasswordEncoder.encode(center.getPwd());
            ret = adminMapper.modifyPwd(password, center.getAdminId(), center.getUpdatedBy());
        }
        return ret;
    }

    @Transactional
    public int remove(List<String> ids, String updatedBy) {
        // delete activity
        int res = centerMapper.delete(ids, updatedBy);

        // TODO member update

        // delete activity
        res +=activityMapper.deleteByCenterId(ids, updatedBy);

        // delete member activity
        res += activityMapper.deleteMemberActivitiesByCenterId(ids, updatedBy);

        // delete activity schedule
        res += activityScheduleMapper.deleteActivitiesSchedulesByCenterId(ids, updatedBy);


        return res;
    }


    public ByteArrayInputStream generateCenterListFile(List<Center> centers) throws IOException {
        logger.info("generate Excel file.");

        Map<String, Admin> adminMap = new HashMap<>();
        if (null != centers) {
            for (Center center : centers) {
                Admin admin = null;
                if (null != center.getAdminId() && null == adminMap.get(center.getAdminId())) {
                    admin = adminMapper.getById(center.getAdminId());
                    adminMap.put(center.getAdminId(), admin);
                }
            }

        }


        return ExcelPOIUtil.centersToExcel(centers, adminMap);
    }


    public Map<String, Long> cntAllByCenterId(String id) {
        return centerMapper.cntAllByCenterId(id);
    }

    public List<Map<String, Long>> cntAllCenter() {
        return centerMapper.cntAllCenter();
    }
}