package kr.co.ksbpartners.seochocoin.controller;

import kr.co.ksbpartners.seochocoin.common.userdetails.CentUser;
import kr.co.ksbpartners.seochocoin.entity.Activity;
import kr.co.ksbpartners.seochocoin.entity.Admin;
import kr.co.ksbpartners.seochocoin.entity.Center;
import kr.co.ksbpartners.seochocoin.service.ActivityService;
import kr.co.ksbpartners.seochocoin.service.AdminService;
import kr.co.ksbpartners.seochocoin.service.CenterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@Controller
@RequestMapping("/cent")
public class CenterMainController {

    @Autowired
    AdminService adminService;

    @Autowired
    ActivityService activityService;

    @Autowired
    CenterService centerService;

    @RequestMapping("/index")
    public String init(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Center cent = ((CentUser)auth.getPrincipal()).getCenter();
        Map<String, Long> allCnt = null;
        if (null != cent) {
            allCnt = centerService.cntAllByCenterId(cent.getId());
        }

        model.addAttribute("allCnt", allCnt);
        return "cent/index";
    }

    @GetMapping("/userinfo")
    public String userinfo(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Admin admin = adminService.getById(auth.getName());

        model.addAttribute("admin", admin);
        return "cent/userinfo";
    }

    @PostMapping("/userinfo-update")
    public String userinfoUpdate(@ModelAttribute("admin") @Validated Admin admin,
                                 BindingResult bindingResult, Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (bindingResult.hasErrors()) {
            return "cent/userinfo";
        }
        admin.setId(auth.getName());
        admin.setUpdatedBy(auth.getName());
        if (adminService.modify(admin) != 1) {

        }

        model.addAttribute("admin", admin);
        return "cent/userinfo";
    }
    @GetMapping("/login")
    public String login() {
        return "cent/login";
    }

    @GetMapping("/modal")
    public String modal(@RequestParam(required = false) String id,
                        @RequestParam(required = false) String name,
                        Model model) {
        Activity ret = new Activity();
        if (!StringUtils.isEmpty(id)) {
            ret.setId(id);
            if (id.contains("act")) {
                ret = activityService.getActivityById(id);
            }
        }
        if (!StringUtils.isEmpty(name)) {
            ret.setName(name);
        }

        model.addAttribute("activity", ret);
        return "cent/modal";
    }
}
