package kr.co.ksbpartners.seochocoin.vo;

import kr.co.ksbpartners.seochocoin.entity.EntityBase;
import lombok.Data;
import org.apache.ibatis.type.Alias;

import java.io.Serializable;

@Data
@Alias("activity_category")
public class ActivityCategoryVO implements Serializable {
    private static final long serialVersionUID = 1L;

    private String id;
    private String type;
    private String actName;

    public ActivityCategoryVO() { }

    public ActivityCategoryVO(String _id, String _actName, String _type) {
        this.id = _id;
        this.actName = _actName;
        this.type = _type;
    }
}
