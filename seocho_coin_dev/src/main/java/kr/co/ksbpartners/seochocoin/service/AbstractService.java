package kr.co.ksbpartners.seochocoin.service;


import com.google.common.io.Files;
import org.springframework.util.StringUtils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AbstractService {

    /**
     * create id using prefix (prefix + 6 digit number)
     * @param prefix
     * @param id
     * @return newId
     */
    public String getNewId(String prefix, String id) {
        String ret = prefix;
        if (!StringUtils.isEmpty(id)) {
            int intId = Integer.parseInt(id.replace(prefix, "")) + 1;
            ret += String.format("%06d", intId);
        } else {
            ret += String.format("%06d", 1);
        }
        return ret;
    }


    public String getFileName (String originalFilename) {
        Date now = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");

        String extension = Files.getFileExtension(originalFilename);
        return originalFilename.substring(0, originalFilename.lastIndexOf(extension) -1)
                + sdf.format(now) + "." + extension;
    }


    public File mkdirs(StringBuffer filePath) {

        File uploadDir = new File(filePath.toString());
        uploadDir.mkdirs();

        return uploadDir;
    }


}