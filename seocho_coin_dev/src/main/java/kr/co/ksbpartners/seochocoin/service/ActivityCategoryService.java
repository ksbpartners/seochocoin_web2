package kr.co.ksbpartners.seochocoin.service;

import kr.co.ksbpartners.seochocoin.constants.Constants;
import kr.co.ksbpartners.seochocoin.entity.*;
import kr.co.ksbpartners.seochocoin.repository.*;
import kr.co.ksbpartners.seochocoin.util.excel.ExcelPOIUtil;
import kr.co.ksbpartners.seochocoin.vo.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ActivityCategoryService extends AbstractService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ActivityCategoryMapper activityScheduleMapper;

    public List<ActivityCategoryVO> getAllByType(String type) {
        List<ActivityCategory> list = activityScheduleMapper.selectAllByType(type);
        List<ActivityCategoryVO> ret = new ArrayList<>();

        for (ActivityCategory ac : list) {
            ret.add(new ActivityCategoryVO(ac.getId(), ac.getActName(), ac.getType()));
        }

        return ret;
    }
}