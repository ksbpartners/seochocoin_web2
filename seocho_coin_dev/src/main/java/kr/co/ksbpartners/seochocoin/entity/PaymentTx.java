package kr.co.ksbpartners.seochocoin.entity;

import lombok.Data;
import org.apache.ibatis.type.Alias;

import java.io.Serializable;

@Data
@Alias("payment_tx")
public class PaymentTx extends EntityBase implements Serializable {
    private static final long serialVersionUID = 1L;

    private String idx;
    private String name;
    private String mobild;
    private String centerId;
    private long coinAmount;
    private long totalAmount;

    private String txDate;
    private String txId;

    private String status;

    public PaymentTx() {
    }

}