package kr.co.ksbpartners.seochocoin.vo;

import kr.co.ksbpartners.seochocoin.entity.EntityBase;
import lombok.Data;
import org.apache.ibatis.type.Alias;

import java.io.Serializable;
import java.util.List;

@Data
public class ActivityInfo extends BaseActivityInfo implements Serializable {
    private static final long serialVersionUID = 1L;

    private String id;
    private String centerId;
    private String centerName;
    private String activityName;
    private String activityType;
    private String activityCategory;

    private String status;

    private String activityArea;
    private String activityLimit;

    private long addCoinVal;
    private String coinPrice;
    private String startDate;
    private String endDate;
    private String address;

    private long cnt;

    private String createdAt;
    private String createdAtStr;

    private List<String> centerIds;
    private List<String> keywords;

    private String mon;
    private String monFrom;
    private String monTo;

    private String tue;
    private String tueFrom;
    private String tueTo;

    private String wed;
    private String wedFrom;
    private String wedTo;

    private String thu;
    private String thuFrom;
    private String thuTo;

    private String fri;
    private String friFrom;
    private String friTo;

    private List<String> timeTables;

    public ActivityInfo() {
    }

    public ActivityInfo(List<String> centerIds) {
        this.centerIds = centerIds;
    }

    public ActivityInfo(List<String> centerIds, List<String> keywords) {
        this.centerIds = centerIds;
        this.keywords = keywords;
    }

    public ActivityInfo(String id, String centerId) {
        this.id = id;
        this.centerId = centerId;
    }
}