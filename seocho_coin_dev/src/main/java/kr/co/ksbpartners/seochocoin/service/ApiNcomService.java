package kr.co.ksbpartners.seochocoin.service;

import kr.co.ksbpartners.seochocoin.entity.*;
import kr.co.ksbpartners.seochocoin.exception.DuplicatedActivityException;
import kr.co.ksbpartners.seochocoin.repository.ActivityScheduleMapper;
import kr.co.ksbpartners.seochocoin.repository.ApiActivityMapper;
import kr.co.ksbpartners.seochocoin.repository.ApiMemberMapper;
import kr.co.ksbpartners.seochocoin.repository.ApiNcomMapper;
import kr.co.ksbpartners.seochocoin.vo.ActivityInfo;
import kr.co.ksbpartners.seochocoin.vo.MemberActivityVO;
import kr.co.ksbpartners.seochocoin.vo.NcomInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class ApiNcomService extends AbstractService {

    @Autowired
    private ApiNcomMapper mapper;

    public NcomInfo checkToken() {
        return mapper.checkToken();
    }

    public Member getInfoByUser(String mobile) {
        return mapper.getInfoByUser(mobile);
    }

    @Transactional
    public boolean storePaidTx(NcomInfo ncom) {
        return mapper.addPaidTx(ncom) > 0 ? true : false;
    }

    public boolean cancelPaidTx(NcomInfo ncom) {
        return mapper.cancelPaidTx(ncom) > 0 ? true : false;
    }

    public PaymentTx getPaidTxById(NcomInfo ncom) {
        return mapper.getPaidTxById(ncom);
    }
}