package kr.co.ksbpartners.seochocoin.util;

import java.sql.*;
import java.util.*;

public class RemoveDashApp {
    public static void main(String[] args) {
        RemoveDashApp app = new RemoveDashApp();
        app.start();
    }

    public void start() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }

        int totalRows = 0;
        try (Connection conn = DriverManager.getConnection("jdbc:mysql://182.162.27.8:3306/secho_db_dev?serverTimezone=JST",
                "seocho_coin", "Astyle12!")) {
            PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM members WHERE del_flg='N' AND mobile like '%-%'");
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                String id = rs.getString("id");
                String orgMobile = rs.getString("mobile");
                StringTokenizer st = new StringTokenizer(orgMobile, "-");
                String newMobile = "";
                try {
                    while (st.hasMoreTokens()) {
                        newMobile += st.nextToken();
                    }
                } catch (NoSuchElementException ex) {
                    System.err.println(ex.getMessage());
                    System.out.println("====>" + orgMobile);
                }

                totalRows++;

                String updateSql = "UPDATE members SET mobile=? WHERE id=?";
                PreparedStatement upPstmt = conn.prepareStatement(updateSql);
                upPstmt.setString(1, newMobile);
                upPstmt.setString(2, id);
                int result = upPstmt.executeUpdate();

                if (result > 0) {
                    System.out.println(String.format("%s) %s, %s -> %s updated", totalRows, id, orgMobile, newMobile));
                }
            }
            System.out.println("----------");
            System.out.println("Total=" + totalRows);
        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
