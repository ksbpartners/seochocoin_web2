package kr.co.ksbpartners.seochocoin.entity;

import lombok.Data;
import org.apache.ibatis.type.Alias;

import java.io.Serializable;
import java.util.Date;

@Data
@Alias("activity")
public class Activity extends EntityBase implements Serializable {
    private static final long serialVersionUID = 1L;

    private String id;
    private String centerId;

    private String name;
    private String type;
    private String category;
    private String inCharge;
    private String contact;

    private String status;

    private String area;
    private String map;

    private String detail;
    private String startDate;
    private String endDate;

    private String hasPeriod;
    private String mon;
    private String monFrom;
    private String monTo;
    private String tue;
    private String tueFrom;
    private String tueTo;
    private String wed;
    private String wedFrom;
    private String wedTo;
    private String thu;
    private String thuFrom;
    private String thuTo;
    private String fri;
    private String friFrom;
    private String friTo;

    private int limt;

    private String useCoin;
    private long useCoinVal;

    private String addCoin;
    private long addCoinVal;
    private String addCoinAfter;

    private String attendCheck;
    private long attendRate;

    private String usePriceInfo;
    private long price;

    private String autoAddMarket;
    private String qrCode;
    private String openActivity;

    private String center;
    private String centerNm;

    private String is_modified;

    public Activity() {
    }

    public Activity(String id, String centerId) {
        this.id = id;
        this.centerId = centerId;
    }

}