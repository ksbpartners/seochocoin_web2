package kr.co.ksbpartners.seochocoin.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class BaseActivityInfo implements Serializable {
    private static final long serialVersionUID = 1L;

    private String mon;
    private String monFrom;
    private String monTo;

    private String tue;
    private String tueFrom;
    private String tueTo;

    private String wed;
    private String wedFrom;
    private String wedTo;

    private String thu;
    private String thuFrom;
    private String thuTo;

    private String fri;
    private String friFrom;
    private String friTo;

    private List<String> timeTables;

    public BaseActivityInfo() {
    }
}