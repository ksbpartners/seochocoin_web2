package kr.co.ksbpartners.seochocoin.controller;

import kr.co.ksbpartners.seochocoin.entity.Admin;
import kr.co.ksbpartners.seochocoin.service.AdminService;
import kr.co.ksbpartners.seochocoin.service.CenterService;
import org.apache.commons.collections4.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/dist")
public class DistMainController {

    @Autowired
    AdminService adminService;

    @Autowired
    CenterService centerService;

    @RequestMapping("/index")
    public String hello(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        List<Map<String, Long>> allCntList = centerService.cntAllCenter();

        model.addAttribute("allCntList", allCntList);
        return "dist/index";
    }

    @GetMapping("/userinfo")
    public String userinfo(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Admin admin = adminService.getById(auth.getName());

        model.addAttribute("admin", admin);
        return "dist/userinfo";
    }

    @PostMapping("/userinfo-update")
    public String userinfoUpdate(@ModelAttribute("admin") @Validated Admin admin,
                                 BindingResult bindingResult, Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (bindingResult.hasErrors()) {
            return "dist/userinfo";
        }
        admin.setId(auth.getName());
        admin.setUpdatedBy(auth.getName());
        if (adminService.modify(admin) != 1) {
            // error
        }

        model.addAttribute("admin", admin);
        return "dist/userinfo";
    }

    @GetMapping("/login")
    public String login() {
        return "dist/login";
    }

    @GetMapping("/forms-basic")
    public String formsBasic() {
        return "dist/forms-basic";
    }
}
