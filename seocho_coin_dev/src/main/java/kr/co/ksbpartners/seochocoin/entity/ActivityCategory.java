package kr.co.ksbpartners.seochocoin.entity;

import lombok.Data;
import org.apache.ibatis.type.Alias;

import java.io.Serializable;
import java.util.Date;

@Data
@Alias("activity_category")
public class ActivityCategory extends EntityBase implements Serializable {
    private static final long serialVersionUID = 1L;

    private String id;
    private String type;
    private String actName;

    public ActivityCategory() { }

    public ActivityCategory(String _id, String _type, String actName) {
        this.id = _id;
        this.type = _type;
        this.actName = actName;
    }

    public ActivityCategory(String _id, String actName) {
        this.id = _id;
        this.actName = actName;
    }
}
