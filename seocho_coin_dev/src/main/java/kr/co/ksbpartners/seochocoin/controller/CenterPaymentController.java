package kr.co.ksbpartners.seochocoin.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/cent")
public class CenterPaymentController {
    @GetMapping("/payment-detail")
    public String paymentDetail() {
        return "cent/payment-detail";
    }

    @GetMapping("/payment-list")
    public String paymentList() {
        return "cent/payment-list";
    }

    @GetMapping("/payment-refund-request-detail")
    public String paymentRefundRequestDetail() {
        return "payment-refund-request-detail";
    }

    @GetMapping("/payment-refund-request-list")
    public String paymentRefundRequestList() {
        return "cent/payment-refund-request-list";
    }
}
