function execDaumPostcode() {
    new daum.Postcode({
        oncomplete: function(data) {
            // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.
            // https://github.com/daumPostcode/QnA#12-%ED%8C%9D%EC%97%85%EC%B0%BD%EC%97%90%EC%84%9C-%EA%B2%80%EC%83%89-%ED%9B%84-%EA%B2%B0%EA%B3%BC%EB%A5%BC-%ED%81%B4%EB%A6%AD%ED%95%B4%EB%8F%84-%EB%B0%98%EC%9D%91%EC%9D%B4-%EC%97%86%EC%8A%B5%EB%8B%88%EB%8B%A4

            // 각 주소의 노출 규칙에 따라 주소를 조합한다.
            // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
            var addr = ''; // 주소 변수
            var extraAddr = ''; // 참고항목 변수

            //사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
            if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
                addr = data.roadAddress;
            } else { // 사용자가 지번 주소를 선택했을 경우(J)
                addr = data.jibunAddress;
            }

            // 사용자가 선택한 주소가 도로명 타입일때 참고항목을 조합한다.
            if(data.userSelectedType === 'R'){
                // 법정동명이 있을 경우 추가한다. (법정리는 제외)
                // 법정동의 경우 마지막 문자가 "동/로/가"로 끝난다.
                if(data.bname !== '' && /[동|로|가]$/g.test(data.bname)){
                    extraAddr += data.bname;
                }
                // 건물명이 있고, 공동주택일 경우 추가한다.
                if(data.buildingName !== '' && data.apartment === 'Y'){
                    extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                }
                // 표시할 참고항목이 있을 경우, 괄호까지 추가한 최종 문자열을 만든다.
                if(extraAddr !== ''){
                    extraAddr = ' (' + extraAddr + ')';
                }
                // 조합된 참고항목을 해당 필드에 넣는다.
                document.getElementById("addrDetail").value = extraAddr;

            } else {
                document.getElementById("addrDetail").value = '';
            }

            // 우편번호와 주소 정보를 해당 필드에 넣는다.
            document.getElementById('postalCode').value = data.zonecode;
            if (document.getElementById("addr")) {
                document.getElementById("addr").value = addr;
            } else {
                document.getElementById("addrDoro").value = data.roadAddress;
                document.getElementById("addrJibun").value = data.jibunAddress;
            }
            // 커서를 상세주소 필드로 이동한다.
            document.getElementById("addrDetail").focus();
        }
    }).open();
}
$(function(){
    // dataTable設定
    $("#bootstrap-data-table-export").DataTable({
        lengthChange: false,
        searching: false,
//        ordering: true,
        displayLength: 10,
        stripeClasses: [],
        columnDefs: [{ "orderable": false, "targets": 0 }],
        order: [],
        language: {
            emptyTable : "",
            zeroRecords : "",
            info : "_TOTAL_중 _START_-_END_ ",
            infoEmpty : "",
            paginate : {
                first : "처음",
                previous : "이전",
                next : "다음",
                last : "끝"
            },
        },
        dom:
          "<'row'<'col-sm-12'tr>>" +
          "<'row'<'col-sm-6 right'i>>" +
          "<'row'<'col-sm-12'p>>"
    });

    $('#datefilter').daterangepicker({
        "startDate": moment().subtract(1, 'week'),
        "endDate": moment()
    }, function(start, end, label) {
//        console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')');
        $('#startDate').text(start.format('YYYY-MM-DD'));
        $('#endDate').text(end.format('YYYY-MM-DD'));
        // submit search
        submitSearch();
    });

    $(document).on("click", ".changeDate", function(e) {
        $('#datefilter').data('daterangepicker').setEndDate(moment());
        if ($(this).attr("id").includes("Today")) {
            $('#datefilter').data('daterangepicker').setStartDate(moment());
        } else if ($(this).attr("id").includes("Week")) {
            $('#datefilter').data('daterangepicker').setStartDate(moment().subtract(1, 'week'));
        } else if ($(this).attr("id").includes("1Month")) {
            $('#datefilter').data('daterangepicker').setStartDate(moment().subtract(1, 'month'));
        } else if ($(this).attr("id").includes("3Month")) {
            $('#datefilter').data('daterangepicker').setStartDate(moment().subtract(3, 'month'));
        } else if ($(this).attr("id").includes("Year")) {
            $('#datefilter').data('daterangepicker').setStartDate(moment().subtract(1, 'year'));
        }
        // submit search
        submitSearch();
    });


});

function submitSearch() {
    var startDate = $('#datefilter').data('daterangepicker').startDate.format('YYYY-MM-DD');
    var endDate = $('#datefilter').data('daterangepicker').endDate.format('YYYY-MM-DD');
    $('#startDate').text(startDate);
    $('#endDate').text(endDate);
    $('#fromDate').val(startDate);
    $('#toDate').val(endDate);

   $('#rangeSearch').submit();

}

var memberSearchModal = {
    target: null,

    open: function(target) {
        this.target = target;
        var data = {};
        window.open('/cent/member-search-modal','_blank','width=1200,height=800,scrollbars=1,location=0,menubar=0,toolbar=0,status=1,directories=0,resizable=1');
    },

    callback: function(json) {
        if (this.target != 'all') {
            var orgIds = $('#' + this.target + 'MemIds').val().split(',');
            var orgNms = $('#' + this.target + 'MemNms').val().split(',');
            var sumIds = orgIds.concat(json.memberIds).filter(function(v){return v!==''});
            var sumNms = orgNms.concat(json.memberNms).filter(function(v){return v!==''});
            $('#' + this.target + 'MemIds').val(sumIds);
            $('#' + this.target + 'MemNms').val(sumNms);
        } else {
            var weekday = ['mon','tue','wed','thu','fri'];
            $.each(weekday, function(index, elem) {
                if ($('#'+elem).prop("checked") && $('#'+elem+'From').val() && $('#'+elem+'To').val()) {
                    var orgIds = $('#' + elem + 'MemIds').val().split(',');
                    var orgNms = $('#' + elem + 'MemNms').val().split(',');
                    var sumIds = orgIds.concat(json.memberIds).filter(function(v){return v!==''});
                    var sumNms = orgNms.concat(json.memberNms).filter(function(v){return v!==''});
                    $('#' + elem + 'MemIds').val(sumIds);
                    $('#' + elem + 'MemNms').val(sumNms);
                }
            });
        }
    }
}

var activityScheduleModal = {
    open: function(activityId, activityDate) {
        var data = {};
        window.open('/cent/activity-schedule-modal?activityId='+ activityId +'&activityDate='+ activityDate,'_blank','width=1200,height=800,scrollbars=1,location=0,menubar=0,toolbar=0,status=1,directories=0,resizable=1');
    },

    callback: function() {
        location.reload();
    }
}

function hasname() {
    $.ajax({
        url: '/dist/center-search',
        type: 'GET',
        data: {centerName: $('#name').val()},
        dataType: 'text',
        timeout: 1000,
        success: function (data, dataType) {
            if (data === '') {
                alert("사용 가능한 센터명 입니다.");
            } else {
                alert("중복된 센터명이 있습니다.");
            }
        }
    });
}
