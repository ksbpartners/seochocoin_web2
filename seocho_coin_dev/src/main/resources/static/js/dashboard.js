(function ($) {
  "use strict";


  // const brandPrimary = '#20a8d8'
  const Old20 = '#90dba1'
  const Old30 = '#63c2de'
  const Old40 = '#dc3545'
  const Old50 = '#ffc107'

  function convertHex(hex, opacity) {
    hex = hex.replace('#', '')
    const r = parseInt(hex.substring(0, 2), 16)
    const g = parseInt(hex.substring(2, 4), 16)
    const b = parseInt(hex.substring(4, 6), 16)

    const result = 'rgba(' + r + ',' + g + ',' + b + ',' + opacity / 100 + ')'
    return result
  }

  function random(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min)
  }

  var elements = 30
  var data1 = []
  var data2 = []
  var data3 = []
  var data4 = []

  for (var i = 0; i <= elements; i++) {
    data1.push(random(50, 200))
    data2.push(random(80, 100))
    data3.push(65, 120)
    data4.push(random(80, 100))
  }


  //Traffic Chart
  var ctx = document.getElementById("trafficChart");
  //ctx.height = 200;
  var myChart = new Chart(ctx, {
    type: 'line',
    data: {
      labels: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30'],
      datasets: [
        {
          label: 'My First dataset',
          backgroundColor: convertHex(Old30, 10),
          borderColor: Old20,
          pointHoverBackgroundColor: '#fff',
          borderWidth: 2,
          data: data1
        },
        {
          label: 'My Second dataset',
          backgroundColor: convertHex(Old30, 10),
          borderColor: Old30,
          pointHoverBackgroundColor: '#fff',
          borderWidth: 2,
          data: data2
        },
        {
          label: 'My third dataset',
          backgroundColor: convertHex(Old30, 10),
          borderColor: Old40,
          pointHoverBackgroundColor: '#fff',
          borderWidth: 2,
          data: data3
        }
        ,
        {
          label: 'My third dataset',
          backgroundColor: convertHex(Old30, 10),
          borderColor: Old50,
          pointHoverBackgroundColor: '#fff',
          borderWidth: 2,
          data: data4
        }
      ]
    },
    options: {
      //   maintainAspectRatio: true,
      //   legend: {
      //     display: false
      // },
      // scales: {
      //     xAxes: [{
      //       display: false,
      //       categoryPercentage: 1,
      //       barPercentage: 0.5
      //     }],
      //     yAxes: [ {
      //         display: false
      //     } ]
      // }


      maintainAspectRatio: true,
      legend: {
        display: false
      },
      responsive: true,
      scales: {
        xAxes: [{
          gridLines: {
            drawOnChartArea: false
          }
        }],
        yAxes: [{
          ticks: {
            beginAtZero: true,
            maxTicksLimit: 5,
            stepSize: Math.ceil(250 / 5),
            max: 250
          },
          gridLines: {
            display: true
          }
        }]
      },
      elements: {
        point: {
          radius: 0,
          hitRadius: 10,
          hoverRadius: 4,
          hoverBorderWidth: 3
        }
      }


    }
  });
  //Traffic Chart
  var ctx = document.getElementById("trafficChart2");
  //ctx.height = 200;
  var myChart = new Chart(ctx, {
    type: 'line',
    data: {
      labels: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30'],
      datasets: [
        {
          label: 'My First dataset',
          backgroundColor: convertHex(Old20, 10),
          borderColor: Old20,
          pointHoverBackgroundColor: '#fff',
          borderWidth: 2,
          data: data1
        },
        {
          label: 'My Second dataset',
          backgroundColor: 'transparent',
          borderColor: Old10,
          pointHoverBackgroundColor: '#fff',
          borderWidth: 2,
          data: data2
        }
      ]
    },
    options: {
      //   maintainAspectRatio: true,
      //   legend: {
      //     display: false
      // },
      // scales: {
      //     xAxes: [{
      //       display: false,
      //       categoryPercentage: 1,
      //       barPercentage: 0.5
      //     }],
      //     yAxes: [ {
      //         display: false
      //     } ]
      // }


      maintainAspectRatio: true,
      legend: {
        display: false
      },
      responsive: true,
      scales: {
        xAxes: [{
          gridLines: {
            drawOnChartArea: false
          }
        }],
        yAxes: [{
          ticks: {
            beginAtZero: true,
            maxTicksLimit: 5,
            stepSize: Math.ceil(250 / 5),
            max: 250
          },
          gridLines: {
            display: true
          }
        }]
      },
      elements: {
        point: {
          radius: 0,
          hitRadius: 10,
          hoverRadius: 4,
          hoverBorderWidth: 3
        }
      }


    }
  });


})(jQuery);