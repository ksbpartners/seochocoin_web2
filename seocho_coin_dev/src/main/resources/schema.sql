
/* admin_master 관리자 마스터 */
CREATE TABLE IF NOT EXISTS admin (
    id          VARCHAR(18)  NOT NULL
  , name        VARCHAR(255) NOT NULL
  , pwd         VARCHAR(256) NOT NULL

  , type        VARCHAR(256) NOT NULL -- all(통합) | center
  , centerId    VARCHAR(18)  NOT NULL -- all | centerId

  , birth       VARCHAR(10)
  , sex         VARCHAR(1) -- 남/여
  , age        smallint(10)
  , phone       VARCHAR(18)
  , mobile      VARCHAR(18)
  , email       VARCHAR(255)

  , postal_code  VARCHAR(255)  -- 우편번호
  , addr         VARCHAR(512)  -- 주소
  , addr_detail  VARCHAR(255)  -- 상세주소

  , created_at  TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP -- 등록일시
  , created_by  VARCHAR(255) NOT NULL  -- 등록자
  , updated_at  TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
  , updated_by  VARCHAR(255) NOT NULL  -- 갱신자
  , del_flg     VARCHAR(1) NOT NULL DEFAULT 'N'
  , PRIMARY KEY(id)
);


/*  member master (사용자 정보) */
CREATE TABLE IF NOT EXISTS members (
    id          VARCHAR(18)  NOT NULL
  , pwd         VARCHAR(18)  NOT NULL
  , name        VARCHAR(18)  NOT NULL
  , mobile      VARCHAR(18)  NOT NULL

  , token       TEXT
  , coin        smallint(10) -- 보유코인
  , area        VARCHAR(255)
  , birth       VARCHAR(10)
  , sex         VARCHAR(1) -- 남/여
  , married     VARCHAR(1) -- Y/N
  , phone       VARCHAR(18)
  , email       VARCHAR(255)

  , postal_code  VARCHAR(255)  -- 우편번호
  , addr_doro    VARCHAR(512)  -- 도로명주소
  , addr_jibun   VARCHAR(512)  -- 지번주소
  , addr_detail  VARCHAR(255)  -- 상세주소

  , center1 VARCHAR(18) -- center_id
  , center2 VARCHAR(18) -- center_id
  , center3 VARCHAR(18) -- center_id

  , created_at  TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP -- 등록일시
  , created_by  VARCHAR(255) NOT NULL  -- 등록자
  , updated_at  TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
  , updated_by  VARCHAR(255) NOT NULL  -- 갱신자
  , del_flg     VARCHAR(1) NOT NULL DEFAULT 'N'
  , PRIMARY KEY(id)
);



/* activity_master (활동) */
CREATE TABLE IF NOT EXISTS activity (
    id          VARCHAR(18)   NOT NULL
  , center_id   VARCHAR(18)   NOT NULL

  , name        VARCHAR(255)  -- 활동명 - 컴활, 요가, 어린이집...
  , type        VARCHAR(255)  -- 활동유형(구분) - 교육, 봉사활동
  , category    VARCHAR(255)  -- ???
  , in_charge   VARCHAR(255)  -- 담당자
  , contact     VARCHAR(255)  -- 문의

  , status      VARCHAR(255)  -- 현재상태 - 모집전, 모집중, 모집마감, 폐강

  , area        VARCHAR(255)  -- 지역 - 중앙,양재,방배,서초1동...
  , map         VARCHAR(1024) -- 지도 정보

  , detail      VARCHAR(2048) -- 활동내용
  , start_date  VARCHAR(255)  -- 시작일
  , end_date    VARCHAR(255)  -- 종료일

  , has_period  VARCHAR(1) NOT NULL DEFAULT 'N' -- 활동횟수 1회성, 기간
  , mon         VARCHAR(1) NOT NULL DEFAULT 'N'
  , mon_from    VARCHAR(5) -- HH:MM
  , mon_to      VARCHAR(5) -- HH:MM
  , tue         VARCHAR(1) NOT NULL DEFAULT 'N'
  , tue_from    VARCHAR(5) -- HH:MM
  , tue_to      VARCHAR(5) -- HH:MM
  , wed         VARCHAR(1) NOT NULL DEFAULT 'N'
  , wed_from    VARCHAR(5) -- HH:MM
  , wed_to      VARCHAR(5) -- HH:MM
  , thu         VARCHAR(1) NOT NULL DEFAULT 'N'
  , thu_from    VARCHAR(5) -- HH:MM
  , thu_to      VARCHAR(5) -- HH:MM
  , fri         VARCHAR(1) NOT NULL DEFAULT 'N'
  , fri_from    VARCHAR(5) -- HH:MM
  , fri_to      VARCHAR(5) -- HH:MM

  , limt        smallint   -- 정원 (명)

  , use_coin       VARCHAR(1) NOT NULL DEFAULT 'N' -- 코인사용 여부
  , use_coin_val   long

  , add_coin       VARCHAR(1) NOT NULL DEFAULT 'N' -- 코인적립 여부
  , add_coin_val   long
  , add_coin_after  VARCHAR(1) NOT NULL DEFAULT 'N' -- 활동환료후 승인(Y/N)

  , attend_check   VARCHAR(1) NOT NULL DEFAULT 'N' -- 출결확인 필요 여부
  , attend_rate    long

  , use_price_info VARCHAR(1) NOT NULL DEFAULT 'N' -- 가격정보 사용|미사용
  , price          long

  , auto_add_market VARCHAR(1) NOT NULL DEFAULT 'N' -- 사용처 자동등록

  , qr_code     VARCHAR(255)  -- QR코드 저장장소

  , open_activity VARCHAR(1) NOT NULL DEFAULT 'N' -- 게시 여부

  , created_at  TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP -- 등록일시
  , created_by  VARCHAR(255) NOT NULL  -- 등록자
  , updated_at  TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
  , updated_by  VARCHAR(255) NOT NULL  -- 갱신자
  , del_flg     VARCHAR(1) NOT NULL DEFAULT 'N'
  , PRIMARY KEY(id, center_id)
);

/* activity schedule (활동) */
CREATE TABLE IF NOT EXISTS activity_schedule (
    member_id      VARCHAR(18)  NOT NULL
  , activity_id    VARCHAR(18)  NOT NULL
  , activity_date  DATE         NOT NULL -- 날짜
  , day            VARCHAR(3)   NOT NULL -- 월,화,수,목,금
  , start          VARCHAR(5)   NOT NULL -- HH:MM
  , end            VARCHAR(5)   NOT NULL -- HH:MM
  , attended_at    TIMESTAMP
  , has_period     VARCHAR(1)   NOT NULL DEFAULT 'Y' -- 활동횟수 1회성, 기간

  , created_at  TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP -- 등록일시
  , created_by  VARCHAR(255) NOT NULL  -- 등록자
  , updated_at  TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
  , updated_by  VARCHAR(255) NOT NULL  -- 갱신자
  , del_flg     VARCHAR(1) NOT NULL DEFAULT 'N'
  , PRIMARY KEY(member_id, activity_id, activity_date, has_period)
);

/* activity_history (활동정보)
CREATE TABLE IF NOT EXISTS activity_history (
    id          int(10)      NOT NULL AUTO_INCREMENT
  , center_id   VARCHAR(18)  NOT NULL
  , activity_id VARCHAR(18)  NOT NULL
  , member_id   VARCHAR(18)  NOT NULL

  , attended_at TIMESTAMP    NOT NULL
  , status      VARCHAR(18)  -- 결재대기 | 결재완료
  , coin        int(10)      -- 적립코인

  , created_at  TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP -- 등록일시
  , created_by  VARCHAR(255) NOT NULL  -- 등록자
  , updated_at  TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
  , updated_by  VARCHAR(255) NOT NULL  -- 갱신자
  , del_flg     VARCHAR(1) NOT NULL DEFAULT 'N'
  , PRIMARY KEY(id, center_id, activity_id, member_id)
);
*/

/* member activity */
CREATE TABLE IF NOT EXISTS member_activity (
    member_id        VARCHAR(18)  NOT NULL
  , center_id        VARCHAR(18)  NOT NULL
  , activity_id      VARCHAR(18)  NOT NULL
  , activity_status  VARCHAR(18)  -- 활동중 | 활동중지
  , payment_status   VARCHAR(18)  -- 결제대기 | 결제완료
  , accum_status     VARCHAR(18)  -- 적립 | 적립취소
  , accum_coin       int(10)      -- 적립코인

  , created_at  TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP -- 등록일시
  , created_by  VARCHAR(255) NOT NULL  -- 등록자
  , updated_at  TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
  , updated_by  VARCHAR(255) NOT NULL  -- 갱신자
  , del_flg     VARCHAR(1) NOT NULL DEFAULT 'N'
  , PRIMARY KEY(member_id, activity_id)
);

/* coin_leger (코인 입출금 장부) */
CREATE TABLE IF NOT EXISTS coin_leger (
    id          int(10)      NOT NULL AUTO_INCREMENT
  , center_id   VARCHAR(18)  NOT NULL
  , activity_id VARCHAR(18)  NOT NULL
  , member_id   VARCHAR(18)  NOT NULL
  , type        VARCHAR(18)  NOT NULL   -- 적립|사용

  , paid        int(10) -- 결제금|적립금
  , coin        int(10) -- 코인 결제금|적립금

  , created_at  TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP -- 등록일시
  , created_by  VARCHAR(255) NOT NULL  -- 등록자
  , updated_at  TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
  , updated_by  VARCHAR(255) NOT NULL  -- 갱신자
  , del_flg     VARCHAR(1) NOT NULL DEFAULT 'N'
  , PRIMARY KEY(id, center_id, activity_id, member_id, type)
);


/* center_master 센터 마스터 (센터 상세보기 참조) */
CREATE TABLE IF NOT EXISTS center (
    id          VARCHAR(18)  NOT NULL
  , admin_id    VARCHAR(255) NOT NULL  -- 담당자 ID

  , name        VARCHAR(255)
  , area        VARCHAR(255)  -- 지역
  , type        VARCHAR(255)  -- 구분 - 복지회관|자치회관
  , postal_code VARCHAR(255)  -- 우편번호
  , addr        VARCHAR(255)  -- 주소
  , description VARCHAR(1024) -- 비고

  , created_at  TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP -- 등록일시
  , created_by  VARCHAR(255) NOT NULL  -- 등록자
  , updated_at  TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
  , updated_by  VARCHAR(255) NOT NULL  -- 갱신자
  , del_flg     VARCHAR(1) NOT NULL DEFAULT 'N'
  , PRIMARY KEY(id)
);


-- 사용처
CREATE TABLE IF NOT EXISTS market (
    id          VARCHAR(18)  NOT NULL
  , center_id   VARCHAR(18)  NOT NULL  -- center id
  , activity_id VARCHAR(18)  NOT NULL

  , name        VARCHAR(255)
  , in_charge   VARCHAR(255)  -- 담당자
  , contact     VARCHAR(255)  -- 문의

  , type        VARCHAR(255)  -- 구분 - 교육|식당
  , detail      VARCHAR(2048) -- 사용처내용
  , area        VARCHAR(255)  -- 지역
  , addr        VARCHAR(255)  -- 주소
  , price       long -- 가격
  , use_coin    long  -- 사용코인
  , open_activity VARCHAR(1) NOT NULL DEFAULT 'N' -- 게시 여부

  , created_at  TIMESTAMP       NOT NULL DEFAULT CURRENT_TIMESTAMP -- 등록일시
  , created_by  VARCHAR(255)    NOT NULL  -- 등록자
  , updated_at  TIMESTAMP       NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
  , updated_by  VARCHAR(255)    NOT NULL  -- 갱신자
  , del_flg     VARCHAR(1)      NOT NULL DEFAULT 'N'
  , PRIMARY KEY(id, center_id, activity_id)
);





/* center_users 센터사용자
CREATE TABLE IF NOT EXISTS center_members (
    center_id   VARCHAR(18) NOT NULL -- 센터 ID
  , member_id   VARCHAR(18) NOT NULL -- 유저 ID

  , created_at  TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP -- 등록일시
  , created_by  VARCHAR(255) NOT NULL  -- 등록자
  , updated_at  TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
  , updated_by  VARCHAR(255) NOT NULL  -- 갱신자
  , del_flg     VARCHAR(1) NOT NULL DEFAULT 'N'
  , PRIMARY KEY(center_id, member_id)
);
*/
