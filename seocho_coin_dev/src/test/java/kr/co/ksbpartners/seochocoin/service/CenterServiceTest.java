package kr.co.ksbpartners.seochocoin.service;

import kr.co.ksbpartners.seochocoin.SeochocoinApplicationTests;
import kr.co.ksbpartners.seochocoin.entity.Center;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


public class CenterServiceTest extends SeochocoinApplicationTests{

    @Resource(name="centerService")
    private CenterService centerService;

    @Before
    public void setup() throws Exception {

        for (int i = 0 ; i < 3 ; i++) {
            Center ent = new Center();
            ent.setAdminId("adm000001");
            centerService.add(ent);
        }

    }

    @Test
    public void whenSelectAllCenter() throws Exception {
        // given
        List<Center> lst = centerService.getAll(null, null, null , null);
        List<String> ids = new ArrayList<>();
        for (Center ent : lst) {
            ids.add(ent.getId());
        }
        centerService.remove(ids, "tester");

        // when
        lst = centerService.getAll(null, null, null , null);

        // then
        assertEquals(lst == null || lst.size() == 0, true);


        for (int i = 0 ; i < 3 ; i++) {
            Center ent = new Center();
            ent.setAdminId("adm000001");
            centerService.add(ent);
        }

        // when
        lst = centerService.getAll(null, null, null , null);

        // then
        assertEquals(lst.size(), 3);
    }


    @Test
    public void whenGetCenterById() {
        // given
        String id = "cen000001";

        // when
        Center member = centerService.getById(id);

        // then
        assertNotNull(member);
        assertEquals(member.getId(), id);
    }


    @Test
    public void whenDeleteCenter() {
        // given
        Center ent = new Center();
        ent.setId("cen000001");

        // when
        List<String> ids = new ArrayList<>();
        ids.add(ent.getId());
        centerService.remove(ids, "tester");

        // then
        Center member = centerService.getById(ent.getId());
        assertEquals(member.getDelFlg(), "Y");
    }


    @Test
    public void whenModifyCenter() throws Exception {
        // given
        Center ent = new Center();
        ent.setAdminId("adm000001");
        String id = centerService.add(ent);

        // when
        Center src = centerService.getById(id);
        src.setName("test modify");
        centerService.modify(src);

        // then
        Center dsc = centerService.getById(id);
        assertEquals(src.getName(), dsc.getName());
    }



    @Test
    public void whenDeleteInsertCenter() throws Exception {
        // given delete all member
        List<Center> lst = centerService.getAll(null, null, null , null);
        List<String> ids = new ArrayList<>();
        for (Center ent : lst) {
            ids.add(ent.getId());
        }
        centerService.remove(ids, "tester");

        // when
        int count = 3;
        for (int i = 0 ; i < count ; i++) {
            Center ent = new Center();
            ent.setAdminId("adm000001");
            centerService.add(ent);
        }

        // then
        lst = centerService.getAll(null, null, null , null);

        assertEquals(lst.size(), count);
    }

}
