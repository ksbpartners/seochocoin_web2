package kr.co.ksbpartners.seochocoin.service;

import kr.co.ksbpartners.seochocoin.SeochocoinApplicationTests;
import kr.co.ksbpartners.seochocoin.entity.Admin;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


public class AdminServiceTest extends SeochocoinApplicationTests{

    @Resource(name="adminService")
    private AdminService adminService;

    @Before
    public void setup() throws Exception {

        for (int i = 0 ; i < 3 ; i++) {
            Admin ent = new Admin("admin_" + i, "name" +i);
            adminService.add(ent);
        }

    }

    @Test
    public void whenSelectAllAdmin() throws Exception {
        // given

        // when
        List<Admin> lst = adminService.getAll();

        // then
        assertEquals(lst != null && lst.size() > 0, true);

    }


    @Test
    public void whenGetAdminById() {
        // given
        String id = "adm000001";

        // when
        Admin member = adminService.getById(id);

        // then
        assertNotNull(member);
        assertEquals(member.getId(), id);
    }


    @Test
    public void whenDeleteAdmin() {
        // given
        Admin ent = new Admin();
        ent.setId("adm000001");

        // when
        adminService.remove(ent);

        // then
        Admin member = adminService.getById(ent.getId());
        assertEquals(member.getDelFlg(), "Y");
    }


    @Test
    public void whenModifyAdmin() throws Exception {
        // given
        Admin ent = new Admin();
        ent.setName("name whenModifyAdmin");
        String id = adminService.add(ent);

        // when
        Admin src = adminService.getById(id);
        src.setName("test modify");
        adminService.modify(src);

        // then
        Admin dsc = adminService.getById(id);
        assertEquals(src.getName(), dsc.getName());
    }



    @Test
    public void whenDeleteInsertAdmin() throws Exception {
        // given delete all member
        List<Admin> lst = adminService.getAll();
        for (Admin ent : lst) {
            adminService.remove(ent);
        }

        // when
        int count = 3;
        for (int i = 0 ; i < count ; i++) {
            Admin ent = new Admin();
            ent.setId("adm000001");
            ent.setName("name");
            adminService.add(ent);
        }

        // then
        lst = adminService.getAll();

        assertEquals(lst.size(), count);
    }

}
