package kr.co.ksbpartners.seochocoin.service;

import kr.co.ksbpartners.seochocoin.SeochocoinApplicationTests;
import kr.co.ksbpartners.seochocoin.entity.Activity;
import kr.co.ksbpartners.seochocoin.vo.ActivityVO;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.List;

import static org.junit.Assert.assertEquals;


public class ActivityServiceTest extends SeochocoinApplicationTests{

    @Resource(name="activityService")
    private ActivityService activityService;

    @Before
    public void setup() throws Exception {
        // do nothing
    }

    @Test
    public void whenSelectAllActivity() {
        // given
        String id = "cen000001";

        // when
        List<ActivityVO> lst = activityService.getAll(id, null, null, null);

        // then
        assertEquals(4, lst.size());
    }

    @Test
    public void whenSelectAllActivityNoResult() {
        // given
        String id = "cen0000XX";

        // when
        List<ActivityVO> lst = activityService.getAll(id, null, null, null);

        // then
        assertEquals(0, lst.size());
    }

}
