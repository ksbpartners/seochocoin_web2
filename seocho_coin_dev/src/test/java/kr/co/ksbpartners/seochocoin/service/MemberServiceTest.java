package kr.co.ksbpartners.seochocoin.service;

import kr.co.ksbpartners.seochocoin.SeochocoinApplicationTests;
import kr.co.ksbpartners.seochocoin.entity.Member;
import kr.co.ksbpartners.seochocoin.vo.MemberVO;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


public class MemberServiceTest extends SeochocoinApplicationTests{

    @Resource(name="memberService")
    private MemberService memberService;

    @Before
    public void setup() throws Exception {

        Member member1 = new Member(null, "pwd", "testMember1", "tester");
        member1.setCenter1("testCenter1");
        Member member2 = new Member(null, "pwd", "testMember2", "tester");
        member2.setCenter1(null);
        member2.setCenter2("testCenter1");
        member2.setCenter3(null);
        Member member3 = new Member(null, "pwd", "testMember3", "tester");
        member3.setCenter1(null);
        member3.setCenter2(null);
        member3.setCenter3("testCenter1");
        Member member4 = new Member(null, "pwd", "testMember4", "tester");
        member4.setCenter1("testCenter2");
        member4.setCenter2("testCenter3");
        member4.setCenter3("testCenter4");

        memberService.add(member1);
        memberService.add(member2);
        memberService.add(member3);
        memberService.add(member4);

    }

    @Test
    public void whenSelectAllMember() {
        // given
        String centerId = "cen000001";

        // when
        List<MemberVO> lst = memberService.getAll(centerId, null);

        // then
        Assert.assertTrue(lst.size() > 0);
    }


    @Test
    public void whenGetMemberById() {
        // given
        String id = "mem000001";

        // when
        Member member = memberService.getById(id);

        // then
        assertNotNull(member);
        assertEquals(member.getId(), id);
    }


    @Test
    public void whenDeleteMember() {
        // given
        Member memberIn = new Member("mem000001", "pwd", "testMember4", "tester");
        List<String> ids = new ArrayList<>();
        ids.add("id04");

        // when
        memberService.remove(ids, "tester");

        // then
        Member ent = memberService.getById(memberIn.getId());
        assertEquals(ent.getDelFlg(), "Y");
    }


    @Test
    public void whenModifyMember() throws Exception {
        // given
        Member ent = new Member(null, "pwd", "testMember4", "tester");
        String id = memberService.add(ent);

        // when
        Member src = memberService.getById(id);
        src.setName("test modify");
        memberService.modify(src);

        // then
        Member dsc = memberService.getById(id);
        assertEquals(src.getName(), dsc.getName());
    }



    @Test
    public void whenDeleteInsertMember() throws Exception {
        // given delete all member
        String centerId = "cen000001";
        List<MemberVO> lst = memberService.getAll(centerId, null);
        List<String> ids = new ArrayList<>();

        for (MemberVO vo : lst) {
            ids.add(vo.getMember().getId());
        }
        memberService.remove(ids, "tester");

        // when
        Member ent = new Member(null, "pwd", "name");
        ent.setMobile("mobile");
        ent.setCenter1(centerId);
        String id = memberService.add(ent);

        // then
        lst = memberService.getAll(ent.getCenter1(), null);

        assertEquals(lst.size(), 1);
        assertEquals(lst.get(0).getMember().getId(), id);
    }

}
