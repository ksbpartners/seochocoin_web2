package kr.co.ksbpartners.seochocoin.service;

import kr.co.ksbpartners.seochocoin.SeochocoinApplicationTests;
import kr.co.ksbpartners.seochocoin.entity.CoinLeger;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.List;

import static org.junit.Assert.assertEquals;


public class CoinLegerServiceTest extends SeochocoinApplicationTests{

    @Resource(name= "coinLegerService")
    private CoinLegerService coinLegerService;

    @Before
    public void setup() throws Exception {
        // do nothing
    }

    @Test
    public void whenSelectAll() {
        // given
        String memberId = "mem000001";
        String centerId = "cen000001";

        // when
        List<CoinLeger> lst = coinLegerService.getAll(memberId, centerId);

        // then
        assertEquals(2, lst.size());
    }


}
