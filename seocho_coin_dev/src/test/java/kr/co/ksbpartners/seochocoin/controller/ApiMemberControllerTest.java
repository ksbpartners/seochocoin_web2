package kr.co.ksbpartners.seochocoin.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import kr.co.ksbpartners.seochocoin.entity.Member;
import kr.co.ksbpartners.seochocoin.service.ApiMemberService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;

import static org.mockito.Mockito.when;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ApiMemberControllerTest {
    @MockBean
    ApiMemberService apiMemberService;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    private WebApplicationContext context;
    private MockMvc mockMvc;

    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context)
                .build();
    }

    @Test
    public void testSettingPwd() throws Exception {
        MultiValueMap<String, String> _params = new LinkedMultiValueMap<>();
        _params.add("uid", "mem000001");
        _params.add("token", "test_token");

        Member member = new Member();
        member.setId("mem000004");
        member.setPwd("logout");

        String memberJson = objectMapper.writeValueAsString(member);

        ResultActions result = mockMvc.perform(put("/api/setting/pwd")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(memberJson)
                .params(_params)).andExpect(status().isOk());

//                .andExpect(jsonPath("name").value("이도원"));
        System.out.println(result.andExpect(jsonPath("result").value("1")));
    }

    private static String asJsonString(final Member obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
