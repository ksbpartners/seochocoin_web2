package kr.co.ksbpartners.seochocoin.repository;

import kr.co.ksbpartners.seochocoin.SeochocoinApplicationTests;
import kr.co.ksbpartners.seochocoin.entity.Member;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


public class MemberMapperTest  extends SeochocoinApplicationTests {

    @Autowired
    private MemberMapper memberMapper;

    @Before
    public void setup() {

        Member member1 = new Member("id01", "pwd", "testMember1", "tester");
        member1.setMobile("mobile");
        member1.setCenter1("testCenter1");
        Member member2 = new Member("id02", "pwd", "testMember2", "tester");
        member2.setMobile("mobile");
        member2.setCenter1(null);
        member2.setCenter2("testCenter1");
        member2.setCenter3(null);
        Member member3 = new Member("id03", "pwd", "testMember3", "tester");
        member3.setMobile("mobile");
        member3.setCenter1(null);
        member3.setCenter2(null);
        member3.setCenter3("testCenter1");
        Member member4 = new Member("id04", "pwd", "testMember4", "tester");
        member4.setMobile("mobile");
        member4.setCenter1("testCenter2");
        member4.setCenter2("testCenter3");
        member4.setCenter3("testCenter4");

        memberMapper.register(member1);
        memberMapper.register(member2);
        memberMapper.register(member3);
        memberMapper.register(member4);

    }

    @Test
    public void whenSelectAll() {
        // given
        String centerId = "testCenter1";

        // when
        List<Member> users = memberMapper.selectAll(centerId, null);

        // then
        assertEquals(users.size(), 3);
    }


    @Test
    public void whenGetMemberById() {
        // given
        String id = "id04";

        // when
        Member member = memberMapper.getById(id);

        // then
        assertNotNull(member);
        assertEquals(member.getId(), id);
    }


    @Test
    public void whenDelete() {
        // given
        Member memberIn = new Member("id04", "pwd", "testMember4", "tester");
        List<String> ids = new ArrayList<>();
        ids.add("id04");

        // when
        memberMapper.delete(ids, "tester");

        // then
        Member member = memberMapper.getById(memberIn.getId());
        assertEquals(member.getDelFlg(), "Y");
    }

}
