package kr.co.ksbpartners.seochocoin;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test") // use test application-test.yml
public class SeochocoinApplicationTests {

    @Test
    public void contextLoads() {
    }

}
